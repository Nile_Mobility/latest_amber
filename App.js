import React, {Component} from 'react';

import {YellowBox, StatusBar} from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Initial from './src/pages/auth/initial';
import Login from './src/pages/auth/login';
import Tab from './src/pages/home/tabBar';
import MobileWorkRecordSheet from './src/pages/home/MobileWorkRecordSheet';

import Signature from './src/component/Signature';
import Jobs from './src/pages/home/Jobs';
import DriverVehInsRep from './src/pages/home/DriverVehInsRep';
import Timesheet from './src/pages/home/Timesheet';
import AddTimesheet from './src/pages/home/AddTimesheet';
import AddTimesheetForWork from './src/pages/home/AddTimesheetForWork';
import AddInspection from './src/pages/home/AddInspection';
import DateTimePickers from './src/component/DateTimePickers';
import RadioButton from './src/component/RadioButton';
import HistoryContentBox from './src/component/HistoryContentBox';

import TimesheetHis from './src/pages/home/history/TimesheetHis';
import VehicleInspectionList from './src/pages/home/history/VehicleInspectionList';
import UploadImages from './src/pages/home/FormList/UploadImages';
import ForgotPassword from './src/pages/auth/forgotPassword';
import ResetPassword from './src/pages/home/ResetPassword';
import VerifyOtp from './src/pages/auth/VerifyOtp';
import Formslist from './src/pages/home/FormList/FormsList';
import TMPlanAndResAlloc from './src/pages/home/FormList/TMPlanAndResAlloc';
import LoadingList from './src/pages/home/FormList/LoadingList';
import TMWorkSheet from './src/pages/home/FormList/TMWorkSheet';
import RecordAndPreWorkSafCheck from './src/pages/home/FormList/RecordAndPreWorkSafCheck';
import TMSiteAssesment from './src/pages/home/FormList/TMSiteAssesment';
import Testing from './src/utility/Testing';
import JobHistory from './src/pages/home/history/JobHistory';
import WorksheetHistory from './src/pages/home/history/WorksheetHistory';

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const MainNavigator = createStackNavigator(
  {
    Initial: {screen: Initial},
    Login: {screen: Login},
    Tab: {screen: Tab},
    MobileWorkRecordSheet: {screen: MobileWorkRecordSheet},
    Signature: {screen: Signature},
    Jobs: {screen: Jobs},
    DriverVehInsRep: {screen: DriverVehInsRep},
    Timesheet: {screen: Timesheet},
    AddTimesheet: {screen: AddTimesheet},
    AddTimesheetForWork: {screen: AddTimesheetForWork},
    AddInspection: {screen: AddInspection},
    DateTimePickers: {screen: DateTimePickers},
    RadioButton: {screen: RadioButton},
    HistoryContentBox: {screen: HistoryContentBox},
    JobHistory: {screen: JobHistory},
    TimesheetHis: {screen: TimesheetHis},
    VehicleInspectionList: {screen: VehicleInspectionList},
    FormsList: {screen: Formslist},
    UploadImages: {screen: UploadImages},
    ForgotPassword: {screen: ForgotPassword},
    ResetPassword: {screen: ResetPassword},
    VerifyOtp: {screen: VerifyOtp},
    TMPlanAndResAlloc: {screen: TMPlanAndResAlloc},
    LoadingList: {screen: LoadingList},
    TMWorkSheet: {screen: TMWorkSheet},
    RecordAndPreWorkSafCheck: {screen: RecordAndPreWorkSafCheck},

    TMSiteAssesment: {screen: TMSiteAssesment},

    WorksheetHistory: {screen: WorksheetHistory},
    Testing: {screen: Testing},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Initial',
    navigationOptions: {
      headerShown: false,
      // disableGestures: true,
      disableGestures: true,
    },
  },
);
StatusBar.setHidden(true);

const AppContainer = createAppContainer(MainNavigator);

YellowBox.ignoreWarnings([
  'componentWillMount has been renamed',
  'Warning: componentWillUpdate is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
]);
