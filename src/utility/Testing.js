// remove node_modules
// npm cache verify
// npm install
// ng serve.

import React, {Component, useState} from 'react';
import DatePicker from 'react-native-date-picker';
import {View, Text, TouchableOpacity,Modal,StyleSheet,TouchableHighlight} from 'react-native';

import moment from 'moment';
export default class Testing extends Component {
  constructor(props) {
    super();
    this.state = {
      open: false,
      setDate: new Date(),
      confirmDate:'',
      modalVisible:false
    };
  }

  render() {
    return (
      <View >

     

        <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          <View>
              <View style={{backgroundColor: '', marginBottom: 100}}>
                <DatePicker
                  date={this.state.setDate}
                  onDateChange={(setDates) => this.setState({setDate:moment(setDates).format('MM-DD- YYYY')})}
                  androidVariant="nativeAndroid"
                />
              </View>
              <View style={{flexDirection: 'row',borderTopWidth:1}}>
              <TouchableOpacity
              onPress={() => this.setState({open:false,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({open:false,confirmDate:this.state.setDate,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>Ok</Text>
                </TouchableOpacity>
                
              </View>
            </View>

          </View>
        </View>
      </Modal>

      <TouchableHighlight
        style={styles.openButton}
        onPress={() => {
          this.setState({modalVisible:true});
        }}
      >
        <Text style={styles.textStyle}>Show Modal</Text>
      </TouchableHighlight>

      <Text>{JSON.stringify(this.state.confirmDate)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {

    backgroundColor: "white",
    borderRadius: 20,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },

});

// import React, { Component } from 'react';
// import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

// export default class RadioButton extends Component {
// 	state = {
//         value: null,
//     data : [
//             {
//                 key: '1',
//                 text: 'yes',
//             },
//             {
//                 key: '2',
//                 text: 'No',
//             },

//         ]
// 	};

// 	render() {

// 		const { value } = this.state;

// 		return (
// 			<View>
// 				{this.state.data.map(res => {
// 					return (
// 						<View key={res.key} style={styles.container}>

// 							<TouchableOpacity
// 								style={styles.radioCircle}
// 								onPress={() => {
// 									this.setState({
// 										value: res.key,
// 									});
// 								}}>
//                                   {value === res.key ? <View style={styles.selectedRb} />:null}
// 							</TouchableOpacity>
//                             <Text style={styles.radioText}>{res.text}</Text>
// 						</View>
// 					);
// 				})}

// 			</View>
// 		);
// 	}
// }

// const styles = StyleSheet.create({
// 	container: {
//         marginBottom: 35,
//         alignItems: 'center',
//         flexDirection: 'row',
// 		justifyContent: 'space-around',
// 	},
//     radioText: {
//         marginRight: 35,
//         fontSize: 20,
//         color: '#000',
//         fontWeight: '700'
//     },
// 	radioCircle: {
// 		height: 30,
// 		width: 30,
// 		borderRadius: 100,
// 		borderWidth: 2,
// 		borderColor: '#3740ff',
// 		alignItems: 'center',
// 		justifyContent: 'center',
// 	},
// 	selectedRb: {
// 		width: 15,
// 		height: 15,
// 		borderRadius: 50,
// 		backgroundColor: '#3740ff',
//     },
//     result: {
//         marginTop: 20,
//         color: 'white',
//         fontWeight: '600',
//         backgroundColor: '#F3FBFE',
//     },
// });

// import React, {Component} from 'react';
// import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
// import { RotationGestureHandler } from 'react-native-gesture-handler';
// import Icon from '../component/Icon'
// export default class Testing extends Component {
//   constructor() {
//     super();
//     this.state = {
//       value: 1,
//     };
//   }


 

//   render() {

//     const {value} = this.state
//     return (
//       <View>
//         <View style={styles.containerCheck}>
//           <View style={styles.squareBox}>
//             <TouchableOpacity onPress={() => this.setState({value: 1})}>
//               <View style={styles.outerBoxNew}>
//                 {value === 1 && <Icon name="check" size={20} />}
//               </View>
//             </TouchableOpacity>
//             <Text style={styles.label}>Morning - 30 min</Text>
//           </View>

//           <View style={styles.squareBox}>
//             <TouchableOpacity onPress={() => this.setState({value: 2})}>
//               <View style={styles.outerBoxNew}>
//                 {value === 2 && <Icon name="check" size={20} />}
//               </View>
//             </TouchableOpacity>
//             <Text style={styles.label}>Afternoon - 1 hour</Text>
//           </View>

//           <View style={styles.squareBox}>
//             <TouchableOpacity onPress={() => this.setState({value: 3})}>
//               <View style={styles.outerBoxNew}>
//                 {value === 3 && <Icon name="check" size={20} />}
//               </View>
//             </TouchableOpacity>
//             <Text style={styles.label}>Evening - 30 min</Text>
//           </View>
//         </View>

    

    
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//  innerCircle: {
//     height: 15,
//     width: 15,
//     backgroundColor: '#00aaff',
//     borderRadius : 100,
//   },
//   outerBoxNew: {
//     height: 30,
//     width: 30,
//     borderWidth: 2,
   
//     justifyContent: 'center',
//     alignItems: 'center',
//     marginRight: 20,
//   },
//   label: {fontSize: 15},
//   containerCheck: {
    
   
//     marginTop: 10,
//   },
//   squareBox: {
//     flexDirection: 'row',
//    alignItems:'center',
//    marginLeft:10,
//    marginBottom:10
//   },
// });
