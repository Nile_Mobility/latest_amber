import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity, FlatList} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import Icon from '../../../component/Icon';
import Loader from '../../../WebAPI/Loader';
import {
  get_all_final_submitted_timesheet,
  requestGetApi,
} from '../../../WebAPI/Service';

import KeyStore from '../../KeyStore/LocalKeyStore';
export default class TimesheetHis extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      data: [],
    };
  }

  async componentDidMount() {
    this.setState({loading: true});

    KeyStore.getKey('data', async (err, value) => {
      if (value) {
        const {user_id} = JSON.parse(value);
        const body = {
          user_id: user_id,
        };
        const {responseJson, err} = await requestGetApi(
          get_all_final_submitted_timesheet,
          body,
          'GET',
        );

        if (responseJson.status) {
          console.warn(responseJson);
          this.setState({loading: false, data: responseJson.all_timesheet});
        }
      } else {
      }
    });
  }

  renderItems(item) {
    const {id, status, updated_date, created_date, user_id} = item.item;
    console.warn(item);
    return (
      <View
        style={{
          borderWidth: 2,
          margin: 15,
          padding: 15,
          flexDirection: 'row',
          borderColor: '#7B7978',
          marginBottom: 0,
          backgroundColor: '#fff',
          borderRadius: 5,
        }}>
        <View style={{flex: 1}}>
          <Icon name="calendar" size={40} />
        </View>
        <View style={{flex: 5, flexDirection: 'column'}}>
          <Text numberOfLines={1} style={{fontSize: 17, fontWeight: 'bold'}}>
            Timesheet
          </Text>
          <Text>{updated_date}</Text>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('Timesheet', {
                id: id,
                button: 0,
              })
            }>
            <Icon name="eye" size={25} color="#F8981D" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Timesheets"
          goBack={() => this.props.navigation.goBack()}
        />

        <FlatList
          data={this.state.data}
          renderItem={(item) => this.renderItems(item)}
        />

        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
