import React, {Component} from 'react';
import {Text, StyleSheet, View,TouchableOpacity,Alert,FlatList} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import Icon from '../../../component/Icon';

import Loader from '../../../WebAPI/Loader'
import {get_vehicle_history,requestGetApi} from '../../../WebAPI/Service'
import KeyStore from '../../KeyStore/LocalKeyStore'

export default class VehicleInspectionList extends Component {
  constructor(){
    super()
    this.state={
      loading:false,
      data:[],
    }
  }
  
  openEdit=(id)=>{
    Alert.alert(
      '',
      'Are you sure you want to edit vehicle inspection info?',
      [
        {
          text: "NO",
          onPress: () => console.warn("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "YES",
          onPress: () => this.props.navigation.navigate('DriverVehInsRep',{
            id:id,
            button:1,
            reload: this.reloadData.bind(this),
          }),
        },
      ]
    )
      }
    
  reloadData() {

    this.fetchListApi()
  }

  async componentDidMount(){
    this.fetchListApi()
}

async fetchListApi(){

  KeyStore.getKey('data', async(err, value) => {
    if (value) {
      const {user_id}= JSON.parse(value)
      const body ={
        user_id:user_id,
     
      }
      this.setState({loading:true})

    const {responseJson,err} = await requestGetApi(get_vehicle_history,body,'GET')
    this.setState({loading:false})

    if(responseJson.status){
      this.setState({data:responseJson.all_vehicle_history})
    }
  
    }else{
    }
  });

}

  render() {
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Vehicle Inspection List"
          goBack={() => this.props.navigation.goBack()}
        />


<FlatList
data = {this.state.data}
renderItem={item=>this.renderItems(item)}
/>
   


        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }

renderItems(item){

  const {id,reg_no,updated_date} = item.item

return  <View
style={{borderWidth: 2, margin: 15, padding: 10, flexDirection: 'row',borderColor:"#7B7978",marginBottom:0,backgroundColor:"#fff",borderRadius:5}}>
<View style={{flex:1,justifyContent:"center",alignItems:"center",paddingRight:5}}>
  
  <Icon
   name="truck"
   size={40}
   
   />

</View>
<View style={{flex:4,flexDirection:"column"}}>
   <Text numberOfLines={2} style={{fontSize:18,fontWeight:"bold"}}>Driver's Vehicle Inspection Report</Text>
<Text>Date: {updated_date}</Text>
<Text>Reg No: {reg_no}</Text>
</View>
<View style={{flex:1,justifyContent:"space-between",alignItems:"center",flexDirection:"row",backgroundColor:""}}>
   
<TouchableOpacity onPress={()=>this.openEdit(id)}>

        
<Icon
name="pencil"
size={25}
color="#F8981D"
/>
</TouchableOpacity>
    <TouchableOpacity onPress={()=>this.props.navigation.navigate('DriverVehInsRep',{
      id:id,
      button:0,
      reload: this.reloadData.bind(this),
    })}>
<Icon
name="eye"
size={25}
color="#F8981D"
/>
</TouchableOpacity>
</View>
</View>


}

}

const styles = StyleSheet.create({});




