import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity, FlatList} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import Icon from '../../../component/Icon';
import Loader from '../../../WebAPI/Loader';
import {all_completed_job, requestGetApi} from '../../../WebAPI/Service';
import KeyStore from '../../KeyStore/LocalKeyStore'
export default class WorksheetHistory extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      data: [],
    };
  }

  async componentDidMount() {
    this.setState({loading: true});
    KeyStore.getKey('data', async (err, value) => {
      if (value) {
        const {user_id} = JSON.parse(value);
        const body = {
          user_id: user_id,
        };
        const {responseJson, err} = await requestGetApi(
          all_completed_job,
          body,
          'GET',
        );

        if (responseJson.status) {
          console.warn(responseJson);
          this.setState({loading: false, data: responseJson.all_jobs});
        }
      } else {
      }
    });
  }

  renderItems(item) {
    const {id, status, updated_date} = item.item;
    console.warn(item);
    return (
      <View 
        style={{
          borderWidth: 2,
          margin: 15,
          padding: 15,
          flexDirection: 'row',
          borderColor: '#7B7978',
          marginBottom: 0,
          backgroundColor: '#fff',
          borderRadius: 5,
        }}>
        <View style={{flex: 1}}>
          <Icon name="book" size={40} />
        </View>
        <View style={{flex: 5, flexDirection: 'column'}}>
          <Text numberOfLines={1} style={{fontSize: 17, fontWeight: 'bold'}}>
            Worksheets
          </Text>
          <Text>{updated_date}</Text>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('MobileWorkRecordSheet', {
                id: id,
                button: 0,
              })
            }>
            <Icon name="eye" size={25} color="#F8981D" />
          </TouchableOpacity>
        </View>
      </View>
    );
  } 
  render() {
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Work Sheet Record List"
          goBack={() => this.props.navigation.goBack()}
        />

        <FlatList
          data={this.state.data}
          renderItem={(item) => this.renderItems(item)}
        />

        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
