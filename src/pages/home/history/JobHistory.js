import React, {Component} from 'react';
import {Text, StyleSheet, View, ScrollView, FlatList} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import JobHistoryContentBox from '../../../component/JobHistoryContentBox';
import Loader from '../../../WebAPI/Loader';
import {completed_jobs, requestGetApi} from '../../../WebAPI/Service';

import KeyStore from '../../KeyStore/LocalKeyStore';
export default class JobHistory extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      data: [],
    };
  }

  async componentDidMount() {
    this.setState({loading: true});

    KeyStore.getKey('data', async (err, value) => {
      if (value) {
        const {user_id} = JSON.parse(value);
        const body = {
          operator_id: user_id,
        };
        const {responseJson, err} = await requestGetApi(
          completed_jobs,
          body,
          'GET',
        );
        this.setState({loading: false})
        if (responseJson.status) {
          this.setState({loading: false, data: responseJson.data1});
        }
      } 
    });
  }

  render() {
    console.warn(this.state.data);
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Jobs"
          goBack={() => this.props.navigation.goBack()}
        />

        <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
          <FlatList
            data={this.state.data}
            renderItem={(item) => this.renderItems(item)}
          />
        </ScrollView>

        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }

  renderItems(item) {
    const {q_no, date_tpra, finish_date} = item.item;
    return (
      <JobHistoryContentBox
        key={q_no}
        jobId={q_no}
        startDate={date_tpra}
        endDate={finish_date}
      />
    );
  }
}

const styles = StyleSheet.create({});
