import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  FlatList,Alert,ScrollView,Platform
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import {get_holding_jobs_data,requestGetApi,delete_user_jobs,requestPostApiMedia,job_final_submit,job_data_ById} from '../../WebAPI/Service'
import Loader from '../../WebAPI/Loader'
import TouchableButton from '../../component/TouchableButton';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

import KeyStore from '../KeyStore/LocalKeyStore'
export default class MobileWorkRecordSheet extends Component {
  constructor(props) {
    super();
    this.state = {
      open: false,
      imageDatas: '',
      loading:false,
      getData:[],
      user_id:'',
      notes:'',
      user_name:'',
      signature:'',
      id:'',
      buttonVisible:1,
      image1:'',
      image2:'',
      image3:'',
      image4:'',
      image5:'',
      edit:''
    };
  }

async getHoldingJobData(){
  
  const body ={
    user_id:this.state.user_id
    
  }
  const {responseJson,err} = await requestGetApi(get_holding_jobs_data,body,'GET')

return responseJson

}

async componentDidMount() {
  this.setState({loading:true})

  KeyStore.getKey('data', async(err, value) => {
    if (value) {
      const {user_id}= JSON.parse(value)
      this.setState({user_id,buttonVisible:this.props.navigation.state.params ? this.props.navigation.state.params.button : 1,id:this.props.navigation.state.params ?this.props.navigation.state.params.id:null})
      //console.warn(info)
      
    if(this.state.id === null){
      console.warn("null",this.state.id)
    
    
    
      // const body ={
      //   user_id:user_id
        
      // }
    
    
      // const {responseJson,err} = await requestGetApi(get_holding_jobs_data,body,'GET')
    
const HoldingJobs = await this.getHoldingJobData()

    if(HoldingJobs.status){
    console.warn(HoldingJobs)
      const {all_holding_job} = HoldingJobs
     
      this.setState({loading:false,getData:all_holding_job})
      console.log("----------------",typeof(getData))
    }else{
      this.setState({loading:false})
    }
    
    }else{
      console.warn("not null",this.state.id)
    
      const body ={
        user_id:user_id,
        status:1,
        id:this.state.id
        
      }
    
    
      const {responseJson,err} = await requestGetApi(job_data_ById,body,'GET')
    
    if(responseJson.status){
    console.warn(responseJson)
      const {sub_job,master_job_record} = responseJson
      const {notes,user_name,signature,submission_date,updated_date} = master_job_record
      this.setState({loading:false,getData:sub_job,notes,user_name,signature})
    }else{
      this.setState({loading:false})
    }
    
    
    }
    
    
    
    
    
    
    
    }else{
    }
  });



}

async onEdit(id,user_id){
 
  Alert.alert(
    '',
    'Are you sure you want to edit this timesheet ?',
    [
      {text: 'No', style: 'cancel'},
      {text: 'Yes', onPress: async()=>{
        
  this.props.navigation.navigate('AddTimesheetForWork',{
    id,
    user_id
  })
      } },
    ]
    );
}

async onDelete(id){

  const formData = new FormData()
  formData.append('user_id',this.state.user_id)
  formData.append('id',id)

Alert.alert(
'',
'Are you sure you want to delete this timesheet ?',
[
  {text: 'No', style: 'cancel'},
  {text: 'Yes', onPress: async()=>{
    
this.setState({loading:true})
    const {responseJson} = await requestPostApiMedia(delete_user_jobs,formData,'POST')
    console.warn(this.props)
console.warn("delete ::::",responseJson)
    if (responseJson.status){
      this.setState({loading:false})
    
      const HoldingJobs = await this.getHoldingJobData()

      if(HoldingJobs.status){
      console.warn("====",HoldingJobs)
        const {all_holding_job} = HoldingJobs
       
        this.setState({loading:false,getData:all_holding_job,})
        console.log("----------------",all_holding_job)
      }else{
        this.setState({loading:false})
      }
    
      //  this.props.navigation.replace('MobileWorkRecordSheet')
    }
  } },
]
);


}

async onSubmit(){
const {notes,user_name,signature,user_id,imageDatas} = this.state
this.setState({loading:true})
var fileName = Math.floor(Math.random() * 100) + 1 ;

const formData = new FormData()
formData.append('user_id',user_id)
 formData.append('notes',notes)
 formData.append('user_name',user_name)
  formData.append('signature',{
    uri: Platform.OS === "android" ?  this.state.imageDatas :  this.state.imageDatas.replace("file://", ""),
    type: 'image/png',
    name: fileName+'.png',
  })

console.log("-----------------",formData)
// formData.append('signature',imageDatas.path ? {
//   uri: `file://${imageDatas.path}`,
  
//   name: img_name
// }:null)



const {responseJson,err} = await requestPostApiMedia(job_final_submit,formData,'POST')

if(responseJson){
  Alert.alert('',responseJson.message,
  [
    {text: 'OK', onPress: ()=>
        this.props.navigation.navigate('Tab')
     },
  ]
  )
  console.warn(responseJson)
  this.setState({loading:false})
}else{
  Alert.alert(err)
  this.setState({loading:false})
}
}
  
passImage(data) {
  this.setState({imageDatas: data});
}
  openTimesheet = () => {
    if(this.state.buttonVisible ===1){
      this.setState({open: true});

    }
  };  

  closeTimesheet() {
    if(this.state.edit == '')
    {
    
    this.setState({open: false});
    }
  };
  render() {
const {notes,user_name,signature} = this.state
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Work Sheet Record"
          goBack={() => this.props.navigation.goBack()}
        />
<ScrollView>
        <View style={{margin: 10}}>
          <View>
            <Text style={{fontSize: 17, fontWeight: 'bold'}}>
              TM Site Details
            </Text>

            <View style={{borderWidth: 0.6, borderColor: '#F8981D'}}>
              <View
                style={{
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 0.6}}>
                  <View
                    style={{
                      borderRadius: 50,
                      backgroundColor: '#403EB6',
                      padding: 5,
                      height: 20,
                      width: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity onPress={this.openTimesheet}>
                      <Icon size={13} name="plus" color="#fff" />
                    </TouchableOpacity>
                  </View>
                </View>
                <View
                  style={{
                    flex: 8,
                    backgroundColor: '',
                    justifyContent: 'center',
                  }}>
                  <Text
                    onPress={this.openTimesheet}
                    style={{color: '#403EB6', fontSize: 16, marginLeft: 5}}>
                    Add Items
                  </Text>
                </View>
              </View>

              {
  this.state.getData.length>0 ? (
    <FlatList
    data = {this.state.getData}
    renderItem={(item)=>this.renderItem(item)}
    />
  ) :null
}

              {this.state.open ? (
                <View
                  style={{
                    backgroundColor: '#E5E1E0',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    padding: 10,
                    marginLeft: 10,
                    marginRight: 10,
                    marginBottom: 10,
                    elevation: 9,
                  }}>
                  <View style={{flex: 5,backgroundColor:''}}>
                    <Text
                      onPress={() =>
                        this.props.navigation.navigate('AddTimesheetForWork')
                      }
                      style={{fontSize: 16}}>
                      Click here to add timesheet
                    </Text>
                  </View>
                  <View style={{flex: 1,backgroundColor:'',}}>
                    
                    <TouchableOpacity 
                    style={{justifyContent:'center',alignItems:'center'}}
                    onPress={()=>this.closeTimesheet()}>
                      <Icon name="times-circle" size={25} />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}
            </View>
          </View>

          <View>
            <Text style={{fontSize: 17, fontWeight: 'bold'}}>
              Additional Note
            </Text>
            <TextInput
              placeholder="Additional Note"
              value={notes}
              onChangeText={notes=>this.setState({notes})}
              style={{
                borderWidth: 0.6,
                borderColor: '#F8981D',
                padding: 5,
                marginTop: 5,
                marginBottom: 5,
                fontSize: 16,
                color: '#757575',
              }}
            />
          </View>

          <View>
            <Text style={{fontSize: 17, fontWeight: 'bold'}}>
              Name of Customer Site Agent
            </Text>
            <TextInput
              placeholder="Enter Name of Customer Site Agent"
              value={user_name}
              onChangeText={user_name=>this.setState({user_name})}
              style={{
                borderWidth: 0.6,
                borderColor: '#F8981D',
                padding: 5,
                marginTop: 5,
                marginBottom: 5,
                fontSize: 16,
                color: '#757575',
              }}
            />
          </View>

          <View>
            <Text style={{fontSize: 17, fontWeight: 'bold'}}>
              Customer Signature
            </Text>
            <Text style={{fontSize: 13, fontWeight: '600'}}>
              Click plus icon to add signature
            </Text>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#F8981D',
                padding: 10,
                marginTop: 5,
                marginBottom: 5,
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.state.buttonVisible ===1
                  ?
                  this.props.navigation.navigate('Signature', {
                    reload: this.passImage.bind(this),
                  })
                  :
                  null
                  
                }>
                <View
                  style={{
                    padding: 10,
                    backgroundColor: '#403EB6',
                    borderRadius: 50,
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon size={25} name="plus" color="#fff" />
                </View>
              </TouchableOpacity>

              <View
                style={{
                  padding: 10,
                  height: 60,
                  width: 60,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                

{this.state.imageDatas != '' ? (
                    <Image
                      source={{
                        uri: this.state.imageDatas}}
                      style={{height: 60, width: 40}}
                    />
                  ) : (
                    <Icon size={40} name="pencil" color="black" />
                  )}
              </View>
            </View>

            {
             this.state.buttonVisible ===1?
             <TouchableButton
             title="SUBMIT"
             onPress={() => this.onSubmit()}
           />:null
           }

          </View>
        </View>

   
        </ScrollView>
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }





  renderItem(item){
    const {id,q_number,date_and_time,status} = item.item
    const user_id=this.state.user_id
    console.warn(id)
      return <View key={id}>
        <View  style={{flexDirection:'row',backgroundColor:'#F8981D',padding: 13,marginHorizontal:10,marginBottom:15}}>
    <View style={{flex:1,justifyContent:'center'}}>
    <Text style={{fontSize: 16,color:'#fff'}}>Date:</Text>
    </View>
    <View style={{flex:3,backgroundColor:"",justifyContent:'center'}}>
    <Text style={{fontSize: 15,color:'#fff',textAlign:'center'}}>{date_and_time}</Text>
    </View>
    <View style={{flex:3,justifyContent:'space-around',flexDirection:'row',alignItems:'center'}}>
    
    {
    this.state.buttonVisible ===1
    ?(
    <TouchableOpacity onPress={()=>this.onDelete(id)}>
    <Icon
    name="trash"
    size={20}
    color="white"
    />
    </TouchableOpacity>):null}
    {
    this.state.buttonVisible ===1
    ?
    <TouchableOpacity onPress={()=>this.onEdit(id,user_id)}>
    <MaterialIcons
    name="edit"
    size={20}
    color="white"
    />
    </TouchableOpacity>:null}
    <TouchableOpacity style={{height:18,width:18,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
    
    <TouchableOpacity>
    <Icon
    name="check"
    size={17}
    color="#F8981D"
    />
    
    </TouchableOpacity>
    
    </TouchableOpacity>
    
    <TouchableOpacity style={{padding:3,backgroundColor:'#fff'}}>
    <View >
    
    <Text style={{color:'red',fontSize:9,fontWeight:'bold'}}>{status === "1"  ? <Text style={{color:'green',fontSize:9,fontWeight:'bold'}}>Completed</Text> :<Text style={{color:'red',fontSize:9,fontWeight:'bold'}}>In-completed</Text> }</Text>
    </View>
    
    </TouchableOpacity>
 
    </View>
    
 






      </View>  

      </View>
    }



}

const styles = StyleSheet.create({});
