import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,  Alert
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import RadioButton from 'react-native-radio-button';
import RadioGroup from 'react-native-radio-button-group';
import TouchableButton from '../../component/TouchableButton';
// import {ScrollView} from 'react-native-gesture-handler'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

export default class AddInspection extends Component {
  constructor(props) {
    super();
    this.state = {
      testing:{
name:"rahul"
      },
      value1:'',
      light:'',
        horm:'', breaks:'', steering:'', clutch:'', tyres:'', wheel:'', reverse_camera:'', vehicle_pack:'', windscreen:'', interior_condition:'', tail_lift:'', mirrors:'', engine_oil:'', beacon:'', work_lamp:'', reverse_beeper:'', rachet_strap:'', number_plate:'', exhaust:'', speedometer:'', body_work:'',spare_wheel:'', fire_extinguisher:'', first_aid_kit:'', side_bar:'', hand_wipes:'', blue_roll_or_towel:'',stage_hand_cream:'', others:'',completeData:{},buttonVisible:1
    };
  }

  componentWillUnmount() {
    if (Object.keys(this.state.completeData).length != 0) {
      this.props.navigation.state.params.reload(this.state.completeData)

    }
  }


onSubmit(){

  const len = Object.keys(this.props.navigation.state.params.getInspectionData).length
  const {light,
    horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others}=this.state

 const  datas={
    light,
    horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others
  }

  if(this.state.others == '' || this.state.others == ' '){
    Alert.alert('','Please add other field.')
  }else{
    this.setState({completeData:datas})
    this.props.navigation.navigate('DriverVehInsRep',len > 0 ? null:{message:'Submitted'})
  
  }
}

async componentDidMount() {
const len = Object.keys(this.props.navigation.state.params.getInspectionData).length
console.warn("mmmm",len)
if(len>0){
console.warn("boooo",this.props.navigation.state.params)
const {light,
  horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others} = this.props.navigation.state.params.getInspectionData
  

  this.setState({light,
    horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others,buttonVisible:this.props.navigation.state.params.buttonVisible})
}else{
  console.warn("no",this.state.light)
  const {light,
    horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others} = this.state
  this.setState({light:'2',
    horm:'2', breaks:'2' , steering:'2', clutch:'2', tyres:'2', wheel:'2', reverse_camera:'2', vehicle_pack:'2', windscreen:'2', interior_condition:'2', tail_lift:'2', mirrors:'2', engine_oil:'2', beacon:'2', work_lamp:'2', reverse_beeper:'2', rachet_strap:'2', number_plate:'2', exhaust:'2', speedometer:'2', body_work:'2',spare_wheel:'2', fire_extinguisher:'2', first_aid_kit:'2', side_bar:'2', hand_wipes:'2', blue_roll_or_towel:'2',stage_hand_cream:'2'})
}


}

  render() {
  
    const {light,
      horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others,buttonVisible}=this.state
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        <HeaderWithBack
          title="Inspection"
          goBack={() => this.props.navigation.goBack()}
        />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              backgroundColor: '#fff',
              margin: 6,
              elevation: 10,
              padding: 10,
            }}>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707',}}>Light</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({light: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.light ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({light: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.light === '2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>

            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Horn</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({horm: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.horm ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({horm: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.horm ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>

            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Breaks</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({breaks: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.breaks ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({breaks: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.breaks ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Steering</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({steering: '1'})}>
              <View style={styles.outerCircle}>
                {steering ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({steering: '2'})}>
              <View style={styles.outerCircle}>
                {steering ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Clutch</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({clutch: '1'})}>
              <View style={styles.outerCircle}>
                {clutch ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({clutch: '2'})}>
              <View style={styles.outerCircle}>
                {clutch ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Tyres</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({tyres: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.tyres ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({tyres: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.tyres ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Wheels</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({wheel: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.wheel ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({wheel: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.wheel ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                Reverse Camera
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({reverse_camera: '1'})}>
              <View style={styles.outerCircle}>
                {reverse_camera ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({reverse_camera: '2'})}>
              <View style={styles.outerCircle}>
                {reverse_camera ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Vehicle Park</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({vehicle_pack: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.vehicle_pack ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({vehicle_pack: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.vehicle_pack ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Wind Screen</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({windscreen: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.windscreen ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({windscreen: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.windscreen ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                Interior Condition
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({interior_condition: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.interior_condition ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({interior_condition:'2'})}>
              <View style={styles.outerCircle}>
                {this.state.interior_condition ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Tail-Lift</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({tail_lift: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.tail_lift ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({tail_lift: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.tail_lift ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Mirrors</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({mirrors: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.mirrors ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({mirrors: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.mirrors ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Engine Oil</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({engine_oil: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.engine_oil ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({engine_oil: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.engine_oil ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Beacon</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({beacon: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.beacon ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({beacon: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.beacon ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Work lamps</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({work_lamp: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.work_lamp ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({work_lamp: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.work_lamp ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                Reverse Beeper
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({reverse_beeper: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.reverse_beeper ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({reverse_beeper: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.reverse_beeper ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Rachet Strap</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({rachet_strap: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.rachet_strap ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({rachet_strap: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.rachet_strap ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Number Plate</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({number_plate: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.number_plate ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({number_plate: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.number_plate ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Exhaust</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({exhaust: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.exhaust ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({exhaust: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.exhaust ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Speedometer</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({speedometer: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.speedometer ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({speedometer: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.speedometer ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Bodywork</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({body_work: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.body_work ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({body_work: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.body_work ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Spare Wheel</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({spare_wheel: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.spare_wheel ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({spare_wheel: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.spare_wheel ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                Fire Extinguisher
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({fire_extinguisher: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.fire_extinguisher ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({fire_extinguisher: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.fire_extinguisher ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                First Aid Kit
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({first_aid_kit: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.first_aid_kit ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({first_aid_kit: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.first_aid_kit ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Side Bars</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({side_bar: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.side_bar ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({side_bar: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.side_bar ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Hand Wipes</Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({hand_wipes: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.hand_wipes ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({hand_wipes: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.hand_wipes ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                Blue Roll/ Hand Towel
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({blue_roll_or_towel:'1'})}>
              <View style={styles.outerCircle}>
                {this.state.blue_roll_or_towel ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({blue_roll_or_towel: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.blue_roll_or_towel ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>
                3 Stage Hand Cream
              </Text>

              <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({stage_hand_cream: '1'})}>
              <View style={styles.outerCircle}>
                {this.state.stage_hand_cream ==='1' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({stage_hand_cream: '2'})}>
              <View style={styles.outerCircle}>
                {this.state.stage_hand_cream ==='2' && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View
              style={{
                borderWidth: 0.6,
                borderColor: '#000',
                padding: 10,
                margin: 4,
                marginBottom: 10,
              }}>
              <Text style={{fontSize: 17, color: '#070707'}}>Others</Text>

              <TextInput
                placeholder="Enter Defect Infromation"
                numberOfLines={4}
                value={others}
                onChangeText={others=>this.setState({others})}
                multiline={true}
                style={{
                  borderWidth: 0.6,
                  paddingTop: 0,
                  marginTop: 5,
                  paddingBottom: 10,
                }}
              />
            </View>

           {
             buttonVisible ===1 ?
             <TouchableButton
             title="SUBMIT"
             onPress={() => this.onSubmit()}
           />:null
           }
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  innerCircle:{height:13,width:13,backgroundColor:'#00aaff',borderRadius:100,borderWidth:1,borderColor:'#00aaff'},
  outerCircle:{height:20,width:20,borderWidth:1,borderRadius:100,justifyContent:'center',alignItems:'center',marginRight:20},
  label:{fontSize:15},
  container:{flexDirection:'row',justifyContent:'space-around',marginLeft:10,marginVertical:10},
  radioBox:{flexDirection:'row',alignItems:'center',flex:1}
})
