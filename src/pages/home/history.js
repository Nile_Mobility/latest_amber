import React, {Component} from 'react';
import {Text, StyleSheet, View,ScrollView} from 'react-native';
import Header from '../../component/Header';
import HistoryContentBox from '../../component/HistoryContentBox';
import Loader from '../../WebAPI/Loader'
import {requestGetApi,get_userhistory} from '../../WebAPI/Service'
import AsyncStorage from '@react-native-community/async-storage';
import KeyStore from '../KeyStore/LocalKeyStore'
export default class History extends Component {

  constructor(){
    super()
    this.state={
      loading:false,
      data:[]
    }
  }

async componentDidMount(){
 

  KeyStore.getKey('data', async(err, value) => {
    if (value) {
      const {user_id}= JSON.parse(value)
      this.setState({loading:true})
      const body ={
        user_id:user_id,
        status:1
      }
      console.warn(body)
    const {responseJson,err} = await requestGetApi(get_userhistory,body,'GET')
    
    if(responseJson.status){
      console.warn(responseJson)
      this.setState({loading:false,data:responseJson})
    }else{
      this.setState({loading:false})
    }

    }else{
    }
  });
}

render() {
    return (
      <View style={{flex: 1, backgroundColor: '#E5E1E0'}}>
        <Header title="HISTORY"
        onPress={async()=>{this.props.navigation.navigate("Login")
        KeyStore.setKey('data',null)
      }}
        />
<ScrollView showsVerticalScrollIndicator={false}>

{
  this.state.data.jobs >0?
<HistoryContentBox
iconName="list-alt"
title="Jobs"
notification={this.state.data.jobs}
onPress={()=>this.props.navigation.navigate("JobHistory")}
/>:null
}



{
  this.state.data.vehicle_inspection>0?
<HistoryContentBox
iconName="truck"
title="Vehicle Inspection Report"
notification={this.state.data.vehicle_inspection}
onPress={()=>this.props.navigation.navigate("VehicleInspectionList")}
/>:null
}


{
  this.state.data.timesheet >0?
  <HistoryContentBox
  iconName="calendar"
  title="Timesheet"
  notification={this.state.data.timesheet}
  onPress={()=>this.props.navigation.navigate("TimesheetHis")}
  />:null
}

{
  this.state.data.user_jobs>0?
  <HistoryContentBox
 iconName="book"
title="Mobile Work Record Sheet"
notification={this.state.data.user_jobs}
onPress={()=>this.props.navigation.navigate("WorksheetHistory")}/>:null
}

</ScrollView>
       
<Loader isLoader={this.state.loading}></Loader>


      </View>
    );
  }
}

const styles = StyleSheet.create({});
