import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  AsyncStorage,
  Modal,
  Image,
  ImageBackground,
  BackHandler,
  Platform,
  TextInput,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import TabBottom from '../../component/TabBottom';
import Home from './home';
import History from './history';
import Profile from './profile';

import {NavigationActions, StackActions} from 'react-navigation';

const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({});

export default class TabBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tabArr: [
        {
          tabName: 'HOME',
          tabImg: 'home',
          isSelect: true,
          tabAction: '',
        },
        {tabName: 'HISTORY', tabImg: 'history', isSelect: false, tabAction: ''},
        {tabName: 'PROFILE', tabImg: 'user', isSelect: false, tabAction: ''},
      ],
      selectedTabTitle: 'Home',
    };
  }

  static navigationOptions = {
    headerShown: false,
    gesturesEnabled: false,
    disableGestures: true,
  };

  componentDidMount() {}

  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick = () => {
    //this.props.navigation.isFocused() helps to exit the app in one screen not in the whole
    if (this.props.navigation.isFocused()) {
        Alert.alert(
            '',
            'Do you want to close app?',
            [
                {
                    text:'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                },
                {
                    text: 'Yes',
                    onPress: () => BackHandler.exitApp()
                }
            ],
            { cancelable: false },
        );
        return true;
    }
  }

  render() {
    const {goBack} = this.props.navigation;
    return (
      <View>
        <StatusBar backgroundColor="#F8981D" />
        <View style={{height: windowHeight - 75, width: '100%', marginTop: 1}}>
          {this.state.tabArr[0].isSelect ? (
            <Home navigation={this.props.navigation}></Home>
          ) : this.state.tabArr[1].isSelect ? (
            <History navigation={this.props.navigation}></History>
          ) : (
            <Profile navigation={this.props.navigation}></Profile>
          )}
        </View>

        <TabBottom
          tabArr={this.state.tabArr}
          action={this.tabAction.bind(this)}></TabBottom>
      </View>
    );
  }

  tabAction = (index) => {
    let arr = [];
    const map = this.state.tabArr.map((item, index) => {
      item.isSelect = false;
      arr.push(item);
    });
    arr[index].isSelect = true;
    this.setState({
      tabArr: arr,
      selectedTabTitle: this.state.tabArr[index].tabName,
    });
  };
}
