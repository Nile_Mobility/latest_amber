import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Alert,
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import DeviceInfo from 'react-native-device-info';
import TouchableButton from '../../component/TouchableButton';
import Loader from '../../WebAPI/Loader.js';
import {
  get_total_timesheet_data,
  requestGetApi,
  delete_timesheetById,
  requestPostApiMedia,
  timesheet_final_submit,
  get_final_submitted_timesheet_data_ById,
} from '../../WebAPI/Service';
import { NavigationEvents } from 'react-navigation';
import KeyStore from '../KeyStore/LocalKeyStore';
export default class Timesheet extends Component {
  constructor(props) {
    super();
    this.state = {
      deviceId: DeviceInfo.getDeviceId(),
      open: false,
      timesheet: '',
      getData: [],
      loading: false,
      emp_name: '',
      hour_worked: '',
      total_break: '',
      total_working_hour: '',
      total_sift: '',
      user_id: '',
      id: '',
      buttonVisible: 1,
      alertStatus:false
    };
  }
  passData(data) {
    console.log('testingggggggggggggggggggggggg', data);
    this.setState({timesheet: data});
  }
  openTimesheet = () => {
    this.setState({open: true});
  };
  closeTimesheet = () => {
    this.setState({open: false});
  };




async getTotalTimesheetData(){
  const body = {
    user_id: this.state.user_id,
    status: '2',
  };

  const {responseJson, err} = await requestGetApi(
    get_total_timesheet_data,
    body,
    'GET',
  );

  return responseJson
}

  async componentDidMount() {
    

    KeyStore.getKey('data', async (err, value) => {
      if (value) {
        const {user_id, name} = JSON.parse(value);

       
        this.setState({
          user_id,
          emp_name: name,
          buttonVisible: this.props.navigation.state.params
            ? this.props.navigation.state.params.button
            : 1,
          id: this.props.navigation.state.params
            ? this.props.navigation.state.params.id
            : null,
        });
        //console.warn(info)

        if (this.state.id === null) {
          console.warn('null', this.state.id);
          this.setState({loading: true});
          const body = {
            user_id: user_id,
            status: '2',
          };

          // const {responseJson, err} = await requestGetApi(
          //   get_total_timesheet_data,
          //   body,
          //   'GET',
          // );

          const totalTimesheet = await this.getTotalTimesheetData()
          console.warn('dataaaaaaaaaaaaaaaaaa', totalTimesheet);
          if (totalTimesheet.status) {
            const {
              total_sift,
              hour_worked,
              total_break,
              total_working_hour,
            } = totalTimesheet.data;
            if(totalTimesheet.data){
              this.setState({
                loading: false,
                getData: totalTimesheet.timesheet_obj,
                total_working_hour,
                total_sift,
                total_break,
                hour_worked,
                alertStatus:true
              });
            }
          
          } else {
            this.setState({loading: false,alertStatus:false});
            // Alert.alert('hey',err)
          }
        } else {
          console.warn('not null', this.state.id);

          const body = {
            user_id: user_id,
            status: 1,
            id: this.state.id,
          };

          const {responseJson, err} = await requestGetApi(
            get_final_submitted_timesheet_data_ById,
            body,
            'GET',
          );
          console.warn('data', responseJson);
          if (responseJson.status) {
            const {
              total_sift,
              hour_worked,
              total_break,
              total_working_hour,
            } = responseJson.data;
            this.setState({
              loading: false,
              getData: responseJson.timesheet_obj,
              total_working_hour,
              total_sift,
              total_break,
              hour_worked,
            });
          } else {
            this.setState({loading: false});
            // Alert.alert('hey',err)
          }
        }
      } else {
      }
    });
  }

  renderItem(item) {
    const {
      id,
      leave_date,
      date_and_time_on_site,
      date_and_time_off_site,
      leave_type,
    } = item.item;
    console.warn(id);
    return (
      <View key={id}>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#F8981D',
            padding: 13,
            marginHorizontal: 10,
            marginBottom: 15,
          }}>
          <View style={{flex: 1, justifyContent: 'center'}}>
            <Text style={{fontSize: 16, color: '#fff'}}>Date:</Text>
          </View>
          <View
            style={{flex: 3, backgroundColor: '', justifyContent: 'center'}}>
            <Text style={{fontSize: 15, color: '#fff', textAlign: 'center'}}>
              {leave_date == 'on_site' ? (
                <Text
                  style={{
                    fontSize: 14,
                  }}>{`${date_and_time_on_site}\n to \n ${date_and_time_off_site}`}</Text>
              ) : (
                leave_date
              )}
            </Text>
          </View>
          <View
            style={{
              flex: 3,
              justifyContent: 'space-around',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={() => this.onDelete(id)}>
              <Icon name="trash" size={20} color="white" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onEdit(id)}>
              <MaterialIcons name="edit" size={20} color="white" />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: 18,
                width: 18,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#fff',
              }}>
              <TouchableOpacity>
                <Icon name="check" size={17} color="#F8981D" />
              </TouchableOpacity>
            </TouchableOpacity>

            <TouchableOpacity style={{padding: 3, backgroundColor: '#fff'}}>
              <View>
                <Text style={{color: 'red', fontSize: 9, fontWeight: 'bold'}}>
                  {leave_type === '1' || leave_type === '2' ? (
                    'ON LEAVE'
                  ) : (
                    <Text
                      style={{color: 'green', fontSize: 9, fontWeight: 'bold'}}>
                      ON WORK
                    </Text>
                  )}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

async onEdit(id){
  const formData = new FormData();
  formData.append('user_id', this.state.user_id);
  formData.append('id', id);


  Alert.alert('', 'Are you sure you want to edit this timesheet ?', [
    {text: 'No', style: 'cancel'},
    {
      text: 'Yes',
      onPress: async () => {
       this.props.navigation.navigate('AddTimesheet',{
         user_id:this.state.user_id,
         id:id
       })
      },
    },
  ]);
}

  async onDelete(id) {
    const formData = new FormData();
    formData.append('user_id', this.state.user_id);
    formData.append('id', id);

    Alert.alert('', 'Are you sure you want to delete this timesheet ?', [
      {text: 'No', style: 'cancel'},
      {
        text: 'Yes',
        onPress: async () => {
          this.setState({loading: true});
          const {responseJson} = await requestPostApiMedia(
            delete_timesheetById,
            formData,
            'POST',
          );
          console.warn(this.props);
          console.warn('delete ::::', responseJson);
          if (responseJson.status) {
            this.setState({loading: false});
            // this.props.navigation.replace('Timesheet')
            const totalTimesheet = await this.getTotalTimesheetData()
            console.warn('dataaaaaaaaaaaaaaaaaa', totalTimesheet);
            if (totalTimesheet.status) {
              const {
                total_sift,
                hour_worked,
                total_break,
                total_working_hour,
              } = totalTimesheet.data;
              if(totalTimesheet.data){
                this.setState({
                  loading: false,
                  getData: totalTimesheet.timesheet_obj,
                  total_working_hour,
                  total_sift,
                  total_break,
                  hour_worked,
                  alertStatus:true
                });
              }

              if(this.state.getData.length==0){
                const {
                  emp_name,
                  hour_worked,
                  total_break,
                  total_sift,
                  total_working_hour,
                  deviceId,
                } = this.state;
                this.setState({hour_worked:'',total_break:'',total_sift:'',total_working_hour:'',})
                
              }
            }
          }
        },
      },
    ]);
  }

  async onSubmit() {
    console.warn("giiiiii",)
    const {
      emp_name,
      hour_worked,
      total_break,
      total_working_hour,
      total_sift,
      deviceId,
      user_id,
    } = this.state;
    this.setState({loading: true});

    if(this.state.alertStatus){
      const formData = new FormData();
      formData.append('user_id', user_id);
      formData.append('emp_name', emp_name);
      formData.append('total_regular_hour', hour_worked);
      formData.append('total_break_hour', total_break);
      formData.append('total_working_hour', total_working_hour);
      formData.append('total_shift', total_sift);
      formData.append('device_id', deviceId);
      console.warn(formData);
      const {responseJson, err} = await requestPostApiMedia(
        timesheet_final_submit,
        formData,
        'POST',
      );
  
      if (responseJson.status) {
        this.setState({loading: false});
        Alert.alert('', responseJson.message, [
          {
            text: 'Ok',
            onPress: async () => {
              this.props.navigation.navigate('Tab');
            },
          },
        ]);
      } else {
        this.setState({loading: false});
        alert(responseJson.message);
      }
    }else{
Alert.alert('','Timesheet Must be added !')
this.setState({loading:false})
    }
    
  }
  render() {
    console.warn(this.state.getData);
    const {
      emp_name,
      hour_worked,
      total_break,
      total_sift,
      total_working_hour,
      deviceId,
    } = this.state;
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        <HeaderWithBack
          title="Timesheet"
          goBack={() => this.props.navigation.goBack()}
        />
        <View
          style={{
            backgroundColor: '#fff',
            margin: 6,
            elevation: 10,
            flex: 1,
            padding: 10,
          }}>

<NavigationEvents
                onDidFocus={() => console.warn('Refreshed')}
                />
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Employee Name
              </Text>
              <TextInput
                placeholder="Employee Name"
                value={emp_name}
                onChangeText={(emp_name) => this.setState({emp_name})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Timesheet</Text>

              <View style={{borderWidth: 0.6, borderColor: '#F8981D'}}>
                <View
                  style={{
                    padding: 7,
                    marginTop: 5,
                    marginBottom: 5,
                    flexDirection: 'row',
                    backgroundColor: '',
                  }}>
                  <View style={{flex: 0.6}}>
                    <View
                      style={{
                        borderRadius: 50,
                        backgroundColor: '#403EB6',
                        padding: 5,
                        height: 20,
                        width: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <TouchableOpacity onPress={this.openTimesheet}>
                        <Icon size={13} name="plus" color="#fff" />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 8,
                      backgroundColor: '',
                      justifyContent: 'center',
                    }}>
                    <Text
                      onPress={this.openTimesheet}
                      style={{color: '#403EB6', fontSize: 16, marginLeft: 5}}>
                      Add Items
                    </Text>
                  </View>
                </View>

                {/* {
  this.state.timesheet ===1 ?
  null:null
} */}

                {this.state.getData.length > 0 ? (
                  <FlatList
                    data={this.state.getData}
                    renderItem={(item) => this.renderItem(item)}
                  />
                ) : null}
                {this.state.open ? (
                  <View
                    style={{
                      backgroundColor: '#E5E1E0',
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1,
                      padding: 10,
                      marginLeft: 10,
                      marginRight: 10,
                      marginBottom: 10,
                      elevation: 9,
                    }}>
                    <View style={{flex: 5}}>
                      <Text
                        onPress={() =>
                          this.props.navigation.navigate('AddTimesheet', {
                           // reload: this.passData.bind(this),
                          })
                        }
                        style={{fontSize: 16}}>
                        Click here to add timesheet
                      </Text>
                    </View>
                    <View style={{flex: 1}}>
                      <TouchableOpacity onPress={this.closeTimesheet}>
                        <Icon name="times-circle" size={25} />
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : null}
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Total Regular Hours(HH:mm)
              </Text>

              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginVertical: 5,
                  paddingVertical: 10,

                  backgroundColor: '#E5E1E0',
                }}>
                <Text style={{color: '#757575', fontSize: 16}}>
                  {hour_worked}
                </Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Total Hours of Break(HH:mm)
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginVertical: 5,
                  paddingVertical: 10,

                  backgroundColor: '#E5E1E0',
                }}>
                <Text style={{color: '#757575', fontSize: 16}}>
                  {total_break}
                </Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Total Working Hours(HH:mm)
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginVertical: 5,
                  paddingVertical: 10,

                  backgroundColor: '#E5E1E0',
                }}>
                <Text style={{color: '#757575', fontSize: 16}}>
                  {total_working_hour}
                </Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Total Number of shifts
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginVertical: 5,
                  paddingVertical: 10,

                  backgroundColor: '#E5E1E0',
                }}>
                <Text style={{color: '#757575', fontSize: 16}}>
                  {total_sift}
                </Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Device Id</Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginVertical: 5,
                  paddingVertical: 10,

                  backgroundColor: '#E5E1E0',
                }}>
                <Text style={{color: '#757575', fontSize: 16}}>{deviceId}</Text>
              </View>
            </View>

            {this.state.buttonVisible === 1 ? (
              <TouchableButton title="SUBMIT" onPress={() => this.onSubmit()} />
            ) : null}
          </ScrollView>

          <Loader isLoader={this.state.loading}></Loader>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
