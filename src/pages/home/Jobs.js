import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Loader from '../../WebAPI/Loader';
import KeyStore from '../KeyStore/LocalKeyStore';
import {requestGetApi, jobslist} from '../../WebAPI/Service';

class JobContentBox extends Component {
  render() {
    return (
      <View
        style={{
          borderWidth: 3,
          borderColor: '#F8981D',
          margin: 10,
          padding: 10,
          flexDirection: 'row',
          backgroundColor: '#fff',
          borderRadius: 8,
        }}>
        <View style={{flex: 3, paddingLeft: 5, paddingRight: 5}}>
          <View style={{justifyContent: 'space-between', paddingBottom: 15}}>
            <Text style={{fontWeight: 'bold', fontSize: 18}}>Job Title</Text>
            <Text style={{textAlign: 'auto'}}>{this.props.jobtile}</Text>
          </View>
          <View style={{justifyContent: 'space-between'}}>
            <Text style={{fontWeight: 'bold', fontSize: 18}}>Start Date</Text>
            <Text>{this.props.startDate}</Text>
          </View>
        </View>
        <View style={{flex: 2}}>
          <View>
            <View style={{}}>
              <Text style={{fontWeight: 'bold', fontSize: 18}}>Location</Text>
              <Text style={{textAlign: 'auto'}}>{this.props.location}</Text>
            </View>
            <View style={{marginTop: 5}}>
              <Text style={{fontWeight: 'bold', fontSize: 18}}>End Date</Text>
              <Text>{this.props.endDate}</Text>
            </View>
          </View>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity onPress={this.props.onPress}>
            <View
              style={{
                borderRadius: 200,
                width: wp('10%'),
                height: wp('10%'),
                backgroundColor: '#F8981D',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon name="angle-right" size={40} color="#fff" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default class Jobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      jobTitle: '',
      address: '',
      startDate: '',
      endDate: '',
      data: [],
    };
  }

  async componentDidMount() {
    this.setState({loading: true});

    KeyStore.getKey('data', async (err, value) => {
      if (value) {
        const {user_id} = JSON.parse(value);
        const body = {
          operator_id: user_id,
        };

        const {responseJson} = await requestGetApi(jobslist, body, 'GET');
        this.setState({loading: false});
        if (responseJson) {
          this.setState({
            data: responseJson.data1,
            loading: false,
          });
        } else {
        }
      } else {
      }
    });
  }

  renderComponent = ({item}) => {

    return (
      <JobContentBox
        key={item.q_no}
        jobtile={item.name_number}
        startDate={item.date_tpra}
        location={item.site_address}
        endDate={item.finish_date}
        onPress={() =>
          this.props.navigation.navigate('FormsList', {data: item})
        }
      />
    );
  };
  render() {
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Jobs"
          goBack={() => this.props.navigation.goBack()}
        />
  {
    this.state.data
    ?
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderComponent}
        />
:
    <Text style={{fontWeight:'bold',fontSize:18,alignSelf:'center',marginTop:150}}>No Data Found</Text>
}
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
