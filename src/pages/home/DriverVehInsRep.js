import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  Image, 
  TouchableOpacity, Alert,Platform
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import DeviceInfo from 'react-native-device-info';

import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import Signature from '../../component/Signature';
import showDateTimePicker from '../../component/DateTimePickers';
import DateTimePickers from '../../component/DateTimePickers';
import TouchableButton from '../../component/TouchableButton';
import Loader from '../../WebAPI/Loader'
import { vehicle_inspection,requestPostApiMedia,requestGetApi,get_vehicle_inspectionById} from '../../WebAPI/Service'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

import KeyStore from '../KeyStore/LocalKeyStore'
export default class DriverVehInsRep extends Component {
  constructor(props) {
    super();
    this.state = {
      deviceId: DeviceInfo.getDeviceId(),
      imageDatas: '',
      isDateTimePickerVisible: false,
      choosenDateTime: '',
      mode: '',
      choosenDate: '',
      choosenTimeIn: '',
      choosenTimeOut: '',
      status: '',
      buttonVisible:1,
      loading:false,
      name:'',
// vehicle data

id:'',
getInspectionData:{},
//Api state

user_id:'',
 user_name:"" ,
 submission_date:"",
  driver_name:"",
   date:'',
    time_in:'',
     time_out:'',
      reg_no:'',
       mileage_in:'' ,
       mileage_out:'', 
        defect_info:'',
        inspectionData:{},
        errMessage:''
 
    };
  }

async componentDidMount() {
  KeyStore.getKey('data', async(err, value) => {
    if (value) {
      const {user_id,name} = JSON.parse(value);
     console.log(value)
 this.setState({user_id,buttonVisible:this.props.navigation.state.params ? this.props.navigation.state.params.button : 1,id:this.props.navigation.state.params ?this.props.navigation.state.params.id:null ,user_name:name})


 if(this.state.buttonVisible == undefined){
   this.setState({buttonVisible:1})
 }

 const body = {
   user_id:user_id,
   id:this.props.navigation.state.params ?this.props.navigation.state.params.id:null 
 }
 
 if(body.id != null){
  this.setState({loading:true})
 const {responseJson,err} = await requestGetApi(get_vehicle_inspectionById,body,'GET')
 
 console.warn(responseJson)
 if(responseJson.status){
   const {id,user_name,submission_date,driver_name,
     date,
     time_in,
     time_out,
     reg_no,
     mileage_in,
     mileage_out,defect_info,light,
     horm, steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,three_stage_hand_cream, others} = responseJson.vehicle_data
     console.warn("new",responseJson.vehicle_data)
   this.setState({loading:false,user_name,submission_date,driver_name,
     date,
     time_in,
     time_out,
     reg_no,
     mileage_in,
     mileage_out,defect_info})
 
 const getInspectionData ={
   light,
   horm, breaks:responseJson.vehicle_data.break , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream:three_stage_hand_cream, others,
 }
 
 this.setState({inspectionData:getInspectionData})
 
 }
 }else{
   console.warn("null")
 }
 
 console.warn(body)
    } else {
    }
  });
}

componentWillUnmount() {
    this.props.navigation.state.params.reload()
}


validation(){
  const {user_name,submission_date,
    driver_name,
    date,
    time_in,
    time_out,
    reg_no,
    mileage_in,
    mileage_out,defect_info,id,imageDatas} = this.state

    const {light,
      horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp, reverse_beeper, rachet_strap, number_plate, exhaust, speedometer, body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes, blue_roll_or_towel,stage_hand_cream, others} = this.state.inspectionData
console.warn("ddddddddd",light)
if(user_name===''|| submission_date===''||
  driver_name===''||
  date===''||
  time_in===''||
  time_out===''||
  reg_no===''||
  mileage_in===''||
  mileage_out===''||defect_info===''|| typeof(light)==='undefined' || imageDatas == ''){
  if(user_name==''){
    Alert.alert('User Name field is required')
    }else if(submission_date===''){
      Alert.alert('','Submission date field is required')
    }else if(driver_name===''){
      Alert.alert('','Driver name field is required')
    }else if(date===''){
      Alert.alert('','Date field is required')
    }else if(time_in===''){
      Alert.alert('','Time in field is required')
    }else if(time_out===''){
      Alert.alert('','Time out field is required')
    }else if(reg_no===''){
      Alert.alert('','Registration No field is required')
    }else if(mileage_in===''){
      Alert.alert('','Mileage in field is required')
    }else if(mileage_out===''){
      Alert.alert('','Mileage out field is required')
    }else if(defect_info===''){
      Alert.alert('','Defect Info field is required')
     // this.setState({errMessageStatus:false})
    }else if(typeof(light)==='undefined'){
      Alert.alert('',' Please add inspections.')
     // this.setState({errMessageStatus:false})
    }else if(imageDatas == ''){
      Alert.alert('','Please add signature.')
    }
    return false
}else{
return true
}  
}

async onSubmit(){

const {user_id,user_name,submission_date,
  driver_name,
  date,
  time_in,
  time_out,
  reg_no,
  mileage_in,
  mileage_out,defect_info,id} = this.state

const {light,
  horm, breaks , steering, clutch, tyres, wheel, reverse_camera, vehicle_pack, windscreen, 
  interior_condition, tail_lift, mirrors, engine_oil, beacon, work_lamp,
   reverse_beeper, rachet_strap, number_plate, exhaust, speedometer,
    body_work,spare_wheel, fire_extinguisher, first_aid_kit, side_bar, hand_wipes,
     blue_roll_or_towel,stage_hand_cream, others} = this.state.inspectionData
  var fileName = Math.floor(Math.random() * 100) + 1 ;


  if(this.state.id === null || this.state.id === undefined || this.state.id === ''){

if(this.validation()){
  const formData = new FormData()
  formData.append('user_id',user_id) 
  formData.append('user_name',user_name) 
  formData.append('submission_date',submission_date) 
  formData.append('driver_name',driver_name) 
  formData.append('date',date) 
  formData.append('time_in',time_in) 
  formData.append('time_out',time_out) 
  formData.append('reg_no',reg_no) 
  formData.append('mileage_in',mileage_in) 
  formData.append('mileage_out',mileage_out)
  formData.append('defect_info',defect_info)
  formData.append('light',light)
  formData.append('horm',horm)
  formData.append('break',breaks)
  formData.append('steering',steering)
  formData.append('clutch',clutch)
  formData.append('tyres',tyres)
  formData.append('wheel',wheel)
  formData.append('reverse_camera',reverse_camera)
  formData.append('vehicle_pack',vehicle_pack)
  formData.append('windscreen',windscreen)
  formData.append('interior_condition',interior_condition)
  formData.append('tail_lift',tail_lift)
  formData.append('mirrors',mirrors)
  formData.append('engine_oil',engine_oil)
  formData.append('beacon',beacon)
  formData.append('work_lamp',work_lamp)
  formData.append('reverse_beeper',reverse_beeper)
  formData.append('rachet_strap',rachet_strap)
  formData.append('number_plate',number_plate)
  formData.append('exhaust',exhaust)
  formData.append('speedometer',speedometer)
  formData.append('body_work',body_work)
  formData.append('spare_wheel',spare_wheel)
  formData.append('fire_extinguisher',fire_extinguisher)
  formData.append('first_aid_kit',first_aid_kit)
  formData.append('side_bar',side_bar)
  formData.append('hand_wipes',hand_wipes)
  formData.append('blue_roll_or_towel',blue_roll_or_towel)
  formData.append('3_stage_hand_cream',stage_hand_cream)
  formData.append('others',others)

  if(this.state.imageDatas != ''){
    formData.append('driver_sign',{
      uri: Platform.OS === "android" ?  this.state.imageDatas :  this.state.imageDatas.replace("file://", ""),
      type: 'image/png',
      name: fileName+'.png',
    })
  }
  this.setState({loading:true})

    const {responseJson,err} = await requestPostApiMedia(vehicle_inspection,formData,'POST')
    this.setState({loading:false})

    Alert.alert(    
      '',  
      'Data saved successfully',  
      [   
          {text: 'OK', onPress: () => 
          this.props.navigation.goBack()
        },  
      ]  
  );  
}else{
}
}else{
  console.warn("no",this.state.id)

  if(this.validation()){

  const formData = new FormData()
  formData.append('user_id',user_id) 
  formData.append('id',id ) 
  formData.append('user_name',user_name) 
  formData.append('submission_date',submission_date) 
  formData.append('driver_name',driver_name) 
  formData.append('date',date) 
  formData.append('time_in',time_in) 
  formData.append('time_out',time_out) 
  formData.append('reg_no',reg_no) 
  formData.append('mileage_in',mileage_in)      
  formData.append('mileage_out',mileage_out)
  formData.append('defect_info',defect_info)
  formData.append('light',light)
  formData.append('horm',horm)
  formData.append('break',breaks)
  formData.append('steering',steering)
  formData.append('clutch',clutch)
  formData.append('tyres',tyres)
  formData.append('wheel',wheel)
  formData.append('reverse_camera',reverse_camera)
  formData.append('vehicle_pack',vehicle_pack)
  formData.append('windscreen',windscreen)
  formData.append('interior_condition',interior_condition)
  formData.append('tail_lift',tail_lift)
  formData.append('mirrors',mirrors)
  formData.append('engine_oil',engine_oil)
  formData.append('beacon',beacon)
  formData.append('work_lamp',work_lamp)
  formData.append('reverse_beeper',reverse_beeper)
  formData.append('rachet_strap',rachet_strap)
  formData.append('number_plate',number_plate)
  formData.append('exhaust',exhaust)
  formData.append('speedometer',speedometer)
  formData.append('body_work',body_work)
  formData.append('spare_wheel',spare_wheel)
  formData.append('fire_extinguisher',fire_extinguisher)
  formData.append('first_aid_kit',first_aid_kit)
  formData.append('side_bar',side_bar)
  formData.append('hand_wipes',hand_wipes)
  formData.append('blue_roll_or_towel',blue_roll_or_towel)
  formData.append('3_stage_hand_cream',stage_hand_cream)
  formData.append('others',others)
  var fileName = Math.floor(Math.random() * 100) + 1 ;

  if(this.state.imageDatas != ''){
    formData.append('driver_sign',{
      uri: Platform.OS === "android" ?  this.state.imageDatas :  this.state.imageDatas.replace("file://", ""),
      type: 'image/png',
      name: fileName+'.png',
    })
  }
  
  console.warn(formData)
  this.setState({loading:true})

  const {responseJson,err} = await requestPostApiMedia(vehicle_inspection,formData,'POST')
  console.warn(responseJson)
  this.setState({loading:false})

  if (responseJson.status){
    Alert.alert(  
      '',  
      responseJson.message,  
      [   
          {text: 'OK', onPress: () => 
          this.props.navigation.goBack()
        },  
      ]  
  );  
  }
}
}
}

passImage(data) {
    this.setState({imageDatas: data});
  }

  passData(completeData) {
    console.warn('testingggggggggggggggggggggggg', completeData);
    this.setState({inspectionData: completeData});
  }

  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (datetime) => {
    this.setState({isDateTimePickerVisible: false});

    if (this.state.status == 1) {
      this.setState({
        submission_date: moment(datetime).format('MM/DD/YYYY HH:mm'),
      });
    } else if (this.state.status == 2) {
      this.setState({
        date: moment(datetime).format('MM/DD/YYYY'),
      });
    } else if (this.state.status == 3) {
      this.setState({
        time_in: moment(datetime).format('HH:mm'),
      });
    } else {
      this.setState({
        time_out: moment(datetime).format('HH:mm'),
      });
    }
  };

  render() {
    
    const {user_id,user_name,submission_date,
      driver_name,
      date,
      time_in,
      time_out,
      reg_no,
      mileage_in,
      mileage_out,defect_info} = this.state
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        <HeaderWithBack
          title="Driver's Vehicle Inspection"
          goBack={() => this.props.navigation.goBack()}
        />
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              backgroundColor: '#fff',
              margin: 6,
              elevation: 10,
              padding: 10,
            }}>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>User</Text>
              <TextInput
                placeholder="User"
                value={user_name}
                editable={this.state.buttonVisible == 1}
                autoCorrect={false}
                onChangeText={user_name=>this.setState({user_name})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Date/Time</Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16}}>
                    {this.state.submission_date
                      ? this.state.submission_date
                      : 'Select Date & Time'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'datetime',
                        status: 1,
                      });
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>

          <View
            style={{
              backgroundColor: '#fff',
              margin: 6,
              elevation: 3,
              padding: 10,
            }}>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Driver Name
              </Text>
              <TextInput
                placeholder="Driver Name"
                value={driver_name}
                autoCorrect={false}
                editable={this.state.buttonVisible == 1}

                onChangeText={driver_name=>this.setState({driver_name})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Date</Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16}}>
                    {this.state.date
                      ? this.state.date
                      : 'Select Date'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                    this.state.buttonVisible == 1?
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 2,
                      })
                      :null
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Time In</Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16}}>
                    {this.state.time_in
                      ? this.state.time_in
                      : 'Select Time In'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.state.buttonVisible == 1?

                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 3,
                      }) : null
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Time Out</Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16}}>
                    {this.state.time_out
                      ? this.state.time_out
                      : 'Select Time Out'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.state.buttonVisible == 1?

                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 4,
                      }) : null
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
              Registration
              </Text>
              <TextInput
                placeholder="Registration"
                value={reg_no}
                autoCorrect={false}
                editable={this.state.buttonVisible == 1}
                onChangeText={reg_no=>this.setState({reg_no})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Mileage In</Text>
              <TextInput
                placeholder="Mileage In"
                editable={this.state.buttonVisible == 1}

                value={mileage_in}
                autoCorrect={false}
                onChangeText={mileage_in=>this.setState({mileage_in})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Mileage Out
              </Text>
              <TextInput
                placeholder="Mileage Out"
                value={mileage_out}
                editable={this.state.buttonVisible == 1}

                autoCorrect={false}
                onChangeText={mileage_out=>this.setState({mileage_out})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Inspection</Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                }}>  
                <Text
                  onPress={() =>
                    this.props.navigation.navigate('AddInspection',{
                      getInspectionData:this.state.getInspectionData ?this.state.getInspectionData : null ,
                      reload: this.passData.bind(this),
                      buttonVisible:this.state.buttonVisible,
                      getInspectionData:this.state.inspectionData
                    })
                  }
                  style={{fontSize: 16, color: 'blue'}}>
                 {this.state.id != null ? "View Timesheet Inspection" : this.props.navigation.state.params?this.props.navigation.state.params.message:'Click here to add Inspection'}
                </Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Defect Information
              </Text>
              <TextInput
                numberOfLines={5}
                placeholder="Enter Defect Information"
                value={defect_info}
                autoCorrect={false}
                editable={this.state.buttonVisible == 1}

                onChangeText={defect_info=>this.setState({defect_info})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: 'blue',
                  justifyContent: 'flex-start',
                  paddingTop: 0,
                }}
              />
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Driver Signature
              </Text>
              <Text style={{fontSize: 13, fontWeight: '600'}}>
                Click plus icon to add signature
              </Text>

              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    this.state.buttonVisible == 1 ?
                    this.props.navigation.navigate('Signature', {
                      reload: this.passImage.bind(this),
                    }) : null
                  }>
                  <View
                    style={{
                      padding: 10,
                      backgroundColor: '#403EB6',
                      borderRadius: 50,
                      height: 40,
                      width: 40,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon size={25} name="plus" color="#fff" />
                  </View>
                </TouchableOpacity>

                <View
                  style={{
                    padding: 10,
                    height: 60,
                    width: 60,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {this.state.imageDatas != '' ? (
                    <Image
                      source={{
                        uri: this.state.imageDatas,
                      }}
                      style={{height: 60, width: 40,resizeMode:'contain'}}
                    />
                  ) : (
                    <Icon size={40} name="pencil" color="black" />
                  )}
                </View>
              </View>
            </View>

           {
             this.state.buttonVisible ===1 ?
             <TouchableButton
             title="SUBMIT"
             onPress={() => this.state.buttonVisible  == 1?  this.onSubmit() : null}
           />:null
           }
          </View>
          <View style={{height:80}}></View>
        </KeyboardAwareScrollView>
        <Loader isLoader={this.state.loading}></Loader>
        {this.state.isDateTimePickerVisible ? (
          <DateTimePicker
            mode={this.state.mode}
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            is24Hour={false}
           // onChange={this.handleDatePicked}
            // onConfirm={(date)=>this._handleDatePicked(date)}
          />
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
