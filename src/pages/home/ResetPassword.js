import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity, Alert} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import LoginTextInput from '../../component/LoginTextInput';
import {requestPostApiMedia, update_password} from '../../WebAPI/Service';
import Loader from '../../WebAPI/Loader';
export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      newPassword: '',
      confirmPassword: '',
      email: '',
    };
  }

  async onSubmit() {

const {newPassword,confirmPassword} = this.state

if(newPassword == ''){
  Alert.alert('Please enter new password.')
}else if(confirmPassword == '')
{
  Alert.alert('Please enter confirm password.')
}else 

if(newPassword==confirmPassword){

  this.setState({loading: true});
  const formData = new FormData();
  formData.append('email', this.state.email);
  formData.append(
    'password',
    this.state.newPassword === this.state.confirmPassword
      ? this.state.newPassword
      : null,
  );
  const {responseJson, err} = await requestPostApiMedia(
    update_password,
    formData,
    'POST',
  );

  if (responseJson.status) {
    this.setState({loading: false});

    Alert.alert('', responseJson.message, [
      {
        text: 'Ok',
        onPress: () => this.props.navigation.replace('Login'),
      },
    ]);
  } else {
    this.setState({loading: false});
    Alert.alert('', responseJson.message);
  }
}else{
  Alert.alert('',"Password does not match")
}
  }

  async componentDidMount() {
    this.setState({email: this.props.navigation.state.params.data.email});
    console.warn('data::', this.props.navigation.state.params.data);
  } 
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        <HeaderWithBack
          title="RESET PASSWORD"
          goBack={() => this.props.navigation.goBack()}
        />
        <View style={{margin: wp('5%')}}>
          <LoginTextInput
            label="New Password"
            secureTextEntry
            value={this.state.newPassword}
            onChangeText={(newText) => this.setState({newPassword: newText})}
          />

          <LoginTextInput
            label="Confirm Password"
            secureTextEntry
            value={this.state.confirmPassword}
            onChangeText={(newText) =>
              this.setState({confirmPassword: newText})
            }
           
          />

          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={{
              backgroundColor: '#FCA73A',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 5,
            }}>
            <Text style={{color: '#fff', fontSize: 18}}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
