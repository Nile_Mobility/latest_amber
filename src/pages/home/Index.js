import * as React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import Jobs from './Jobs';
import Timesheet from './Timesheet';
import DriverVehInsRep from './DriverVehInsRep';
import MobileWorkRecordSheet from './MobileWorkRecordSheet';
import Home from '../home/home';

import Signature from '../../component/Signature';
import AddInspection from './AddInspection';
import AddTimesheet from './AddTimesheet';
import AddTimesheetForWork from './AddTimesheetForWork';
import DateTimePickers from '../../component/DateTimePickers';

const Stack = createStackNavigator();

const MyHomeTabStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Jobs" component={Jobs} />
      <Stack.Screen name="DriverVehInsRep" component={DriverVehInsRep} />

      <Stack.Screen name="Timesheet" component={Timesheet} />
      <Stack.Screen
        name="MobileWorkRecordSheet"
        component={MobileWorkRecordSheet}
      />
      <Stack.Screen name="AddInspection" component={AddInspection} />
      <Stack.Screen name="AddTimesheet" component={AddTimesheet} />
      <Stack.Screen
        name="AddTimesheetForWork"
        component={AddTimesheetForWork}
      />
      <Stack.Screen name="DateTimePickers" component={DateTimePickers} />
      <Stack.Screen name="Signature" component={Signature} />
    </Stack.Navigator>
  );
};

export default MyHomeTabStack;
