import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  AsyncStorage,
  TextInput,
  Alert,Platform
} from 'react-native';

import Header from '../../component/Header';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import KeyStore from '../KeyStore/LocalKeyStore';
import ProfileContentBox from '../../component/ProfileContentBox';
import {
  operator_details,
  requestGetApi,
  resend_otp,
  update_details,
  requestPostApiMedia,
} from '../../WebAPI/Service';
import ImagePicker from 'react-native-image-crop-picker';
import Loader from '../../WebAPI/Loader';
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0,
      data: {},
      loading: false,
      name: '',
      email: '',
      phone: '',
      address: '',
      full_path: '',
      profile_image: '',
      image: [],
      selectedTabs: false,
      user_id: '',
    };
  }

  selectTab = (tabIndex) => {
    this.setState({selectedTab: tabIndex});
  };

  async profileImagePicker() {
    await ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      includeBase64: true,
      compressImageQuality: 0.5,
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300,
    }).then( (image) => {

       this.setState({image});
       
    });
  }

  async componentDidMount() {
    this.setState({loading: true});

    KeyStore.getKey('data', async (err, value) => {
      if (value) {
        const {user_id} = JSON.parse(value);

        const body = {
          operator_id: user_id,
        };

        const {responseJson, err} = await requestGetApi(
          operator_details,
          body,
          'GET',
        );

        if (responseJson.status) {
          const {
            name,
            email,
            phone,
            address,
            profile_image,
            full_path,
            user_id,
          } = responseJson.data;
          this.setState({
            loading: false,
            name,
            email,
            phone,
            address,
            profile_image,
            full_path,
            user_id,
          });
        } else {
          Alert.alert(err);
        }
      } else {
      }
    });
  }

  async onUpdateProfile() {

    const {image, address, phone, name, email, user_id} = this.state;
    this.setState({loading: true});
    var fileName = Math.floor(Math.random() * 100) + 1;
    const formData = new FormData();
    formData.append('user_id', user_id);
    formData.append('email', email);
    formData.append('address', address);
    formData.append('phone', phone);
    formData.append('name', name);

    if(this.state.image.path){
      formData.append('profile_image',{
        uri: Platform.OS === "android" ?  image.path :  image.path.replace("file://", ""),
        type: 'image/png',
        name: fileName+'.png',
      })
    }

 
  
    // formData.append('profile_image',image.path ? {
    //   uri: image.path,
    //   type: image.mime,
    //   name: image.path ? fileName + '.jpg' : null,
    // } : null)

    const {responseJson, err} = await requestPostApiMedia(
      update_details,
      formData,
      'POST',
    );
    this.setState({loading: false});

    if (responseJson.status) {
      Alert.alert('', responseJson.message);
    } else {
      Alert.alert(err);
    }
  }

  render() {
    const {name, email, phone, address, profile_image, full_path} = this.state;
    console.warn(this.state.data.name);

    const fullProfilePath = full_path + profile_image;
    console.warn(fullProfilePath);
    return (
      <View style={{flex: 1, backgroundColor: '#FAAFA'}}>
        <StatusBar backgroundColor="#C77D1C" />
        <Header
          title="PROFILE"
          onPress={async () => {
            this.props.navigation.navigate('Login');
            KeyStore.setKey('data',null)
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: '',
          }}>
          <TouchableOpacity
            onPress={() => this.selectTab(0)}
            style={{
              backgroundColor: '#fff',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
              width: wp('49.5%'),
            }}>
            <Text
              style={{
                fontSize: 16,
                color: this.state.selectedTab == 0 ? 'orange' : '#000',
              }}>
              MY PROFILE
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.selectTab(1);
              this.setState({selectedTabs: !this.state.selectedTabs});
            }}
            style={{
              backgroundColor: '#fff',
              padding: 10,
              justifyContent: 'center',
              alignItems: 'center',
              width: wp('49.5%'),
            }}>
            <Text
              style={{
                fontSize: 16,
                color: this.state.selectedTab == 1 ? 'orange' : '#000',
              }}>
              {this.state.selectedTabs && this.state.selectedTab === 1 ? (
                <Text
                  onPress={() => {
                    this.setState({selectedTabs: !this.state.selectedTabs});
                    this.onUpdateProfile();
                  }}>
                  SAVE PROFILE
                </Text>
              ) : (
                'EDIT PROFILE'
              )}
            </Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          disabled={this.state.selectedTab === 0 ? true : false}
          style={{justifyContent: 'center', alignItems: 'center', padding: 20}}
          onPress={() => this.profileImagePicker()}>
          {this.state.image.path ? (
            <Image
              source={{uri: this.state.image.path}}
              style={{
                height: wp('25%'),
                width: wp('25%'),
                borderRadius: 100,
                borderWidth: 3,
                borderColor: '#F8981D',
              }}
            />
          ) : (
            <Image
              source={fullProfilePath ?{uri: fullProfilePath }:require('../../images/app_icon_circle.png')}
              style={{
                height: wp('25%'),
                width: wp('25%'),
                borderRadius: 100,
                borderWidth: 3,
                borderColor: '#F8981D',
              }}
            />
          )}
        </TouchableOpacity>

        <View style={{backgroundColor: ''}}>
          <TextInput
            style={{
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 19,
              paddingBottom: 5,
            }}
            value={name}
            onChangeText={(name) => this.setState({name})}
            editable={this.state.selectedTab === 0 ? false : true}
          />

          <Text style={{textAlign: 'center', fontSize: 15}}>
            Working Hours : 9AM - 6PM
          </Text>
          <Text style={{textAlign: 'center', fontSize: 15}}>
            Monday-Saturday
          </Text>
        </View>

        <View style={{margin: wp('3%'), padding: 5, borderTopWidth: 0.5}}>
          <ProfileContentBox
            title="Email"
            editable={false}
            value={email}
            iconName="envelope"
            color="#FF951A"
          />

          {/* <ProfileContentBox
            
            value={phone}
          
           
            editable={this.state.selectedTab === 0 ? false : true}
          /> */}

          <View
            style={{
              borderBottomWidth: 0.5,
              flexDirection: 'row',
              backgroundColor: '',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <View
              style={{
                flex: 1,
                backgroundColor: '',
                alignItems: 'center',
                padding: 5,
                paddingBottom: 10,
              }}>
              <Icon name="phone" color="green" size={30} />
            </View>
            <View style={{flex: 4}}>
              <Text>Phone</Text>
              <TextInput
                style={{
                  backgroundColor: '',
                  fontSize: 17,
                  padding: 0,
                  fontWeight: 'bold',
                  color: '#757575',
                }}
                value={phone}
                onChangeText={(phone) => this.setState({phone})}
                editable={this.state.selectedTab === 0 ? false : true}
              />
            </View>
          </View>

          <View
            style={{
              borderBottomWidth: 0.5,
              flexDirection: 'row',
              backgroundColor: '',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <View
              style={{
                flex: 1,
                backgroundColor: '',
                alignItems: 'center',
                padding: 5,
                paddingBottom: 10,
              }}>
              <Icon name="map-pin" color="skyblue" size={30} />
            </View>
            <View style={{flex: 4}}>
              <Text>Address</Text>
              <TextInput
                style={{
                  backgroundColor: '',
                  fontSize: 17,
                  padding: 0,
                  fontWeight: 'bold',
                  color: '#757575',
                }}
                value={address}
                onChangeText={(address) => this.setState({address})}
                editable={this.state.selectedTab === 0 ? false : true}
              />
            </View>
          </View>
        </View>
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewStyle: {
    height: 100,
    width: 100,
    backgroundColor: '#00aadd',
    borderRadius: 50,
  },
  shadowStyle: {
    shadowOpacity: 1,
    shadowColor: 'red',
    shadowRadius: 5,
    shadowOffset: {
      height: -10,
      width: -10,
    },
  },
});
