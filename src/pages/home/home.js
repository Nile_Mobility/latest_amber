import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Alert,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Loader from '../../WebAPI/Loader';
import Header from '../../component/Header';
import BoxContent from '../../component/BoxContent';
import {totalNoOfJob, requestGetApi} from '../../WebAPI/Service';
import KeyStore from '../KeyStore/LocalKeyStore';
 
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      job: null,
      loading: false,
    };
  }

  async componentDidMount() {
    KeyStore.getKey('data', (err, value) => {
      if (value) {
        const {user_id} = JSON.parse(value);
        this.getJobApi(user_id);
      } else {
      }
    });
  }

  async getJobApi(id) {
    const body = {
      user_id: id,
      status: '0',
    };

    this.setState({loading: true});

    const {responseJson, err} = await requestGetApi(totalNoOfJob, body, 'GET');
    this.setState({loading: false});

    // console.warn(responseJson)
    if (responseJson) {
      this.setState({job: responseJson.jobs});
    } else {
      Alert.alert('', err);
    }
  }

  reload(){
    
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#E5E1E0'}}>
        <Header
          title="Home"
          onPress={() => {
            this.props.navigation.navigate('Login');
            KeyStore.setKey('data',null)
            // AsyncStorage.removeItem('data');
          }}
        />

        <View style={{margin: wp('5%')}}>
          <BoxContent
            title="Jobs"
            iconName="list-alt"
            openBox={() => this.props.navigation.navigate('Jobs')}
            notification={this.state.job}
          />

          <BoxContent
            title="Driver's Vehicle Inspection Report"
            iconName="truck"
            openBox={() => this.props.navigation.navigate('DriverVehInsRep',{reload:this.reload.bind(this)})}
          />

          <BoxContent
            title="TimeSheet"
            iconName="calendar"
            openBox={() => this.props.navigation.navigate('Timesheet')}
          />

          <BoxContent
            title="Mobile Work Record Sheet"
            iconName="book"
            openBox={() =>
              this.props.navigation.navigate('MobileWorkRecordSheet')
            }
          />
        </View>
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
