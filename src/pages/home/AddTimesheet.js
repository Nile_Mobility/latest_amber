import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,Alert
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import DeviceInfo from 'react-native-device-info';
import TouchableButton from '../../component/TouchableButton';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import Loader from '../../WebAPI/Loader'
import AsyncStorage from '@react-native-community/async-storage';
import KeyStore from '../KeyStore/LocalKeyStore'
// import showToast from '../../component/Toast';


import {save_user_timesheet,requestPostApiMedia,get_timesheetById,requestGetApi} from '../../WebAPI/Service'
export default class AddTimesheet extends Component {
  constructor(props) {
    super();
    this.state = {
      deviceId: DeviceInfo.getDeviceId(),
      open: false,
     

      status: '',
      isDateTimePickerVisible: false,
      mode: '',
      value:1,
     
      timesheet:1,
        shift:1,
        restTime:false,
        restMorningTime:false,
        restAfterTime:false,
        restEveningTime:false,
        loading:false,
        statusField:1,
        working_location:'',
        q_number:'',
       
        date_and_time_on_site:'',
        date_and_time_off_site:'',
     
        morningRest:false,
        noonRest:false,
        eveningRest:false,
       
      
       total_break:0,
       hour_worked:0,
       total_hour_worked:0,
     
       morning_break:'',
          mid_break:0, evening_break:'',  early_break:0, vehicle_ckeck_complete:1,
            item_counter:1, afternoon_break:'',  late_break:0, id:'' ,
            leave_status:'',
            leaveDate:'',
            leaveType:1,
            id:''

            };
  }


  openTimesheet = () => {
    this.setState({open: true});
  };
  closeTimesheet = () => {
    this.setState({open: false});
  };
  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (datetime) => {

    this.setState({isDateTimePickerVisible: false});

    if (this.state.status == 1) {
        
        this.setState({
          date_and_time_on_site: moment(datetime).format('MM/DD/YYYY HH:mm'),date_and_time_off_site:'',total_break:'00',totalHours:'00',total_hour_worked:'00',morningRest:false,noonRest:false,eveningRest:false
        });
    }

     else if (this.state.status == 2) {

        let dt = moment(datetime).format('MM/DD/YYYY HH:mm')
       
        let diffMin = moment(dt).diff(this.state.date_and_time_on_site, 'minutes')
        let diffHour = moment(dt).diff(this.state.date_and_time_on_site, 'hours')
       // showToast(diffHour)

        if (diffMin <= 0 ){
          //showToast('Off time should be greate than on site.','error')
         // Alert.alert('','')
           
        }
      else{
        this.setState({
          date_and_time_off_site: moment(datetime).format(
            'MM/DD/YYYY HH:mm'
          ),total_break:'00',hour_worked:diffHour+':'+diffMin%60,total_hour_worked:diffHour+':'+diffMin%60,morningRest:false,noonRest:false,eveningRest:false
        });
      }
     }
     else if(this.state.status ===3){
      this.setState({
        leaveDate: moment(datetime).format('DD-MM-YYYY'),
      });
     }
  };


async componentDidMount() {
  const id = this.props.navigation.state.params ? this.props.navigation.state.params.id:null
const user_id = this.props.navigation.state.params ? this.props.navigation.state.params.user_id : null

if(id){
  this.setState({loading:true})
const body ={
  user_id,
  id
}

const {responseJson,err} = await requestGetApi(get_timesheetById,body,'GET')  
this.setState({loading:false})
if(responseJson.status){
console.warn("++++++++++++++++++++++++++",responseJson.timesheet_data.early_break+"      "+responseJson.timesheet_data.late_break+"   "+responseJson.timesheet_data.mid_break)
const {leave_status,leave_type,leave_date,working_location,q_number,shift,date_and_time_on_site,date_and_time_off_site,morning_break,afternoon_break,evening_break,total_break,total_working_hour,vehicle_ckeck_complete,item_counter,mid_break,early_break,late_break} =responseJson.timesheet_data

if(leave_status==='1'){
  this.setState({leaveType:leave_type,leaveDate:leave_date,statusField:2})
}else{
  this.setState({leaveType:leave_type,working_location,q_number,shift,date_and_time_on_site,date_and_time_off_site,morning_break,afternoon_break,evening_break,total_break,total_working_hour,vehicle_ckeck_complete,item_counter,noonRest:mid_break ==60? true:false,morningRest:early_break == 30? true :false,eveningRest:late_break ==30 ? true:false})
}



}
else{
  this.setState({loading:false})
}
console.warn("darta",responseJson)

}else{
  console.warn("yeess id nottttttt is threr")
}
}

  componentWillUnmount() {
   // this.props.navigation.state.params.reload(this.state.timesheet)
  }

  getFormatDate(str){
    try {
      let arrHour = str.split(':')
      let h = arrHour[0].length == 1 ? '0'+arrHour[0]  : arrHour[0]
      let m =  arrHour.length == 2 ?  (arrHour[1].length == 1 ? '0'+arrHour[1]  : arrHour[1]) : '00'
      return h+':'+m
    }catch{
      let h = String(str).length == 1 ? '0'+str  : str
      let m =  '00'
      return h+':'+m
    }
  }


  userInputFormValidation(){
    const {working_location,q_number,shift,date_and_time_on_site,date_and_time_off_site,vehicle_ckeck_complete} = this.state
   
    if(working_location==''||q_number==''|| shift==''||date_and_time_on_site==''||date_and_time_off_site==''||vehicle_ckeck_complete==''){
  if(working_location==''){
    Alert.alert('','Please fill Working location')
  
  }else if(q_number==''){
    Alert.alert('','Please fill q number')
  
  }else if(shift==''){
    Alert.alert('','Please fill shift ')
   
  }
  else if(date_and_time_on_site==''){
    Alert.alert('','Please fill date adn time On Site name')
   
  }else if(date_and_time_off_site==''){
    Alert.alert('','Please fill date adn time Off Site name')
    
  }else if(vehicle_ckeck_complete==''){
    Alert.alert('','Please fill vehicle check compelte')
  
  }
  return false
  }else{
    return true
  }
  
  }

  async onSubmit(){
    const {statusField,working_location,q_number,shift,date_and_time_on_site,date_and_time_off_site,restTime,total_break,hour_worked,vehicle_ckeck_complete,item_counter,mid_break,late_break,early_break,morningRest,noonRest,eveningRest,leaveDate,leaveType,morning_break,afternoon_break,evening_break} = this.state
   
this.setState({loading:true})

const id = this.props.navigation.state.params ? this.props.navigation.state.params.id:null

if(id){

  KeyStore.getKey('data',async (err, value) => {
    if (value) {
      const {user_id} = JSON.parse(value);
      const formData = new FormData()
  formData.append('user_id',user_id)
  formData.append('id',id)
  if(statusField===2){
    formData.append('leave_status',1)

   
  formData.append('leave_type',leaveType)
  formData.append('leave_date',leaveDate)
  
  const {responseJson,err} = await requestPostApiMedia(save_user_timesheet,formData,'POST')
  
  if(responseJson.status){
    this.setState({loading:false})
    Alert.alert(
      '',
      responseJson.message,
      [
       
        {text: 'Ok', onPress: async()=>{
          this.props.navigation.replace('Timesheet')
  
        } },
      ]
      );
  }else {
    this.setState({loading:false})
    Alert.alert(err)
  }


  }else if(statusField===1){

 

    formData.append('leave_status',2)
  
    formData.append('working_location',working_location)
    formData.append('q_number',q_number)
    formData.append('shift',shift)
    formData.append('date_and_time_on_site',date_and_time_on_site)
    formData.append('date_and_time_off_site',date_and_time_off_site)
    formData.append('morning_break',morningRest)
    formData.append('afternoon_break',noonRest)
    formData.append('evening_break',eveningRest)
    formData.append('restTime',restTime)
    formData.append('total_break',this.getFormatDate(total_break))
    formData.append('hour_worked',this.getFormatDate(hour_worked))
    formData.append('vehicle_ckeck_complete',vehicle_ckeck_complete)
    formData.append('item_counter',item_counter)
    formData.append('mid_break',noonRest ? 60:0)
    formData.append('late_break',eveningRest? 30:0)
    formData.append('early_break',morningRest ? 30:0)
  
    const {responseJson,err} = await requestPostApiMedia(save_user_timesheet,formData,'POST')
  
  if(responseJson.status){
    this.setState({loading:false})
    Alert.alert(
      '',
      responseJson.message,
      [
       
        {text: 'Ok', onPress: async()=>{
          this.props.navigation.replace('Timesheet')
  
        } },
      ]
      );
  }else {
    this.setState({loading:false})
    Alert.alert(err)
  }
  
  
  
  
  
  

}
  // print(formData)


    } else {
    }
  });

}else{

  KeyStore.getKey('data',async (err, value) => {
    if (value) {
      const {user_id} = JSON.parse(value);
      const formData = new FormData()
  formData.append('user_id',user_id)
  
  if(statusField===2){
    formData.append('leave_status',1)
    
  formData.append('leave_type',leaveType)
  formData.append('leave_date',leaveDate)

  if(leaveDate==''){
    this.setState({loading:false})
    alert("Please fill Leave date")
  }else{
    const {responseJson,err} = await requestPostApiMedia(save_user_timesheet,formData,'POST')

    if(responseJson.status){
      this.setState({loading:false})
      Alert.alert(
        '',
        responseJson.message,
        [
         
          {text: 'Ok', onPress: async()=>{
            this.props.navigation.replace('Timesheet')
    
          } },
        ]
        );
    }else {
      this.setState({loading:false})
      Alert.alert(err)
    }
  }
 
  }else if(statusField===1){
  
    if(this.userInputFormValidation()){
     
      formData.append('leave_status',2)
    
      formData.append('working_location',working_location)
      formData.append('q_number',q_number)
      formData.append('shift',shift)
      formData.append('date_and_time_on_site',date_and_time_on_site)
      formData.append('date_and_time_off_site',date_and_time_off_site)
      formData.append('morning_break',morningRest)
      formData.append('afternoon_break',noonRest)
      formData.append('evening_break',eveningRest)
  
      formData.append('restTime',restTime)
      formData.append('total_break',this.getFormatDate(total_break))
      formData.append('hour_worked',this.getFormatDate(hour_worked))
      formData.append('vehicle_ckeck_complete',vehicle_ckeck_complete)
      formData.append('item_counter',item_counter)
      formData.append('mid_break',noonRest ? 60:0)
      formData.append('late_break',eveningRest? 30:0)
      formData.append('early_break',morningRest ? 30:0)
   
   
    const {responseJson,err} = await requestPostApiMedia(save_user_timesheet,formData,'POST')

    if(responseJson.status){
      this.setState({loading:false})
      Alert.alert(
        '',
        responseJson.message,
        [
         
          {text: 'Ok', onPress: async()=>{
            this.props.navigation.replace('Timesheet')
    
          } },
        ]
        );
    }else {
      this.setState({loading:false})
      Alert.alert(err)
    }}else{
     
      this.setState({loading:false})
    }
    }
  
   
    } else {
    }
  });
  



}






}

getTotalTime(){


  var dateTimeOnSite  = moment(this.state.date_and_time_on_site); 
    var dateTimeOffSite = moment(this.state.date_and_time_off_site)
    var duration = moment.duration(dateTimeOffSite.diff(dateTimeOnSite));
    var totalHours = duration.asHours();
    console.warn(totalHours)
    this.setState({hour_worked:totalHours})

const total_break= this.state.early_break+this.state.mid_break+this.state.late_break
this.setState({total_break})

console.warn(total_break)
}



  
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        <HeaderWithBack
          title="Add Timesheet"
          goBack={() => this.props.navigation.goBack()}
        />
        <View
          style={{
            backgroundColor: '#fff',
            margin: 6,
            elevation: 10,
            flex: 1,
            padding: 10,
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Status</Text>
             
              <View style={[styles.container,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,}]}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({statusField: 1})}>
              <View style={styles.outerCircle}>
                {this.state.statusField === 1 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>On Work</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({statusField: 2})}>
              <View style={styles.outerCircle}>
                {this.state.statusField === 2 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>On Leave</Text>
          </View>
        </View>
            </View>

            {
              this.state.statusField ===1 ?
              <View>
                <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Working Location
              </Text>
              <TextInput
                placeholder="Enter Working Location"
value={this.state.working_location}
onChangeText={working_location=>this.setState({working_location})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Q Number</Text>
              <TextInput
                placeholder="Enter Submission Id"
                value={this.state.q_number}
onChangeText={q_number=>this.setState({q_number})}
                placeholderTextColor="#757575"
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
              <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Shift</Text>
             
              <View style={[styles.container,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,}]}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({shift: 1})}>
              <View style={styles.outerCircle}>
                {this.state.shift == 1 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Day</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({shift: 2})}>
              <View style={styles.outerCircle}>
                {this.state.shift == 2 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Night</Text>
          </View>
        </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Date And Time on Site
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16, color: '#757575'}}>
                    {this.state.date_and_time_on_site
                      ? this.state.date_and_time_on_site
                      : 'Select Date & Time'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'datetime',
                        status: 1,
                      });
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Date And Time off Site
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16, color: '#757575'}}>
                    {this.state.date_and_time_off_site
                      ? this.state.date_and_time_off_site
                      : 'Select Date & Time'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {

                      if(this.state.date_and_time_on_site == ''){
                         // showToast('Please select on time first.','error')
                     //   Alert.alert('Please select on time first.')
                        
                      }else{                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'datetime',
                        status: 2,
                      });
}

                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Rest Time</Text>
             
              <View>
        
        
        <View style={[styles.containerCheck,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 0,paddingBottom:0}]}>
       
       
          <View style={styles.squareBox}>
            <TouchableOpacity onPress={() => {
              
              this.breakHoursAction(true,false,false)

            }}>
              <View style={styles.outerBoxNew}>
                {this.state.morningRest && <Icon name="check" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Morning - 30 min</Text>
          </View>

      
          <View style={styles.squareBox}>
            <TouchableOpacity onPress={() =>{
             
              this.breakHoursAction(false,true,false)

            }}>
              <View style={styles.outerBoxNew}>
                {this.state.noonRest  && <Icon name="check" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Afternoon - 1 hour</Text>
          </View>

          <View style={styles.squareBox}>
            <TouchableOpacity onPress={() =>{
              
              this.breakHoursAction(false,false,true)
            }}>
              <View style={styles.outerBoxNew}>
                {this.state.eveningRest  && <Icon name="check" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Evening - 30 min</Text>
          </View>


        </View>
      </View>
            </View>


            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Break(HH:mm)
              </Text>
             
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{ this.getFormatDate(this.state.total_break)}</Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Hours Worked(HH:mm)
              </Text>
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{ this.getFormatDate(this.state.hour_worked)}</Text>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Total Hours Worked(HH:mm)
              </Text>
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{this.getFormatDate(this.state.total_hour_worked)}</Text>
              </View>
            </View>


            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Vehicle Check Complete</Text>
             
              <View style={[styles.container,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,}]}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({vehicle_ckeck_complete: 1})}>
              <View style={styles.outerCircle}>
                {this.state.vehicle_ckeck_complete == 1 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({vehicle_ckeck_complete: 2})}>
              <View style={styles.outerCircle}>
                {this.state.vehicle_ckeck_complete == 2 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Item Counter
              </Text>
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{this.state.item_counter}</Text>
              </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Mid Break</Text>
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{this.state.mid_break ? this.state.mid_break : this.state.noonRest ? 60 : 0}</Text>
              </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Late Break</Text>
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{ this.state.late_break ? this.state.late_break : this.state.eveningRest ? 30 : 0}</Text>
              </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Early Break
              </Text>
              <View  style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                 
                  backgroundColor: '#E5E1E0',
                  paddingVertical:8
                }}>
<Text style={{ fontSize: 16,
                  color: '#757575',}}>{ this.state.early_break ? this.state.early_break : this.state.morningRest ? 30 : 0}</Text>
              </View>
            </View>
              </View>:<View>
              <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Leave Type</Text>
             
              <View style={[styles.container,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,}]}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({leaveType: 1})}>
              <View style={styles.outerCircle}>
                {this.state.leaveType == 1 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Annual Leave</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({leaveType: 2})}>
              <View style={styles.outerCircle}>
                {this.state.leaveType == 2 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Sick Leave</Text>
          </View>
        </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Leave Date
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16, color: '#757575'}}>
                    {this.state.leaveDate
                      ? this.state.leaveDate
                      : 'Select Date'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 3,
                      });
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
              </View>
            }

            <TouchableButton
              title="SUBMIT"
              onPress={() =>this.onSubmit()}
            />
          </ScrollView>
          <Loader isLoader={this.state.loading}></Loader>
        
          {this.state.isDateTimePickerVisible ? (
            <DateTimePicker
              mode={this.state.mode}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.handleDatePicked}
              onCancel={this.hideDateTimePicker}
              is24Hour={false}
             // onChange={this.handleDatePicked}
              isDarkModeEnabled= {false}
              textColor= "#000000	"
              date = {this.state.initialDateTime}

             //  onConfirm={(date)=>this.hideDateTimePicker(date)}
            />
          ) : null}

        </View>
      </View>
    );
  }

  breakHoursAction(first,second,third){

    if(this.state.date_and_time_on_site == '' || this.state.date_and_time_off_site == '' ){
     // showToast('Please selet dates first.','error')
    }else{

      let diffMin = moment(this.state.date_and_time_off_site).diff(this.state.date_and_time_on_site, 'minutes')
      let diffHour = moment(this.state.date_and_time_off_site).diff(this.state.date_and_time_on_site, 'hours')

    if(first){

      if(this.state.morningRest){
        let totalBreak = (this.state.noonRest ? 60 : 0) + (this.state.eveningRest ? 30 : 0) + 0
        this.setState({total_break:Math.floor((totalBreak)/60) + ':' + (totalBreak)%60,total_hour_worked:Math.floor((diffMin-totalBreak)/60) + ':' + (diffMin-totalBreak)%60 ,hour_worked:diffHour})

      }
      else{

        let totalBreak = (this.state.noonRest ? 60 : 0) + (this.state.eveningRest ? 30 : 0) + 30
        this.setState({total_break:Math.floor((totalBreak)/60) + ':' + (totalBreak)%60,total_hour_worked:Math.floor((diffMin-totalBreak)/60) + ':' + (diffMin-totalBreak)%60 ,hour_worked:diffHour})
    
      }

      this.setState({morningRest:!this.state.morningRest})

    }

    if(second){
      if(this.state.noonRest){
        let totalBreak = (this.state.morningRest ? 30 : 0) + (this.state.eveningRest ? 30 : 0) + 0
        this.setState({total_break:Math.floor((totalBreak)/60) + ':' + (totalBreak)%60,total_hour_worked:Math.floor((diffMin-totalBreak)/60) + ':' + (diffMin-totalBreak)%60 ,hour_worked:diffHour})

      }else{
        let totalBreak = (this.state.morningRest ? 30 : 0) + (this.state.eveningRest ? 30 : 0) + 60
        this.setState({total_break:Math.floor((totalBreak)/60) + ':' + (totalBreak)%60,total_hour_worked:Math.floor((diffMin-totalBreak)/60) + ':' + (diffMin-totalBreak)%60 ,hour_worked:diffHour})

      }
      this.setState({noonRest:!this.state.noonRest})
    }

    if(third){
      if(this.state.eveningRest){
        let totalBreak = (this.state.morningRest ? 30 : 0) + (this.state.noonRest ? 60 : 0) + 0
        this.setState({total_break:Math.floor((totalBreak)/60) + ':' + (totalBreak)%60,total_hour_worked:Math.floor((diffMin-totalBreak)/60) + ':' + (diffMin-totalBreak)%60 ,hour_worked:diffHour})


        
      }else{
        let totalBreak = (this.state.morningRest ? 30 : 0) + (this.state.noonRest ? 60 : 0) + 30
        this.setState({total_break:Math.floor((totalBreak)/60) + ':' + (totalBreak)%60,total_hour_worked:Math.floor((diffMin-totalBreak)/60) + ':' + (diffMin-totalBreak)%60 ,hour_worked:diffHour})

      }
      this.setState({eveningRest:!this.state.eveningRest})

    }
    }
  }
}

const styles = StyleSheet.create({
  innerCircle:{height:13,width:13,backgroundColor:'#00aaff',borderRadius:100,borderWidth:1,borderColor:'#00aaff'},
  outerCircle:{height:20,width:20,borderWidth:1,borderRadius:100,justifyContent:'center',alignItems:'center',marginRight:10,marginLeft:5},
  label:{fontSize:15},
  container:{flexDirection:'row',justifyContent:'space-around',marginLeft:0,marginVertical:10},
  radioBox:{flexDirection:'row',alignItems:'center',flex:1},
  label: {fontSize: 15},
  containerCheck: {
    
   
    marginTop: 10,
  },
  squareBox: {
    flexDirection: 'row',
   alignItems:'center',
   marginLeft:5,
   marginBottom:10
  },
  outerBoxNew: {
    height: 20,
    width: 20,
    borderWidth: 2,
   
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20,
  },
});
