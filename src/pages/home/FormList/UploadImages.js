import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,Platform
} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from '../../../component/Icon';
import {   
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Loader from '../../../WebAPI/Loader'
import {requestPostApiMedia,update_image2} from '../../../WebAPI/Service'
import TouchableButton from '../../../component/TouchableButton';

export default class Jobs extends Component {

constructor(props){
  super()
  this.state={
    image_one:{},
    image_second:{},
    image_third:{},
    image_fourth:{},
    image_fifth:{},
    loading:false
  }
}

async onSubmit(){
const {image_one,image_second,image_third,image_fifth,image_fourth} = this.state


 var fileName = Math.floor(Math.random() * 100) + 1 ;
const formData = new FormData()
formData.append('q_no', this.props.navigation.state.params.q_no);

formData.append('project_image',image_one.path === undefined ? null : {
  uri: Platform.OS === "android" ?  image_one.path :  image_one.path.replace("file://", ""),
  type: 'image/png',
  name: fileName+'.png',
})

formData.append('project_image2',image_second.path === undefined  ? null : {
  uri: Platform.OS === "android" ?  image_second.path :  image_second.path.replace("file://", ""),
  type: 'image/png',
  name: fileName+'.png',
})

formData.append('project_image3',image_third.path === undefined ? null : {
  uri: Platform.OS === "android" ?  image_third.path :  image_third.path.replace("file://", ""),
  type: 'image/png',
  name: fileName+'.png',
})

formData.append('project_image4',image_fourth.path === undefined ?null : {
  uri: Platform.OS === "android" ?  image_fourth.path :  image_fourth.path.replace("file://", ""),
  type: 'image/png',
  name: fileName+'.png',
})

formData.append('project_image5',image_fifth.path === undefined ?null : {
  uri: Platform.OS === "android" ?  image_fifth.path :  image_fifth.path.replace("file://", ""),
  type: 'image/png',
  name: fileName+'.png',
})
this.setState({loading:true})

const {responseJson,err} = await requestPostApiMedia(update_image2,formData,'POST')
this.setState({loading:false})

this.props.navigation.goBack()
// console.warn("one",responseJson)
// console.log("two",err)

}


async firstImagePicker(){
  console.log('first')
  await ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true,
   // includeBase64:true,
    compressImageQuality:.5,
    compressImageMaxWidth:300,
    compressImageMaxHeight:400
  }).then(async(image) => {
   await this.setState({image_one:image})

  });

  
}

async secondImagePicker(){
  await ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true,
    includeBase64:true,
    compressImageQuality:.5,
    compressImageMaxWidth:300,
    compressImageMaxHeight:400
  }).then(async(image) => {
   await this.setState({image_second:image})
//    console.warn(image.data);
  });

  
}



async thirdImagePicker(){
  await ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true,
    includeBase64:true,
    compressImageQuality:.5,
    compressImageMaxWidth:300,
    compressImageMaxHeight:400
  }).then(async(image) => {
   await this.setState({image_third:image})
   // console.warn(image.data);
  });

  
}

async fourthImagePicker(){
  await ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true,
    includeBase64:true,
    compressImageQuality:.5,
    compressImageMaxWidth:300,
    compressImageMaxHeight:400
  }).then(async(image) => {
   await this.setState({image_fourth:image})
  //  console.warn(image.data);
  });

  
}

async fifthImagePicker(){
  await ImagePicker.openPicker({
    width: 300,
    height: 400,
    cropping: true,
    includeBase64:true,
    compressImageQuality:.5,
    compressImageMaxWidth:300,
    compressImageMaxHeight:400
  }).then(async(image) => {
   await this.setState({image_fifth:image})
   // console.warn(image.data);
  });

  
}

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <HeaderWithBack
          title="UPLOAD IMAGES"
          goBack={() => this.props.navigation.goBack()}
        />
        <View
          style={{justifyContent: 'center', alignItems: 'center', margin: 20}}>
          <Text style={{fontSize: 17, fontWeight: 'bold', marginBottom: 5}}>
            You can upload maximum 5 images
          </Text>
          <Text style={{fontSize: 17, fontWeight: 'bold'}}>
            Click on logo to upload
          </Text>
        </View>

        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginBottom: 20,
            }}>

          
<TouchableOpacity onPress={()=>this.firstImagePicker()}>

{
  this.state.image_one.path ?
  <Image
            
            source={{uri: this.state.image_one.path}}
             style={{
               height: wp('25%'),
               width: wp('40%'),
               resizeMode: 'stretch',
               
             }}
            />:(<Image
            source={require('../../../images/images_iconn.png')}
            style={{
              height: wp('25%'),
              width: wp('40%'),
              resizeMode: 'stretch',
            }}
          />)
}

</TouchableOpacity>
           
<TouchableOpacity onPress={()=>this.secondImagePicker()}>

{
  this.state.image_second.path ?
  <Image
            
  source={{uri: this.state.image_second.path}}
  style={{
               height: wp('25%'),
               width: wp('40%'),
               resizeMode: 'stretch',
               
             }}
            />:(<Image
            source={require('../../../images/images_iconn.png')}
            style={{
              height: wp('25%'),
              width: wp('40%'),
              resizeMode: 'stretch',
            }}
          />)
}

</TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              marginBottom: 20,
            }}>
            <TouchableOpacity onPress={()=>this.thirdImagePicker()}>

{
  this.state.image_third.path ?
  <Image
            
  source={{uri: this.state.image_third.path}}
             style={{
               height: wp('25%'),
               width: wp('40%'),
               resizeMode: 'stretch',
               
             }}
            />:(<Image
            source={require('../../../images/images_iconn.png')}
            style={{
              height: wp('25%'),
              width: wp('40%'),
              resizeMode: 'stretch',
            }}
          />)
}

</TouchableOpacity>

<TouchableOpacity onPress={()=>this.fourthImagePicker()}>

{
  this.state.image_fourth.path ?
  <Image
            
  source={{uri: this.state.image_fourth.path}}
             style={{
               height: wp('25%'),
               width: wp('40%'),
               resizeMode: 'stretch',
               
             }}
            />:(<Image
            source={require('../../../images/images_iconn.png')}
            style={{
              height: wp('25%'),
              width: wp('40%'),
              resizeMode: 'stretch',
            }}
          />)
}

</TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginBottom: 10,
              alignItems: 'center',
            }}>
            <TouchableOpacity onPress={()=>this.fifthImagePicker()}>

{
  this.state.image_fifth.data ?
  <Image
            
  source={{uri: this.state.image_fifth.path}}
             style={{
               height: wp('25%'),
               width: wp('40%'),
               resizeMode: 'stretch',
               
             }}
            />:(<Image
            source={require('../../../images/images_iconn.png')}
            style={{
              height: wp('25%'),
              width: wp('40%'),
              resizeMode: 'stretch',
            }}
          />)
}

</TouchableOpacity>
          </View>
        </View>

        <TouchableButton
title="SUBMIT"
onPress={(()=>this.onSubmit())}
/>

<Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
