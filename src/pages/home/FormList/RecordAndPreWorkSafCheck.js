import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Alert,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Modal
} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import Orientation from 'react-native-orientation';
import Icon from '../../../component/Icon'
import TouchableButton from '../../../component/TouchableButton';
import Loader from '../../../WebAPI/Loader'
import {update_briefing,requestPostApiMedia} from '../../../WebAPI/Service'
import KeyStore from '../../KeyStore/LocalKeyStore'
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';
export default class RecordAndPreWorkSafCheck extends Component {
constructor(props){
  super()
  this.state={
    loading: false,
    brief_required: "0",
    date_briefing: "",
    name_briefing: "",
    team_leader: "",
    contents: "0",
    marker: "",
    cars: "",
    hgv: "",
    total: "",
    remarks: "",
    agreed: "0",
    visibility: "0",
    sight: "0",
    time_of_count: "",
    time_of_count2: "",
    marker_post: "",
    cars2: "",
    hgv2: "",
    total2: "",
    remarks2: "",
    time_of_count3: "",
    marker_post2: "",
    cars3: "",
    hgv3: "",
    total3: "",
    remarks3: "",
    statement: "",
    drawing_br: "",
    risk_br: "",
    isDateTimePickerVisible: false,
    mode: '',

    open: false,
    setDate: new Date(),
    confirmDate1:'',
    confirmDate2:'',
    confirmDate3:'',
    confirmDate4:'',
    confirmDate5:'',
    confirmDate6:'',
    confirmDate7:'',
    confirmDate8:'',
    confirmDate9:'',
    confirmDate10:'',
    confirmDate11:'',
    confirmDate12:'',
    confirmDate13:'',
    confirmDate14:'',

    confirmDate15:'',
    confirmDate16:'',
    confirmDate17:'',
    confirmDate18:'',
    
    modalVisible:false,
    optionDate:'',

    printName1:'',
    printName2:'',
    printName3:'',
    printName4:'',
    printName5:'',
    printName6:'',
    printName7:'',
    printName8:'',
    printName9:'',
    printName10:'',
    printName11:'',
    printName12:'',
    printName13:'',
    printName14:'',
    printName15:'',
    printName16:'',
    printName17:'',
    printName18:'',
    signature_1:'',
    signature_2:'',
    signature_3:'',
    signature_4:'',
    signature_5:'',
    signature_6:'',
    signature_7:'',
    signature_8:'',
    signature_9:'',
    signature_10:'',
    signature_11:'',
    signature_12:'',
    signature_13:'',
    signature_14:'',
    signature_15:'',
    signature_16:'',
    signature_17:'',
    


  }
}

  componentWillMount() {
    // The getOrientation method is async. It happens sometimes that
    // you need the orientation at the moment the JS runtime starts running on device.
    // `getInitialOrientation` returns directly because its a constant set at the
    // beginning of the JS runtime.

    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      console.warn('portartait');
      // do something
    } else {
      // do something else
    }
  }


async onSubmit(){
  const {
    brief_required,
    date_briefing,
    name_briefing,
    team_leader,
    contents,
    marker,
    cars,
    hgv,
    total,
    remarks,
    agreed,
    visibility,
    sight,
    time_of_count,
    time_of_count2,
    marker_post,
    cars2,
    hgv2,
    total2,
    remarks2,
    time_of_count3,
    marker_post2,
    cars3,
    hgv3,
    total3,
    remarks3,
    statement,
    drawing_br,
    risk_br,
    printName1,printName2,printName3,printName4,printName5,printName6,printName7,printName8,printName9,printName10,printName12,printName13,printName14,printName15,printName16,printName17,printName11,confirmDate1,confirmDate2,confirmDate3,confirmDate4,confirmDate5,confirmDate6,confirmDate7,confirmDate8,confirmDate9,confirmDate10,confirmDate11,confirmDate12,confirmDate13,confirmDate14,confirmDate15,confirmDate17,confirmDate16,confirmDate18,signature_1,signature_2,signature_3,signature_4,signature_5,signature_6,signature_7,signature_8,signature_9,signature_10,signature_11,signature_12,signature_13,signature_14,signature_15,signature_16,signature_17
  }= this.state

this.setState({loading:true})
console.warn(this.props.navigation.state.params.q_no)

const formData = new FormData()
formData.append('q_no',this.props.navigation.state.params.q_no)

formData.append('print_name_1',printName1)
formData.append('date_1',confirmDate2)
formData.append('print_name_2',printName2)
formData.append('date_2',confirmDate3)
formData.append('print_name_3',printName3)
formData.append('date_3',confirmDate4)
formData.append('print_name_4',printName4)
formData.append('date_4',confirmDate5)
formData.append('print_name_5',printName5)
formData.append('date_5',confirmDate6)
formData.append('print_name_6',printName6)
formData.append('date_6',confirmDate7)
formData.append('print_name_7',printName7)
formData.append('date_7',confirmDate8)
formData.append('print_name_8',printName8)
formData.append('date_8',confirmDate9)
formData.append('print_name_9',printName9)
formData.append('date_9',confirmDate10)
formData.append('print_name_10',printName10)
formData.append('date_10',confirmDate11)
formData.append('print_name_11',printName11)
formData.append('date_11',confirmDate12)
formData.append('print_name_12',printName12)
formData.append('date_12',confirmDate13)
formData.append('print_name_13',printName13)
formData.append('date_13',confirmDate14)
formData.append('print_name_14',printName14)
formData.append('date_14',confirmDate15)
formData.append('print_name_15',printName15)
formData.append('date_15',confirmDate16)
formData.append('print_name_16',printName16)
formData.append('date_16',confirmDate17)
formData.append('print_name_17',printName17)
formData.append('date_17',confirmDate18)

formData.append('signature_1',signature_1)
formData.append('signature_2',signature_2)
formData.append('signature_3',signature_3)
formData.append('signature_4',signature_4)
formData.append('signature_5',signature_5)
formData.append('signature_6',signature_6)
formData.append('signature_7',signature_7)
formData.append('signature_8',signature_8)
formData.append('signature_9',signature_9)
formData.append('signature_10',signature_10)
formData.append('signature_11',signature_11)
formData.append('signature_12',signature_12)
formData.append('signature_13',signature_13)
formData.append('signature_14',signature_14)
formData.append('signature_15',signature_15)
formData.append('signature_16',signature_16)
formData.append('signature_17',signature_17)




formData.append('brief_required',brief_required)
formData.append('date_briefing',date_briefing)
formData.append('name_briefing',name_briefing)
formData.append('team_leader',team_leader)
formData.append('contents',contents)
formData.append('time_of_count',time_of_count)
formData.append('marker',marker)
formData.append('cars',cars)
formData.append('hgv',hgv)
formData.append('total',total)
formData.append('remarks',remarks)
formData.append('time_of_count2',time_of_count2)
formData.append('marker_post',marker_post)
formData.append('cars2',cars2)
formData.append('hgv2',hgv2)
formData.append('total2',total2)
formData.append('remarks2',remarks2)
formData.append('time_of_count3',time_of_count3)
formData.append('marker_post2',marker_post2)
formData.append('cars3',cars3)
formData.append('hgv3',hgv3)
formData.append('total3',total3)
formData.append('remarks3',remarks3)
formData.append('agreed',agreed)
formData.append('visibility',visibility)
formData.append('sight',sight)


const recordandpreworkdata={
  printName1,printName2,printName3,printName4,printName5,printName6,printName7,printName8,printName9,printName10,printName12,printName13,printName14,printName15,printName16,printName17,printName11,signature_1,signature_2,signature_3,signature_4,signature_5,signature_6,signature_7,signature_8,signature_9,signature_10,signature_11,signature_12,signature_13,signature_14,signature_15,signature_16,signature_17,confirmDate1,
      confirmDate2,
      confirmDate3,
      confirmDate4,
      confirmDate5,
      confirmDate6,
      confirmDate7,
      confirmDate8,
      confirmDate9,
      confirmDate10,
      confirmDate11,
      confirmDate12,
      confirmDate13,
      confirmDate14,
  
      confirmDate15,
      confirmDate16,
      confirmDate17,
      confirmDate18,q_no:this.props.navigation.state.params.q_no
}
const {responseJson,err} = await requestPostApiMedia(update_briefing,formData,'POST')

console.warn("Submitted",responseJson)

if(responseJson.status){
  this.setState({loading:false})

  //make name for local storage
  const nameforrecordandprework = "rapwsc"+this.props.navigation.state.params.q_no

  KeyStore.setKey(nameforrecordandprework, JSON.stringify(recordandpreworkdata));
  Alert.alert('',responseJson.message,
  [
        
    {
      text: "Ok",
      onPress: () => this.props.navigation.navigate('FormsList')
      
    },
  ])
}else{
  this.setState({loading:false})
  Alert.alert(err,responseJson.message)
}

}

  async componentDidMount() {
  this.setState({loading:true})
    const formData = new FormData();
 
    formData.append('q_no', this.props.navigation.state.params.q_no);
 
    const {responseJson,err} =await requestPostApiMedia(update_briefing,formData,'POST')
if(responseJson.status){
  const {brief_required,
  date_briefing,
  name_briefing,
  team_leader,
  contents,
  marker,
  cars,
  hgv,
  total,
  remarks,
  agreed,
  visibility,
  sight,
  time_of_count,
  time_of_count2,
  marker_post,
  cars2,
  hgv2,
  total2,
  remarks2,
  time_of_count3,
  marker_post2,
  cars3,
  hgv3,
  total3,
  remarks3,
  statement,
  drawing_br,
  risk_br} = responseJson.data
  console.warn("dataaaa",responseJson.data)
  this.setState({loading:false,brief_required,
  date_briefing,
  name_briefing,
  team_leader,
  contents,
  marker,
  cars,
  hgv,
  total,
  remarks,
  agreed,
  visibility,
  sight,
  time_of_count,
  time_of_count2,
  marker_post,
  cars2,
  hgv2,
  total2,
  remarks2,
  time_of_count3,
  marker_post2,
  cars3,
  hgv3,
  total3,
  remarks3,
  statement,
  drawing_br,
  risk_br})


  const name = "rapwsc"+this.props.navigation.state.params.q_no

  KeyStore.getKey(name, (err, value) => {


    if (value) {

     const data = JSON.parse(value)
     const {printName1,printName2,printName3,printName4,printName5,printName6,printName7,printName8,printName9,printName10,printName12,printName13,printName14,printName15,printName16,printName17,printName11,signature_1,signature_2,signature_3,signature_4,signature_5,signature_6,signature_7,signature_8,signature_9,signature_10,signature_11,signature_12,signature_13,signature_14,signature_15,signature_16,signature_17,confirmDate1,
      confirmDate2,
      confirmDate3,
      confirmDate4,
      confirmDate5,
      confirmDate6,
      confirmDate7,
      confirmDate8,
      confirmDate9,
      confirmDate10,
      confirmDate11,
      confirmDate12,
      confirmDate13,
      confirmDate14,
    
      confirmDate15,
      confirmDate16,
      confirmDate17,
      confirmDate18} = data
console.log("++++++++++++++++",data)
this.setState({printName1,printName2,printName3,printName4,printName5,printName6,printName7,printName8,printName9,printName10,printName12,printName13,printName14,printName15,printName16,printName17,printName11,signature_1,signature_2,signature_3,signature_4,signature_5,signature_6,signature_7,signature_8,signature_9,signature_10,signature_11,signature_12,signature_13,signature_14,signature_15,signature_16,signature_17,confirmDate1,
  confirmDate2,
  confirmDate3,
  confirmDate4,
  confirmDate5,
  confirmDate6,
  confirmDate7,
  confirmDate8,
  confirmDate9,
  confirmDate10,
  confirmDate11,
  confirmDate12,
  confirmDate13,
  confirmDate14,

  confirmDate15,
  confirmDate16,
  confirmDate17,
  confirmDate18})
  }})





}

   
    Orientation.lockToLandscape();
    Orientation.addOrientationListener(this._orientationDidChange);
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      console.warn('portartaitttttttttttttt');
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  };

  componentWillUnmount() {
    Orientation.lockToPortrait();
    Orientation.getOrientation((err, orientation) => {
      console.warn(`Current Device Orientation: ${orientation}`);
    });

    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
  }





  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };
  
  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };
  
  handleDatePicked = (datetime) => {
    this.setState({isDateTimePickerVisible: false});
  
    if (this.state.status == 1) {
      this.setState({
        confirmDate1: moment(datetime).format('MM/DD/YYYY'),
      });
    } else if (this.state.status == 2) {
      this.setState({
        confirmDate2: moment(datetime).format('MM/DD/YYYY'),
      });
    } else if (this.state.status == 3) {
      this.setState({
        confirmDate3: moment(datetime).format('MM/DD/YYYY'),
      });
    } else if(this.state.status==4) {
      this.setState({
        confirmDate4: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==5) {
      this.setState({
        confirmDate5: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==6) {
      this.setState({
        confirmDate6: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==7) {
      this.setState({
        confirmDate7: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==8) {
      this.setState({
        confirmDate8: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==9) {
      this.setState({
        confirmDate9: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==10) {
      this.setState({
        confirmDate10: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==11) {
      this.setState({
        confirmDate11: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==12) {
      this.setState({
        confirmDate12: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==13) {
      this.setState({
        confirmDate13: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==14) {
      this.setState({
        confirmDate14: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==15) {
      this.setState({
        confirmDate15: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==16) {
      this.setState({
        confirmDate16: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==17) {
      this.setState({
        confirmDate17: moment(datetime).format('MM/DD/YYYY'),
      });
    }else if(this.state.status==18) {
      this.setState({
        confirmDate18: moment(datetime).format('MM/DD/YYYY'),
      });
    }
  };
  





  render() {
    const {
      brief_required,
      date_briefing,
      name_briefing,
      team_leader,
      contents,
      marker,
      cars,
      hgv,
      total,
      remarks,
      agreed,
      visibility,
      sight,
      time_of_count,
      time_of_count2,
      marker_post,
      cars2,
      hgv2,
      total2,
      remarks2,
      time_of_count3,
      marker_post2,
      cars3,
      hgv3,
      total3,
      remarks3,
      statement,
      drawing_br,
      risk_br,
      printName1,printName2,printName3,printName4,printName5,printName6,printName7,printName8,printName9,printName10,printName12,printName13,printName14,printName15,printName16,printName17,printName11,signature_1,signature_2,signature_3,signature_4,signature_5,signature_6,signature_7,signature_8,signature_9,signature_10,signature_11,signature_12,signature_13,signature_14,signature_15,signature_16,signature_17,confirmDate1,
      confirmDate2,
      confirmDate3,
      confirmDate4,
      confirmDate5,
      confirmDate6,
      confirmDate7,
      confirmDate8,
      confirmDate9,
      confirmDate10,
      confirmDate11,
      confirmDate12,
      confirmDate13,
      confirmDate14,
  
      confirmDate15,
      confirmDate16,
      confirmDate17,
      confirmDate18
    }= this.state
    return (
      <View style={{flex: 1, backgroundColor: '#FBFBFB'}}>
        <HeaderWithBack
          title="BRIEFING RECORD AND PRE-WORK SAFETY CHECKS"
          goBack={() =>
            Alert.alert('Amber RTM', 'Do you want to Exit?', [
              {text: 'No', style: 'cancel'},
              {text: 'Yes', onPress: () => this.props.navigation.goBack()},
            ])
          }
        />

        <ScrollView
          style={{margin: 8, backgroundColor: '', flex: 1}}>
{/* first row */}

<View style={{backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderWidth:2,marginBottom:8}}>
<Text style={{fontSize:16,fontWeight:'bold'}}>BRIFING RECORD AND PRE-WORK SAFTEY CHECKS (if in doubt, contact Project Manager)</Text>
</View>
{/* second row */}

<View style={{flexDirection:'row',}}>

<View style={{flex:3,borderWidth:2,backgroundColor:'#F9BF8F',justifyContent:'center',paddingVertical:3,paddingLeft:2,marginRight:3}}>
<Text style={{fontSize:15}}>Is Brefing Recquired :</Text>
</View>
<View style={{flex:2,borderWidth:2,backgroundColor:'#FAFAFA',justifyContent:'space-around',alignItems:'center',flexDirection:'row',marginRight:3}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({brief_required: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:brief_required ==='1' ?'#FF1493':'#fff', borderWidth:brief_required ==='1'?0:1}]}>
                {brief_required === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({brief_required: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:brief_required ==='0' ?'#FF1493':'#fff', borderWidth:brief_required ==='0'?0:1}]}>
                {brief_required === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:5,borderWidth:2,backgroundColor:'#F9BF8F',flexDirection:'row'}}>
<View style={{flex:3,backgroundColor:'#F9BF8F',justifyContent:'center',paddingLeft:3,borderRightWidth:2}}>

  <Text style={{fontSize:15}}>Date :</Text>
</View>
<View style={{flex:2,backgroundColor:'#FAFAFA',justifyContent:'space-between',paddingLeft:3,flexDirection:'row',alignItems:'center',paddingRight:3}}>

  <Text style={{fontSize:14,color:'#989898',fontWeight:'bold'}}>{this.state.confirmDate1?this.state.confirmDate1 :date_briefing ? date_briefing :"MM/DD/YYYY"}</Text>
  <TouchableOpacity  onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 1,
                      });
                    }}>
                  <Icon   name="calendar" size={17} />
                  </TouchableOpacity>
</View>
</View>
</View>

<View style={{marginVertical:8,flexDirection:'row'}}>
<View style={{flex:3,backgroundColor:'#F9BF8F',borderWidth:2,marginRight:3,justifyContent:'center',paddingVertical:3}}>
  <Text style={{fontSize:14,textAlign:'center'}}>Name of Team Leader Giving Brifing :</Text>
</View>
<View style={{flex:7,backgroundColor:'#FAFAFA',borderWidth:2,marginRight:3,justifyContent:'center',paddingVertical:3}}>
  <TextInput
  value={name_briefing}
  onChangeText={name_briefing=>this.setState({name_briefing})}
  style={{padding:0,fontSize:15,fontWeight:'bold',paddingHorizontal:5}}
  />
</View>
</View>



{/* Fourth Row */}

<View style={{backgroundColor:'#F9BF8F',paddingVertical:3,justifyContent:'center',borderWidth:2}}>
<Text style={{fontWeight:'bold',fontSize:15,paddingHorizontal:3}}>
  TEAM LEADER - Note any additional items discussed/ variations from the above methods discussed during the brifing
</Text>
</View>
<View style={{borderWidth:2,borderTopWidth:0,paddingVertical:5,justifyContent:'center',marginBottom:8}}>
<TextInput
value={team_leader}
onChangeText={team_leader=>this.setState({team_leader})}
numberOfLines={3}
style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5}}
/>
</View>

{/* fifith row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',marginBottom:8}}>
<View style={{flex:4,justifyContent:'center',borderWidth:2,marginRight:3,paddingVertical:2}}>
  <Text style={{textAlign:'center'}}>Have the contents of the site assessment and necessary controls been communicated within the briefing? </Text>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',flexDirection:'row',borderWidth:2}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({contents: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:contents ==='1' ?'#FF1493':'#fff', borderWidth:contents ==='1'?0:1}]}>
                {contents === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({contents: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:contents ==='0' ?'#FF1493':'#fff', borderWidth:contents ==='0'?0:1}]}>
                {contents === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
</View>

<View style={{backgroundColor:'#F9BF8F',justifyContent:'center',paddingVertical:3,borderWidth:2,paddingHorizontal:3}}>
<Text style={{fontWeight:'bold',fontSize:15}}>
  By signing below, you confirm that you have been briefed on the above items and understands your responsibilites : 
</Text>
</View>

{/* child row */}

<View style={{flexDirection:'row',backgroundColor:'#F9BF8F',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',alignItems:'center',borderRightWidth:2,paddingVertical:3}}>
  <Text style={{fontSize:15}}>Print name</Text>
</View>
<View style={{flex:1,justifyContent:'center',alignItems:'center',borderRightWidth:2,paddingVertical:3}}>
  <Text style={{fontSize:15}}>Present</Text>
</View>
<View style={{flex:1,justifyContent:'center',alignItems:'center',borderRightWidth:0,paddingVertical:3}}>
  <Text style={{fontSize:15}}>Date</Text>
</View>
</View>


{/* child one row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName1}
onChangeText={printName1=>this.setState({printName1})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_1: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_1 ==='1' ?'#FF1493':'#fff', borderWidth:signature_1 ==='1'?0:1}]}>
                {signature_1 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_1: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_1 ==='0' ?'#FF1493':'#fff', borderWidth:signature_1 ==='0'?0:1}]}>
                {signature_1 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
            <Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate2 ?(this.state.confirmDate2): "MM/DD/YYYY"}</Text>
  <TouchableOpacity  onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 2,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>


{/* child 2 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName2}
onChangeText={printName2=>this.setState({printName2})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_2: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_2 ==='1' ?'#FF1493':'#fff', borderWidth:signature_2 ==='1'?0:1}]}>
                {signature_2 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_2: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_2 ==='0' ?'#FF1493':'#fff', borderWidth:signature_2 ==='0'?0:1}]}>
                {signature_2 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate3 ? (this.state.confirmDate3): "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 3,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 3 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName3}
onChangeText={printName3=>this.setState({printName3})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_3: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_3 ==='1' ?'#FF1493':'#fff', borderWidth:signature_3 ==='1'?0:1}]}>
                {signature_3 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_3: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_3 ==='0' ?'#FF1493':'#fff', borderWidth:signature_3 ==='0'?0:1}]}>
                {signature_3 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate4 ? (this.state.confirmDate4): "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 4,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 4 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName4}
onChangeText={printName4=>this.setState({printName4})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_4: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_4 ==='1' ?'#FF1493':'#fff', borderWidth:signature_4 ==='1'?0:1}]}>
                {signature_4 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_4: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_4 ==='0' ?'#FF1493':'#fff', borderWidth:signature_4 ==='0'?0:1}]}>
                {signature_4 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate5 ?(this.state.confirmDate5): "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 5,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 5 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName5}
onChangeText={printName5=>this.setState({printName5})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_5: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_5 ==='1' ?'#FF1493':'#fff', borderWidth:signature_5 ==='1'?0:1}]}>
                {signature_5 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_5: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_5 ==='0' ?'#FF1493':'#fff', borderWidth:signature_5 ==='0'?0:1}]}>
                {signature_5 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate6 ? (this.state.confirmDate6): "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 6,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 6 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName6}
onChangeText={printName6=>this.setState({printName6})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_6: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_6 ==='1' ?'#FF1493':'#fff', borderWidth:signature_6 ==='1'?0:1}]}>
                {signature_6 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_6: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_6 ==='0' ?'#FF1493':'#fff', borderWidth:signature_6 ==='0'?0:1}]}>
                {signature_6 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate7 ?(this.state.confirmDate7): "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 7,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 7 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName7}
onChangeText={printName7=>this.setState({printName7})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_7: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_7 ==='1' ?'#FF1493':'#fff', borderWidth:signature_7 ==='1'?0:1}]}>
                {signature_7 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_7: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_7 ==='0' ?'#FF1493':'#fff', borderWidth:signature_7 ==='0'?0:1}]}>
                {signature_7 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate8 ? (this.state.confirmDate8): "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 8,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 8 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName8}
onChangeText={printName8=>this.setState({printName8})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_8: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_8 ==='1' ?'#FF1493':'#fff', borderWidth:signature_8 ==='1'?0:1}]}>
                {signature_8 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_8: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_8 ==='0' ?'#FF1493':'#fff', borderWidth:signature_8 ==='0'?0:1}]}>
                {signature_8 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate9 ? this.state.confirmDate9: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 9,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 9 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName9}
onChangeText={printName9=>this.setState({printName9})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_9: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_9 ==='1' ?'#FF1493':'#fff', borderWidth:signature_9 ==='1'?0:1}]}>
                {signature_9 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_9: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_9 ==='0' ?'#FF1493':'#fff', borderWidth:signature_9 ==='0'?0:1}]}>
                {signature_9 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate10 ? this.state.confirmDate10: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 10,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 10 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName10}
onChangeText={printName10=>this.setState({printName10})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_10: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_10 ==='1' ?'#FF1493':'#fff', borderWidth:signature_10 ==='1'?0:1}]}>
                {signature_10 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_10: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_10 ==='0' ?'#FF1493':'#fff', borderWidth:signature_10 ==='0'?0:1}]}>
                {signature_10 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate11 ?this.state.confirmDate11: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 11,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 11 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName11}
onChangeText={printName11=>this.setState({printName11})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_11: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_11 ==='1' ?'#FF1493':'#fff', borderWidth:signature_11 ==='1'?0:1}]}>
                {signature_11 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_11: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_11 ==='0' ?'#FF1493':'#fff', borderWidth:signature_11 ==='0'?0:1}]}>
                {signature_11 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate12 ?this.state.confirmDate12: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 12,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 12 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName12}
onChangeText={printName12=>this.setState({printName12})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_12: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_12 ==='1' ?'#FF1493':'#fff', borderWidth:signature_12 ==='1'?0:1}]}>
                {signature_12 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_12: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_12 ==='0' ?'#FF1493':'#fff', borderWidth:signature_12 ==='0'?0:1}]}>
                {signature_12 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate13 ? this.state.confirmDate13: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 13,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 13 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName13}
onChangeText={printName13=>this.setState({printName13})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_13: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_13 ==='1' ?'#FF1493':'#fff', borderWidth:signature_13 ==='1'?0:1}]}>
                {signature_13 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_13: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_13 ==='0' ?'#FF1493':'#fff', borderWidth:signature_13 ==='0'?0:1}]}>
                {signature_13 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate14 ?this.state.confirmDate14: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 14,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 14 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName14}
onChangeText={printName14=>this.setState({printName14})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_14: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_14 ==='1' ?'#FF1493':'#fff', borderWidth:signature_14 ==='1'?0:1}]}>
                {signature_14 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_14: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_14 ==='0' ?'#FF1493':'#fff', borderWidth:signature_14 ==='0'?0:1}]}>
                {signature_14 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate15 ? this.state.confirmDate15: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 15,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 15 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName15}
onChangeText={printName15=>this.setState({printName15})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_15: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_15 ==='1' ?'#FF1493':'#fff', borderWidth:signature_15 ==='1'?0:1}]}>
                {signature_15 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_15: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_15 ==='0' ?'#FF1493':'#fff', borderWidth:signature_15 ==='0'?0:1}]}>
                {signature_15 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate16 ?this.state.confirmDate16: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 16,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 16 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName16}
onChangeText={printName16=>this.setState({printName16})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_16: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_16 ==='1' ?'#FF1493':'#fff', borderWidth:signature_16 ==='1'?0:1}]}>
                {signature_16 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_16: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_16 ==='0' ?'#FF1493':'#fff', borderWidth:signature_16 ==='0'?0:1}]}>
                {signature_16 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate17 ? this.state.confirmDate17: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 17,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

{/* child 17 row */}

<View style={{flexDirection:'row',backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2,paddingVertical:3}}>
<TextInput
value={printName17}
onChangeText={printName17=>this.setState({printName17})}

style={{padding:0,fontWeight:'bold',fontSize:15,paddingHorizontal:5,textAlign:'center'}}
/>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:2,paddingVertical:3,flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({signature_17: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:signature_17 ==='1' ?'#FF1493':'#fff', borderWidth:signature_17 ==='1'?0:1}]}>
                {signature_17 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({signature_17: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:signature_17 ==='0' ?'#FF1493':'#fff', borderWidth:signature_17 ==='0'?0:1}]}>
                {signature_17 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:1,justifyContent:'space-around',alignItems:'center',borderRightWidth:0,paddingVertical:3,flexDirection:'row'}}>
<Text style={{fontSize:15,color:'#989898'}}>{this.state.confirmDate18 ? this.state.confirmDate18: "MM/DD/YYYY"}</Text>
  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 18,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
</View>

<View style={{flexDirection:'row',marginTop:8,backgroundColor:'#F9BF8F',borderWidth:2}}>
<View style={{flex:1,justifyContent:'center',alignItems:'center',borderRightWidth:2}}>
  <Text style={{fontSize:14}}>No.</Text>
</View>

<View style={{flex:2,borderRightWidth:2,}}>
  <Text style={{fontSize:14,textAlign:'center'}}>Time of Count</Text>
</View>

<View style={{flex:2,borderRightWidth:2,justifyContent:'center'}}>
  <Text style={{fontSize:14,textAlign:'center'}}>Marker Post</Text>
</View>
<View style={{flex:6,borderRightWidth:2}}>

<View style={{flex:1,borderBottomWidth:2}}>
  <Text style={{textAlign:'center'}}>Vehicles per 3 minutes</Text>
</View>
<View style={{flex:1,flexDirection:'row'}}>
<View style={{flex:1,borderRightWidth:2,justifyContent:'center',alignItems:'center'}}>
  <Text>Cars</Text>
</View>
<View style={{flex:1,borderRightWidth:2,justifyContent:'center',alignItems:'center'}}>
  <Text>HGV's</Text>
</View>
<View style={{flex:1,borderRightWidth:0,justifyContent:'center',alignItems:'center'}}>
  <Text>Total</Text>
</View>
</View>

</View>

<View style={{flex:6,justifyContent:'center',alignItems:'center',borderRightWidth:0}}>
  <Text style={{fontSize:14}}>Remarks, Incidents and Stops</Text>
</View>

</View>

{/* 
<TextInput
 
  style={{padding:0,fontSize:15,fontWeight:'bold',paddingHorizontal:5}}
  />
*/}
{/* child */}

<View style={{flexDirection:'row',marginTop:0,backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2}}>
  <Text style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}>1</Text>

</View>

<View style={{flex:2,borderRightWidth:2,}}>
<TextInput
 value={this.state.time_of_count}
 onChangeText={time_of_count=>this.setState({time_of_count})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>

<View style={{flex:2,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={this.state.marker}
onChangeText={marker=>this.setState({marker})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:6,borderRightWidth:2,flexDirection:'row'}}>



<View style={{flex:1,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={this.state.cars}
onChangeText={cars=>this.setState({cars})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:1,borderRightWidth:2,justifyContent:'center',}}>
<TextInput
value={this.state.hgv}
onChangeText={hgv=>this.setState({hgv})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:1,borderRightWidth:0,justifyContent:'center'}}>
<TextInput
value={this.state.total}
onChangeText={total=>this.setState({total})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>


</View>

<View style={{flex:6,justifyContent:'center',alignItems:'center',borderRightWidth:0}}>
<TextInput
value={this.state.remarks}
onChangeText={remarks=>this.setState({remarks})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>

</View>



{/* child */}

<View style={{flexDirection:'row',marginTop:0,backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2}}>
<Text style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}>2</Text>
</View>

<View style={{flex:2,borderRightWidth:2,}}>
<TextInput
value={this.state.time_of_count2}
onChangeText={time_of_count2=>this.setState({time_of_count2})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>

<View style={{flex:2,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={this.state.marker_post}
onChangeText={marker_post=>this.setState({marker_post})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:6,borderRightWidth:2,flexDirection:'row'}}>



<View style={{flex:1,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={this.state.cars2}
onChangeText={cars2=>this.setState({cars2})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:1,borderRightWidth:2,justifyContent:'center',}}>
<TextInput
value={this.state.hgv2}
onChangeText={hgv2=>this.setState({hgv2})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:1,borderRightWidth:0,justifyContent:'center'}}>
<TextInput
value={this.state.total2}
onChangeText={total2=>this.setState({total2})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>


</View>

<View style={{flex:6,justifyContent:'center',alignItems:'center',borderRightWidth:0}}>
<TextInput
value={this.state.remarks2}
onChangeText={remarks2=>this.setState({remarks2})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>

</View>



{/* child */}

<View style={{flexDirection:'row',marginTop:0,backgroundColor:'#FAFAFA',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,justifyContent:'center',borderRightWidth:2}}>
<Text style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}>3</Text>
</View>

<View style={{flex:2,borderRightWidth:2,}}>
<TextInput
value={this.state.time_of_count3}
onChangeText={time_of_count3=>this.setState({time_of_count3})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>

<View style={{flex:2,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={this.state.marker_post2}
onChangeText={marker_post2=>this.setState({marker_post2})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:6,borderRightWidth:2,flexDirection:'row'}}>



<View style={{flex:1,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={this.state.cars3}
onChangeText={cars3=>this.setState({cars3})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:1,borderRightWidth:2,justifyContent:'center',}}>
<TextInput
value={this.state.hgv3}
onChangeText={hgv3=>this.setState({hgv3})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>
<View style={{flex:1,borderRightWidth:0,justifyContent:'center'}}>
<TextInput
value={this.state.total3}
onChangeText={total3=>this.setState({total3})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>


</View>

<View style={{flex:6,justifyContent:'center',alignItems:'center',borderRightWidth:0}}>
<TextInput
value={this.state.remarks3}
onChangeText={remarks3=>this.setState({remarks3})}
style={{textAlign:'center',fontWeight:'bold',padding:0,paddingHorizontal:3}}
/>
</View>

</View>



{/* second row */}

<View style={{flexDirection:'row',marginVertical:8}}>

<View style={{flex:3,borderWidth:2,backgroundColor:'#F9BF8F',justifyContent:'center',paddingVertical:3,paddingLeft:2,borderRightWidth:0}}>
<Text style={{fontSize:15}}>Is traffic count within agreed limits ?</Text>
</View>
<View style={{flex:2,borderWidth:2,backgroundColor:'#FAFAFA',justifyContent:'space-around',alignItems:'center',flexDirection:'row',borderRightWidth:0}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({agreed: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:agreed ==='1' ?'#FF1493':'#fff', borderWidth:agreed ==='1'?0:1}]}>
                {agreed === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({agreed: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:agreed ==='0' ?'#FF1493':'#fff', borderWidth:agreed ==='0'?0:1}]}>
                {agreed === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:5,borderWidth:2,backgroundColor:'#F9BF8F',flexDirection:'row'}}>
<View style={{flex:3,backgroundColor:'#F9BF8F',justifyContent:'center',paddingLeft:3,borderRightWidth:2}}>

  <Text style={{fontSize:15}}>Is visibility 400m or more ?</Text>
</View>
<View style={{flex:2,backgroundColor:'#FAFAFA',justifyContent:'space-between',paddingLeft:3,flexDirection:'row',alignItems:'center',paddingRight:3}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({visibility: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:visibility ==='1' ?'#FF1493':'#fff', borderWidth:visibility ==='1'?0:1}]}>
                {visibility === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({visibility: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:visibility ==='0' ?'#FF1493':'#fff', borderWidth:visibility ==='0'?0:1}]}>
                {visibility === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>

</View>
</View>
</View>


{/* second row */}

<View style={{flexDirection:'row',marginVertical:8}}>

<View style={{flex:8,borderWidth:2,backgroundColor:'#F9BF8F',justifyContent:'center',paddingVertical:3,paddingLeft:2,borderRightWidth:0}}>
<Text style={{fontSize:15}}>Are sight lines greater than 400m ?</Text>
</View>
<View style={{flex:2,borderWidth:2,backgroundColor:'#FAFAFA',justifyContent:'space-around',alignItems:'center',flexDirection:'row'}}>
<View style={{flexDirection:'row'}}>

<TouchableOpacity
             onPress={() => this.setState({sight: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:sight ==='1' ?'#FF1493':'#fff', borderWidth:sight ==='1'?0:1}]}>
                {sight === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({sight: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:sight ==='0' ?'#FF1493':'#fff', borderWidth:sight ==='0'?0:1}]}>
                {sight === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>

</View>

<TouchableButton
onPress={()=>this.onSubmit()}
title="SUBMIT"
/>

          </ScrollView>
          <Loader isLoader={this.state.loading}></Loader>
          {/* <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          <View>
              <View style={{backgroundColor: '', marginBottom: 100}}>
                <DatePicker
                mode="date"
                  date={this.state.setDate}
                  onDateChange={(setDates) => this.setState({setDate:moment(setDates).format('MM-DD- YYYY')})}
                  androidVariant="nativeAndroid"
                />
              </View>
              <View style={{flexDirection: 'row',borderTopWidth:1}}>
              <TouchableOpacity
              onPress={() => this.setState({open:false,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
if(this.state.optionDate===1){
  this.setState({open:false,confirmDate1:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===2){
  this.setState({open:false,confirmDate2:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===3){
  this.setState({open:false,confirmDate3:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===4){
  this.setState({open:false,confirmDate4:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===5){
  this.setState({open:false,confirmDate5:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===6){
  this.setState({open:false,confirmDate6:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===7){
  this.setState({open:false,confirmDate7:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===8){
  this.setState({open:false,confirmDate8:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===9){
  this.setState({open:false,confirmDate9:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===10){
  this.setState({open:false,confirmDate10:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===11){
  this.setState({open:false,confirmDate11:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===12){
  this.setState({open:false,confirmDate12:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===13){
  this.setState({open:false,confirmDate13:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===14){
  this.setState({open:false,confirmDate14:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===15){
  this.setState({open:false,confirmDate15:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===16){
  this.setState({open:false,confirmDate16:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===17){
  this.setState({open:false,confirmDate17:this.state.setDate,modalVisible:!this.state.modalVisible})
}else if(this.state.optionDate===18){
  this.setState({open:false,confirmDate18:this.state.setDate,modalVisible:!this.state.modalVisible})
}



                  }}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>Ok</Text>
                </TouchableOpacity>
                
              </View>
            </View>

          </View>
        </View>
      </Modal> */}

{this.state.isDateTimePickerVisible ? (
            <DateTimePicker
              mode={this.state.mode}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.handleDatePicked}
              onCancel={this.hideDateTimePicker}
              is24Hour={false}
             // onChange={this.handleDatePicked}
              isDarkModeEnabled= {false}
              textColor= "#000000	"
              date = {this.state.initialDateTime}

             //  onConfirm={(date)=>this.hideDateTimePicker(date)}
            />
          ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {

    backgroundColor: "white",
    borderRadius: 20,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  
  outerBox: {
    height: 19,
    width: 19,
    borderWidth: 2,
  
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  label: {fontSize: 15},
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 0,
  },
  radioBox: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});
