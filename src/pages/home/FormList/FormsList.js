import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  CheckBox,
  Alert,
} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
import Icon from '../../../component/Icon';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import TouchableButton from '../../../component/TouchableButton';
import Loader from '../../../WebAPI/Loader';
import {update_status,requestPostApiMedia} from '../../../WebAPI/Service'
class JobContentBox extends Component {
  render() {
    return (
      <View
        style={{
          borderWidth: 3,
          borderColor: '#F8981D',
          margin: 10,
          padding: 10,
          flexDirection: 'row',
          backgroundColor: '#fff',
          borderRadius: 8,
          paddingBottom: 15,
          marginBottom: 10,
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Job Id</Text>
            <Text numberOfLines={2} style={{color: '#6C6C6C'}}>
              {this.props.jobId}
            </Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Start Date</Text>
            <Text style={{color: '#6C6C6C'}}>{this.props.startDate}</Text>
          </View>
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingRight: 5,
          }}>
          <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: 5,
            }}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Location</Text>
            <Text style={{textAlign: 'justify', color: '#6C6C6C'}}>
              {this.props.location}
            </Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>End Date</Text>
            <Text style={{color: '#6C6C6C'}}>{this.props.endDate}</Text>
          </View>
        </View>
      </View>
    );
  }
}

class FormOptionsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //  isSelected: false,
    };
  }

  render() {
    return (
      <TouchableOpacity
        style={{
          borderWidth: 3,
          borderColor: '#F8981D',
          margin: 10,
          padding: 8,
          flexDirection: 'row',
          backgroundColor: '#F5F1F0',
          borderRadius: 8,
          marginBottom: 0,
          marginTop: 8,
        }}
        onPress={this.props.onPress}>
        <View
          style={{flex: 4, justifyContent: 'center', alignItems: 'flex-start'}}>
          <Text style={{fontWeight: 'bold'}}>{this.props.title}</Text>
        </View>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          {/* {this.props.disabled == 1 ? (
            <CheckBox
              value={this.props.isSelected == 1 ? true : false}
              onValueChange={() =>
                this.setState({isSelected: !this.state.isSelected})
              }
            />
          ) : null} */}

          {this.props.isSelected == 1 ? (
            <View
              style={{
                height: 18,
                width: 18,
                backgroundColor: '#FF4081',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon name="check" size={15} color="#fff" />
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  }
}

export default class Formslist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataFromParam: [],
      loading: true,
    };
  }

  async componentDidMount() {
    const data = await this.props.navigation.state.params.data;
    if (data) {
      this.setState({dataFromParam: data, loading: false});
      console.warn('data from param', this.state.dataFromParam);
    } else {
      this.setState({loading: false});
    }
  }

  componentWillUnmount() {}

async onSubmit(){
  const {
    
    tpra_update_status,
    tsaf_update_status,
    briefing_update_status,
    tws_update_status,
    loading_list2_update_status,
    op_uploads_update_status,
  
  } = this.state.dataFromParam;

this.setState({loading: true});
const formData = new FormData();
formData.append('user_id', this.props.navigation.state.params.data.operator_id);
formData.append('q_no', this.props.navigation.state.params.data.q_no);
formData.append('is_completed', 1);


if(tpra_update_status==1&&
  tsaf_update_status==1&&
  briefing_update_status==1&&
  tws_update_status==1&&
  loading_list2_update_status==1&&
  op_uploads_update_status==1){


    const {responseJson,err} = await requestPostApiMedia(update_status,formData,'POST')

    if(responseJson.status){
      this.setState({loading: false});
      Alert.alert('',responseJson.message,[
        {
          text:'OK',
          onPress:()=>this.props.navigation.navigate('Jobs')
        }
      ])
    
    }else{
      this.setState({loading:false});
    console.warn(err)
    }
    


    
}
else{
  this.setState({loading:false});
  alert("Make sure please complete  ")
}


}


  render() {
    const {
      name_number,
      site_address,
      date_tpra,
      finish_date,
      tpra_update_status,
      tsaf_update_status,
      briefing_update_status,
      tws_update_status,
      loading_list2_update_status,
      op_uploads_update_status,
      q_no,
      operator_id,
    } = this.state.dataFromParam;
    const data = {
      operator_id: operator_id,
      q_no: q_no,
    };

    console.warn("nnnnnnnnnnnnn",this.props.navigation.state.params.data.operator_id)
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="FORMS LIST"
          goBack={() => this.props.navigation.goBack()}
        />
        <JobContentBox
          jobId={q_no}
          location={site_address}
          startDate={date_tpra}
          endDate={finish_date}
        />
        <View
          style={{
            borderWidth: 3,
            borderColor: '#F8981D',
            marginLeft: 10,
            marginRight: '80%',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 6,
            paddingTop: 5,
            paddingBottom: 5,
          }}>
          <Text style={{fontWeight: 'bold', fontSize: 16}}>FORMS</Text>
        </View>
        <FormOptionsContainer
          title="TM PLANNING AND RESOURSE ALLOCATION"
          onPress={() =>
            tpra_update_status == 1
              ? Alert.alert(
                  'Amber RTM',
                  'Do you Wish to Edit?',
                  [
                    {
                      text: 'No',
                      // onPress: () => console.warn("Cancel Pressed"),
                      style: 'cancel',
                    },
                    {
                      text: 'Yes',
                      onPress: () =>
                        this.props.navigation.navigate(
                          'TMPlanAndResAlloc',
                          data,
                        ),
                    },
                  ],
                  {cancelable: false},
                )
              : this.props.navigation.navigate('TMPlanAndResAlloc', data)
          }
          isSelected={tpra_update_status}
        />
        <FormOptionsContainer
          title="LOADING LIST"
          onPress={() =>
            loading_list2_update_status == 1
              ? Alert.alert(
                  'Amber RTM',
                  'Do you Wish to Edit?',
                  [
                    {
                      text: 'No',
                      // onPress: () => console.warn("Cancel Pressed"),
                      style: 'cancel',
                    },
                    {
                      text: 'Yes',
                      onPress: () =>
                        this.props.navigation.navigate('LoadingList',data),
                    },
                  ],
                  {cancelable: false},
                )
              : this.props.navigation.navigate('LoadingList',data)
          }
          isSelected={loading_list2_update_status}
        />
        <FormOptionsContainer
          title="TM SITE ASSESMENT FORM"
          onPress={() =>
            tsaf_update_status == 1
              ? Alert.alert(
                  'Amber RTM',
                  'Do you Wish to Edit?',
                  [
                    {
                      text: 'No',
                      onPress: () => console.warn('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {
                      text: 'Yes',
                      onPress: () =>
                        this.props.navigation.navigate('TMSiteAssesment',data),
                    },
                  ],
                  {cancelable: false},
                )
              : this.props.navigation.navigate('TMSiteAssesment',data)
          }
          isSelected={tsaf_update_status}
        />
        <FormOptionsContainer
          title="RECORD & PRE-WORK SAFETY CHECK"
          onPress={() =>
            briefing_update_status == 1
              ? Alert.alert(
                  'Amber RTM',
                  'Do you Wish to Edit?',
                  [
                    {
                      text: 'No',
                      onPress: () => console.warn('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {
                      text: 'Yes',
                      onPress: () =>
                        this.props.navigation.navigate(
                          'RecordAndPreWorkSafCheck',data
                        ),
                    },
                  ],
                  {cancelable: false},
                )
              : this.props.navigation.navigate('RecordAndPreWorkSafCheck',data)
          }
          isSelected={briefing_update_status}
        />
        <FormOptionsContainer
          title="TM WORKS SHEET"
          onPress={() =>
            tws_update_status == 1
              ? Alert.alert(
                  'Amber RTM',
                  'Do you Wish to Edit?',
                  [
                    {
                      text: 'No',
                      // onPress: () => console.warn("Cancel Pressed"),
                      style: 'cancel',
                    },
                    {
                      text: 'Yes',
                      onPress: () =>
                        this.props.navigation.navigate('TMWorkSheet',data),
                    },
                  ],
                  {cancelable: false},
                )
              : this.props.navigation.navigate('TMWorkSheet',data)
          }
          isSelected={tws_update_status}
        />
        <FormOptionsContainer
          title="UPLOAD IMAGES"
          disabled={true}
          isSelected={op_uploads_update_status}
          onPress={() => this.props.navigation.navigate('UploadImages',data)}
        />

        <TouchableButton title="SUBMIT" onPress={() => this.onSubmit()} />

        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
