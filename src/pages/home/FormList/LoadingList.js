import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Alert,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Image,
  FlatList
} from 'react-native';
import Orientation from 'react-native-orientation';
import HeaderWithBack from '../../../component/HeaderWithBack';

import TouchableButton from '../../../component/TouchableButton';
import Loader from '../../../WebAPI/Loader';
import {requestPostApiMedia,get_loading_list_item,get_all_inventory_items,requestGetApi,update_loading_list_data} from '../../../WebAPI/Service.js'

import CPicker from '../../../component/CustomPicker'


import AntDesign from 'react-native-vector-icons/AntDesign';
export default class LodingList extends Component {
  constructor(props) {
    super()
    this.state = {
      loading:false,
      country: 'uk',
      loadingList:[],
      loadingStack:[],
      showPicker:false ,
      pickerArr:[{},{},] ,
      selectedTempData:{},
      inventry_item:[],
      extracted_name:[],
      requested:[],
      loading_list_items_id:[],
      product:[],
      request:[],
      selectedIndex:0
      

    };
  }

  componentWillMount() {
 
    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
     // console.warn('portartait');
      // do something
    } else {
      // do something else
    }
  }
 

  async inventryItems(){

const body = {}

    const {responseJson,err} = await requestGetApi(get_all_inventory_items,body,'GET')
    

const data = await responseJson
return data
  }

async extractName(){

  let loadingArr = this.state.loadingList
  let aArr = []
console.warn("------",loadingArr)
  loadingArr.map((data,itemIndex)=>{
    let {product_id,product_type} = data
   
    product_id =product_type==2? product_id+"^$-VP":product_id+"^$-SP"
   let data_name=this.state.inventry_item.filter((data)=>{
 
  if(product_type==data.type && product_id==data.id){
       return data.name
     }
})
data.extractedName = data_name[0].name
data.varientId = product_id
aArr.push(data)
// data_name = data_name[0]
// let arr = this.state.extracted_name
// arr.push(data_name.name)

// this.setState({extracted_name:arr})

console.warn("*********************",data)
  })
  this.setState({loadingList:aArr})
}

async componentDidMount() {
//   this.setState({loading:true})
   
 const formData = new FormData()
 formData.append('q_no',this.props.navigation.state.params.q_no)
 
 
 const {responseJson,err} = await requestPostApiMedia(get_loading_list_item,formData,'POST')
console.warn("yeee",responseJson.loading_list)

let arr = []

 responseJson.loading_list.map((item,index)=>{
item.extractedName = ''
item.showClose = false
arr.push(item)
})

this.setState({loadingList:arr})

// Inventry Api
const data = await this.inventryItems()

const {all_items} = data

//console.warn("tycoon",data)

 all_items.filter((item,index)=>{
if(item.type==2){
 
//console.log(item)

const split_data = item.variableProductInfo.split(":::??")

//console.log(" splited array ::",split_data)

  split_data.filter((data)=>{

  const final_split = data.split(":::::")

//console.log("final split :::",final_split)

const name =item.name+" "+ final_split[1]
const id=  final_split[0]+"^$-VP"

const arr =this.state.inventry_item
arr.push({
  id,
  name,
  type:item.type
})

return this.setState({inventry_item:arr})
})

}else if(item.type==1){
  const arr =this.state.inventry_item
arr.push({
  id:item.id+"^$-SP",
  name:item.name,
  type:item.type
})

return this.setState({inventry_item:arr})

}
}
)

this.extractName()
    
    Orientation.lockToLandscape();
    Orientation.addOrientationListener(this._orientationDidChange);
  }
  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      console.warn('portartaitttttttttttttt');
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  };

  componentWillUnmount() {
     Orientation.lockToPortrait();
    Orientation.getOrientation((err, orientation) => {
     // console.warn(`Current Device Orientation: ${orientation}`);
    });

    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

onSubmit=async()=>{

const formData = new FormData()
formData.append('q_no',this.props.navigation.state.params.q_no)
let arr = []

this.state.loadingList.map((item,index)=>{
  if(item.extractedName == 'Select your Items' || item.requested == ''){
    arr.push(item)
  }
})

if(arr.length != 0){
  Alert.alert('Please fill all the details.')
  return 
}
this.state.loadingList.map((item,index)=>{
    formData.append('loading_list_items_id['+index+']',item.id)
    formData.append('product['+index+']',item.varientId)
    formData.append('request['+index+']',item.requested)
  })

  this.setState({loading: true});
  const {responseJson, err} = await requestPostApiMedia(
    update_loading_list_data,
    formData,
    'POST',
  );
  this.setState({loading: false});
  if (responseJson.status) {
    this.setState({loading: false});
    console.warn(responseJson)
    Alert.alert("",responseJson.message,[
      {
        text:"OK",
        onPress:()=>this.props.navigation.goBack()
        
      }
    ])
    
  } else {
    //console.log('checking', responseJson.data);
  }



// console.warn(this.state.loadingStack)



}

   handleValue2 = (index, value) => {
    this.state.loadingStack[index] = {...this.state.loadingStack[index], ['requested']: value};
    /*setState(prevObj => ({
      ...prevObj[index],
      value1: value,
    }));*/
   // console.warn(this.state.loadingStack);
  }

addRowOnStack(){

let dict = {
 created_date:"",
 delivered:"",
 extractedName:"Select your Items",
 id:"0",
 picked:"",
 product_id:"",
 product_type:"",
 q_no:"121",
 requested:"",
 updated_date:"",
 showClose:true,
 varientId:''
}

let arr = this.state.loadingList
arr.push(dict)
this.setState({loadingList:arr})

// let insertData= {
//   selectItem:'',
//   requested:''
// }
// let arr = this.state.loadingStack
// arr.push(insertData)
//  this.setState({ loadingStack:arr})
//   console.warn("checking",this.state.loadingStack)

}


handleValue1 = (index, value) => {

  let arr = this.state.loadingList

  arr.map((item,i)=>{

    if(i == index){
      item.requested = value
    }
  })
  this.setState({loadingList:arr})


  //this.state.loadingList[index] = {...this.state.loadingList[index], ['requested']: value};
}

renderItems1(a){

  const {item,index} = a
  const {requested,delivered,picked} = item

return (
  
<View style={{flexDirection:"row"}}>
<View style={{flex:5,backgroundColor:"#fff",borderWidth:2,borderRightWidth:0,padding:2,paddingHorizontal:4,justifyContent:"center",paddingVertical:5,borderTopWidth:0}}>
   
  <TouchableOpacity 
  onPress={()=>this.setState({showPicker:true,selectedIndex:index}) }
  style={{backgroundColor:'',flex:1,flexDirection:'row',alignItems:'center',borderWidth:.5}}>
<View style={{flex:5}}>
<Text numberOfLines={1}>{item.extractedName}</Text>
</View>

<View style={{flex:1}}>
<AntDesign
name="caretdown"
size={18}
/>
</View>
  </TouchableOpacity>
  
</View>

<View style={{flex:3,backgroundColor:"#DBDAD8",borderWidth:2,borderRightWidth:0,padding:2,justifyContent:"center",borderTopWidth:0}}>
<TextInput
value={requested}
keyboardType="number-pad"
onChangeText={e => this.handleValue1(index, e)}
style={{textAlign:'center'}}
/>
</View>
<View style={{flex:4,backgroundColor:"",borderWidth:2,borderRightWidth:0,padding:2,justifyContent:"center",alignItems:'center',borderTopWidth:0}}>
<Text style={{fontSize:15}}>{delivered}</Text>
</View>
<View style={{flex:4,backgroundColor:"#fff",borderWidth:2,padding:2,justifyContent:"center",alignItems:'center',borderTopWidth:0}}>
<Text style={{fontSize:15}}>{picked}</Text>
</View>
<View style={{flex:3,backgroundColor:"",borderWidth:2,borderRightWidth:2,padding:2,borderLeftWidth:0,borderTopWidth:0,justifyContent:"center",alignItems:'center'}}>
{
  item.showClose ? 

<TouchableOpacity onPress={()=>this.closeAction(index)}  style={{justifyContent:"center",alignItems:'center',paddingVertical:2,elevation:5}}>
   <Text style={{color:'green'}}>Close</Text>
  </TouchableOpacity>
:
null
}

</View>
</View>
)

}

renderItems2(a){
const {item,index} = a
// console.warn("indexing",index)
  return (
    
  <View style={{flexDirection:"row"}}>
  <View style={{flex:5,backgroundColor:"#fff",borderWidth:2,borderRightWidth:0,padding:2,paddingHorizontal:4,justifyContent:"center",paddingVertical:5,borderTopWidth:0}}>
     
    <TouchableOpacity 
    onPress={()=>this.setState({showPicker:true}) }
    style={{backgroundColor:'',flex:1,flexDirection:'row',alignItems:'center',borderWidth:.5}}>
  <View style={{flex:5}}>
  <Text numberOfLines={1}>{this.state.selectedTempData.val ? this.state.selectedTempData.val :"Select your Items"  }</Text>
  </View>
  
 <View style={{flex:1}}>
 <AntDesign
  name="caretdown"
  size={18}
  />
 </View>
    </TouchableOpacity>
    
  </View>
  
  <View style={{flex:3,backgroundColor:"#DBDAD8",borderWidth:2,borderRightWidth:0,padding:2,justifyContent:"center",borderTopWidth:0}}>
  <TextInput
  value={this.state.loadingStack[index]}
  keyboardType="number-pad"
  onChangeText={e => this.handleValue2(index, e)}
  style={{textAlign:'center'}}
  />
  </View>
  <View style={{flex:4,backgroundColor:"",borderWidth:2,borderRightWidth:0,padding:2,justifyContent:"center",alignItems:'center',borderTopWidth:0}}>
  <Text style={{fontSize:15}}>{}</Text>
  </View>
  <View style={{flex:4,backgroundColor:"#fff",borderWidth:2,padding:2,justifyContent:"center",alignItems:'center',borderTopWidth:0}}>
  <Text style={{fontSize:15}}>{}</Text>
  </View>
  <View style={{flex:3,backgroundColor:"",borderWidth:2,borderRightWidth:2,padding:2,borderLeftWidth:0,borderTopWidth:0,justifyContent:"center",alignItems:'center'}}>
  <TouchableOpacity onPress={()=>{
   const arr =  this.state.loadingStack
   arr.splice(index,1)
   
    this.setState({loadingStack:arr})
  }} style={{justifyContent:"center",alignItems:'center',paddingVertical:2,elevation:5}}>
   <Text style={{color:'green'}}>Close</Text>
  </TouchableOpacity>
  </View>
  </View>
  )
  
  }
  
  render() {

console.warn("chec",this.props.navigation.state.params.q_no)
  

    return (
      <View style={{flex: 1, backgroundColor: '#FBFBFB'}}>
        <HeaderWithBack
          title="LOADING LIST"
          goBack={() =>
            Alert.alert('Amber RTM', 'Do you want to Exit?', [
              {text: 'No', style: 'cancel'},
              {text: 'Yes', onPress: () => this.props.navigation.goBack()},
            ])
          }
        />
        <ScrollView
          style={{margin: 8, flex: 1}}
          
          >



<View style={{flexDirection:"row",}}>
<View style={{flex:5,backgroundColor:"#F9BF8F",borderWidth:2,borderRightWidth:0,padding:2,paddingHorizontal:4,justifyContent:"center",alignItems:'center',paddingVertical:5}}>
    <Text style={{fontSize:15}}>Item</Text>
</View>

<View style={{flex:3,backgroundColor:"#F9BF8F",borderWidth:2,borderRightWidth:0,padding:2,justifyContent:"center",alignItems:'center'}}>
<Text style={{fontSize:15}}>Requested</Text>
</View>
<View style={{flex:4,backgroundColor:"#F9BF8F",borderWidth:2,borderRightWidth:0,padding:2,justifyContent:"center",alignItems:'center'}}>
<Text style={{fontSize:15}}>Delivered</Text>
</View>
<View style={{flex:4,backgroundColor:"#F9BF8F",borderWidth:2,padding:2,justifyContent:"center",alignItems:'center'}}>
<Text style={{fontSize:15}}>Picked up</Text>
</View>
<View style={{flex:3,backgroundColor:"#5EA03F",borderWidth:2,borderRightWidth:2,padding:2,borderLeftWidth:0}}>
<TouchableOpacity onPress={()=>this.addRowOnStack()} style={{justifyContent:"center",alignItems:'center',paddingVertical:2}}>
 <Text style={{color:'#fff'}}>+ Add Items</Text>
</TouchableOpacity>
</View>
</View>


{/* //loading List */}

<FlatList
data={this.state.loadingList}

renderItem={(item)=>this.renderItems1(item)}
/>

{/* 
<FlatList
data={this.state.loadingStack}
renderItem={(a)=>this.renderItems2(a)}
/> */}


<TouchableButton
title="SUBMIT"
onPress={()=>this.onSubmit()}
/>

          </ScrollView>

          <Loader isLoader={this.state.loading}></Loader>
          <CPicker

showPicker={this.state.showPicker} 
arr = {this.state.inventry_item}
 handleClose={()=>this.setState({showPicker:false})} 
 title={"Select Options"}

handleSubmit={this.handleSubmit}/>
      </View>
    );
  }

  handleSubmit = (val,index)=>{
    // const data= {
    //   val,index
    // }

    // created_date:"2020-11-10 02:25:10"
// delivered:"0"
// id:"1"
// picked:"0"
// product_id:"3"
// product_type:"1"
// q_no:"121"
// requested:"10"
// updated_date:"2020-11-10 02:25:10"


// q_no:121
// loading_list_items_id[]:1
// product[]:3^$-SP
// request[]:10
// loading_list_items_id[]:2
// product[]:4^$-VP
// request[]:2
// loading_list_items_id[]:0
// product[]:3^$-VP
// request[]:10
// loading_list_items_id[]:0
// product[]:3^$-VP
// request[]:10



// Type a message


      let arr = this.state.loadingList

      arr.map((item,i)=>{
        if(i == this.state.selectedIndex){
          item.extractedName = val
          item.varientId = index
        }
      })

      this.setState({showPicker:false,loadingList:arr})
      // this.setState({showPicker:false,selectedTempData:data})
      }

      closeAction(index){

        let arr = []

        this.state.loadingList.map((item,i)=>{
          if(i != index){
            arr.push(item)
          }
        })
  
        this.setState({showPicker:false,loadingList:arr})
  
      }
}

const styles = StyleSheet.create({});