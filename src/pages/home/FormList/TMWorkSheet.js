import React, { Component } from 'react'
import { Text, StyleSheet, View ,Alert,ScrollView,TextInput,TouchableOpacity,Modal} from 'react-native'
import HeaderWithBack from '../../../component/HeaderWithBack'
import Orientation from 'react-native-orientation';
import Icon from '../../../component/Icon'
import Ionicons from 'react-native-vector-icons/Ionicons'
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';


import TouchableButton from '../../../component/TouchableButton';
import Loader from '../../../WebAPI/Loader'
import {update_tws,requestPostApiMedia} from '../../../WebAPI/Service'
import Signature from '../../../component/Signature';
import KeyStore from '../../KeyStore/LocalKeyStore'

export default class TMWorkSheet extends Component {

constructor(props){
  super()
  this.state={
    loading: false,
    date_tws: "",
    delivery_tws: "",
    on_site: "",
    collection: "",
    maintenance_tws: "",
    frequency_tws: "",
    road1: "",
    permit1: "",
    road2: "",
    permit2: "",
    authority: "",
    area: "",
    srw1: "",
    time1: "",
    op_name1: "",
    timeoff1: "",
    opname1: "",
    log1: "",
    srw2: "",
    time2: "",
    op_name2: "",
    timeoff2: "",
    opname2: "",
    log2: "",
    pname1: "",
    sign1: "",
    date1: "",
    pname2: "",
    sign2: "",
    date2: "",

    isDateTimePickerVisible: false,
    mode: '',
    open: false,
    setDate: new Date(),
    confirmDate1:'',
    confirmDate2:'',
    confirmDate3:'',
    modalVisible:false,
    optionDate:'',
    
    time_defect_1:'',    details_1:'', print_1:'',time_wr_1:'', defect_1:'', 
    time_defect_2:'',    details_2:'', print_2:'',time_wr_2:'',defect_2:'',  
    time_defect_3:'',    details_3:'', print_3:'',time_wr_3:'',defect_3:'',  
    time_defect_4:'',    details_4:'', print_4:'',time_wr_4:'',defect_4:'',  
    time_defect_5:'',    details_5:'', print_5:'',time_wr_5:'', defect_5:'', 
    time_defect_6:'',    details_6:'', print_6:'',time_wr_6:'', defect_6:'', 
    time_defect_7:'',    details_7:'', print_7:'',time_wr_7:'',defect_7:'',  
    time_defect_8:'',    details_8:'', print_8:'',time_wr_8:'',defect_8:'',  
    time_defect_9:'',    details_9:'', print_9:'',time_wr_9:'',defect_9:'',  
    time_defect_10:'',    details_10:'', print_10:'',time_wr_10:'',defect_10:'',  
    time_defect_11:'',    details_11:'', print_11:'',time_wr_11:'',defect_11:'',  
    time_defect_12:'',    details_12:'', print_12:'',time_wr_12:'',defect_12:'',  
    time_defect_13:'',    details_13:'', print_13:'',time_wr_13:'', defect_13:'', 
    time_defect_14:'',    details_14:'', print_14:'',time_wr_14:'', defect_14:'', 
    time_defect_15:'',    details_15:'', print_15:'',time_wr_15:'',defect_15:'',  
    time_defect_16:'',    details_16:'', print_16:'',time_wr_16:'', defect_16:'', 
    time_defect_17:'',    details_17:'', print_17:'',time_wr_17:'',  defect_17:'',
    time_defect_18:'',    details_18:'', print_18:'',time_wr_18:'',  defect_18:'',
    time_defect_19:'',    details_19:'', print_19:'',time_wr_19:'', defect_19:'', 
    time_defect_20:'',    details_20:'', print_20:'',time_wr_20:'',  defect_20:'',
    time_defect_21:'',    details_21:'', print_21:'',time_wr_21:'',defect_21:'',  
    time_defect_22:'',    details_22:'', print_22:'',time_wr_22:'',defect_22:'',  

  }
}

async onSubmit(){
  const {date_tws,
    delivery_tws,
    on_site,
    collection,
    maintenance_tws,
    frequency_tws,
    road1,
    permit1,
    road2,
    permit2,
    authority,
    area,
    srw1,
    time1,
    op_name1,
    timeoff1,
    opname1,
    log1,
    srw2,
    time2,
    op_name2,
    timeoff2,
    opname2,
    log2,
    pname1,
    sign1,
    date1,
    pname2,
    sign2,
    date2,
    time_defect_1,    details_1, print_1,time_wr_1,defect_1,
        time_defect_2,    details_2, print_2,time_wr_2,defect_2,  
        time_defect_3,    details_3, print_3,time_wr_3,defect_3,  
        time_defect_4,    details_4, print_4,time_wr_4,defect_4,  
        time_defect_5,    details_5, print_5,time_wr_5, defect_5, 
        time_defect_6,    details_6, print_6,time_wr_6,defect_6,  
        time_defect_7,    details_7, print_7,time_wr_7,defect_7,  
        time_defect_8,    details_8, print_8,time_wr_8, defect_8, 
        time_defect_9,    details_9, print_9,time_wr_9, defect_9, 
        time_defect_10,    details_10, print_10,time_wr_10, defect_10, 
        time_defect_11,    details_11, print_11,time_wr_11, defect_11, 
        time_defect_12,    details_12, print_12,time_wr_12,  defect_12,
        time_defect_13,    details_13, print_13,time_wr_13,  defect_13,
        time_defect_14,    details_14, print_14,time_wr_14,  defect_14,
        time_defect_15,    details_15, print_15,time_wr_15,  defect_15,
        time_defect_16,    details_16, print_16,time_wr_16,  defect_16,
        time_defect_17,    details_17, print_17,time_wr_17,  defect_17,
        time_defect_18,    details_18, print_18,time_wr_18,  defect_18,
        time_defect_19,    details_19, print_19,time_wr_19,  defect_19,
        time_defect_20,    details_20, print_20,time_wr_20,  defect_20,
        time_defect_21,    details_21, print_21,time_wr_21, defect_21, 
        time_defect_22,    details_22, print_22,time_wr_22,defect_22,
  } = this.state
  this.setState({loading:true})
  const formData = new FormData()
  formData.append('q_no',this.props.navigation.state.params.q_no)
  formData.append('date_tws',this.state.confirmDate1)
  formData.append('delivery_tws',delivery_tws)
  formData.append('on_site',on_site)
  formData.append('collection',collection)
  formData.append('maintenance_tws',maintenance_tws)
  formData.append('frequency_tws',frequency_tws)
  formData.append('road1',road1)
  formData.append('permit1',permit1)
  formData.append('road2',road2)
  formData.append('permit2',permit2)
  formData.append('authority',authority)
  formData.append('area',area)
  formData.append('srw1',srw1)
  formData.append('srw2',srw2)
  formData.append('time1',time1)
  formData.append('time2',time2)
  formData.append('op_name1',op_name1)
  formData.append('op_name2',op_name2)
  formData.append('timeoff1',timeoff1)
  formData.append('timeoff2',timeoff2)
  formData.append('opname2',opname2)
  formData.append('opname1',opname1)
  formData.append('log2',log2)
  formData.append('log1',log1)
  formData.append('pname1',pname1)
  formData.append('pname2',pname2)
  formData.append('date1',this.state.confirmDate2)
  formData.append('date2',this.state.confirmDate3)
  formData.append('sign1',sign1)
  formData.append('sign2',sign2)

  formData.append('time_defect_1',time_defect_1)
  formData.append('details_1',details_1)
  formData.append('print_1',print_1)
  formData.append('time_wr_1',time_wr_1)
  formData.append('defect_1',defect_1)

  formData.append('time_defect_2',time_defect_2)
  formData.append('details_2',details_2)
  formData.append('print_2',print_2)
  formData.append('time_wr_2',time_wr_2)
  formData.append('defect_2',defect_2)

  formData.append('time_defect_3',time_defect_3)
  formData.append('details_3',details_3)
  formData.append('print_3',print_3)
  formData.append('time_wr_3',time_wr_3)
  formData.append('defect_3',defect_3)

  formData.append('time_defect_4',time_defect_4)
  formData.append('details_4',details_4)
  formData.append('print_4',print_4)
  formData.append('time_wr_4',time_wr_4)
  formData.append('defect_4',defect_4)

  formData.append('time_defect_5',time_defect_5)
  formData.append('details_5',details_5)
  formData.append('print_5',print_5)
  formData.append('time_wr_5',time_wr_5)
  formData.append('defect_5',defect_5)

  formData.append('time_defect_6',time_defect_6)
  formData.append('details_6',details_6)
  formData.append('print_6',print_6)
  formData.append('time_wr_6',time_wr_6)
  formData.append('defect_6',defect_6)

  formData.append('time_defect_7',time_defect_7)
  formData.append('details_7',details_7)
  formData.append('print_7',print_7)
  formData.append('time_wr_7',time_wr_7)
  formData.append('defect_7',defect_7)

  formData.append('time_defect_8',time_defect_8)
  formData.append('details_8',details_8)
  formData.append('print_8',print_8)
  formData.append('time_wr_8',time_wr_8)
  formData.append('defect_8',defect_8)

  formData.append('time_defect_9',time_defect_9)
  formData.append('details_9',details_9)
  formData.append('print_9',print_9)
  formData.append('time_wr_9',time_wr_9)
  formData.append('defect_9',defect_9)

  formData.append('time_defect_10',time_defect_10)
  formData.append('details_10',details_10)
  formData.append('print_10',print_10)
  formData.append('time_wr_10',time_wr_10)
  formData.append('defect_10',defect_10)

  formData.append('time_defect_11',time_defect_11)
  formData.append('details_11',details_11)
  formData.append('print_11',print_11)
  formData.append('time_wr_11',time_wr_11)
  formData.append('defect_11',defect_11)

  formData.append('time_defect_12',time_defect_12)
  formData.append('details_12',details_12)
  formData.append('print_12',print_12)
  formData.append('time_wr_12',time_wr_12)
  formData.append('defect_12',defect_12)

  formData.append('time_defect_13',time_defect_13)
  formData.append('details_13',details_13)
  formData.append('print_13',print_13)
  formData.append('time_wr_13',time_wr_13)
  formData.append('defect_13',defect_13)

  formData.append('time_defect_14',time_defect_14)
  formData.append('details_14',details_14)
  formData.append('print_14',print_14)
  formData.append('time_wr_14',time_wr_14)
  formData.append('defect_14',defect_14)

  formData.append('time_defect_15',time_defect_15)
  formData.append('details_15',details_15)
  formData.append('print_15',print_15)
  formData.append('time_wr_15',time_wr_15)
  formData.append('defect_15',defect_15)

  formData.append('time_defect_16',time_defect_16)
  formData.append('details_16',details_16)
  formData.append('print_16',print_16)
  formData.append('time_wr_16',time_wr_16)
  formData.append('defect_16',defect_16)

  formData.append('time_defect_17',time_defect_17)
  formData.append('details_17',details_17)
  formData.append('print_17',print_17)
  formData.append('time_wr_17',time_wr_17)
  formData.append('defect_17',defect_17)

  formData.append('time_defect_18',time_defect_18)
  formData.append('details_18',details_18)
  formData.append('print_18',print_18)
  formData.append('time_wr_18',time_wr_18)
  formData.append('defect_18',defect_18)

  formData.append('time_defect_19',time_defect_19)
  formData.append('details_19',details_19)
  formData.append('print_19',print_19)
  formData.append('time_wr_19',time_wr_19)
  formData.append('defect_19',defect_19)

  formData.append('time_defect_20',time_defect_20)
  formData.append('details_20',details_20)
  formData.append('print_20',print_20)
  formData.append('time_wr_20',time_wr_20)
  formData.append('defect_20',defect_20)

  formData.append('time_defect_21',time_defect_21)
  formData.append('details_21',details_21)
  formData.append('print_21',print_21)
  formData.append('time_wr_21',time_wr_21)
  formData.append('defect_21',defect_21)

  formData.append('time_defect_22',time_defect_22)
  formData.append('details_22',details_22)
  formData.append('print_22',print_22)
  formData.append('time_wr_22',time_wr_22)
  formData.append('defect_22',defect_22)

const worksheet_data={
  time_defect_1,    details_1, print_1,time_wr_1,defect_1,
        time_defect_2,    details_2, print_2,time_wr_2,defect_2,  
        time_defect_3,    details_3, print_3,time_wr_3,defect_3,  
        time_defect_4,    details_4, print_4,time_wr_4,defect_4,  
        time_defect_5,    details_5, print_5,time_wr_5, defect_5, 
        time_defect_6,    details_6, print_6,time_wr_6,defect_6,  
        time_defect_7,    details_7, print_7,time_wr_7,defect_7,  
        time_defect_8,    details_8, print_8,time_wr_8, defect_8, 
        time_defect_9,    details_9, print_9,time_wr_9, defect_9, 
        time_defect_10,    details_10, print_10,time_wr_10, defect_10, 
        time_defect_11,    details_11, print_11,time_wr_11, defect_11, 
        time_defect_12,    details_12, print_12,time_wr_12,  defect_12,
        time_defect_13,    details_13, print_13,time_wr_13,  defect_13,
        time_defect_14,    details_14, print_14,time_wr_14,  defect_14,
        time_defect_15,    details_15, print_15,time_wr_15,  defect_15,
        time_defect_16,    details_16, print_16,time_wr_16,  defect_16,
        time_defect_17,    details_17, print_17,time_wr_17,  defect_17,
        time_defect_18,    details_18, print_18,time_wr_18,  defect_18,
        time_defect_19,    details_19, print_19,time_wr_19,  defect_19,
        time_defect_20,    details_20, print_20,time_wr_20,  defect_20,
        time_defect_21,    details_21, print_21,time_wr_21, defect_21, 
        time_defect_22,    details_22, print_22,time_wr_22,defect_22,
}


  const {responseJson,err} = await requestPostApiMedia(update_tws,formData,'POST')
if(responseJson.status){
  this.setState({loading:false})

  // creating name for local storage for different q_no
  const name = "tmws"+this.props.navigation.state.params.q_no
  KeyStore.setKey(name, JSON.stringify(worksheet_data));
  Alert.alert(responseJson.message,'',[
        
    {
      text: "Ok",
      onPress: () => this.props.navigation.navigate('FormsList')
      
    },
  ])
}else{
  this.setState({loading:false})
  Alert.alert(err,responseJson.message)
}


}





showDateTimePicker = () => {
  this.setState({isDateTimePickerVisible: true});
};

hideDateTimePicker = () => {
  this.setState({isDateTimePickerVisible: false});
};

handleDatePicked = (datetime) => {
  this.setState({isDateTimePickerVisible: false});

  if (this.state.status == 1) {
    this.setState({
      confirmDate1: moment(datetime).format('MM/DD/YYYY'),
    });
  } else if (this.state.status == 2) {
    this.setState({
      confirmDate2: moment(datetime).format('MM/DD/YYYY'),
    });
  } else if (this.state.status == 3) {
    this.setState({
      confirmDate3: moment(datetime).format('MM/DD/YYYY'),
    });
  } else if(this.state.status==4) {
    this.setState({
      time_defect_1: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==5) {
    this.setState({
      time_defect_2: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==6) {
    this.setState({
      time_defect_3: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==7) {
    this.setState({
      time_defect_4: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==8) {
    this.setState({
      time_defect_5: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==9) {
    this.setState({
      time_defect_6: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==10) {
    this.setState({
      time_defect_7: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==11) {
    this.setState({
      time_defect_8: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==12) {
    this.setState({
      time_defect_9: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==13) {
    this.setState({
      time_defect_10: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==14) {
    this.setState({
      time_defect_11: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==15) {
    this.setState({
      time_defect_12: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==16) {
    this.setState({
      time_defect_13: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==17) {
    this.setState({
      time_defect_14: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==18) {
    this.setState({
      time_defect_15: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==19) {
    this.setState({
      time_defect_16: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==20) {
    this.setState({
      time_defect_17: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==21) {
    this.setState({
      time_defect_18: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==22) {
    this.setState({
      time_defect_19: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==23) {
    this.setState({
      time_defect_20: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==24) {
    this.setState({
      time_defect_21: moment(datetime).format('HH:mm'),
    });
  }else if(this.state.status==25) {
    this.setState({
      time_defect_22: moment(datetime).format('HH:mm'),
    });
  }
};



















  componentWillMount() {
    // The getOrientation method is async. It happens sometimes that
    // you need the orientation at the moment the JS runtime starts running on device.
    // `getInitialOrientation` returns directly because its a constant set at the
    // beginning of the JS runtime.

    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      console.warn('portartait');
      // do something
    } else {
      // do something else
    }
  }

  async componentDidMount() {
  
    this.setState({loading:true})
    const formData = new FormData();
 
    formData.append('q_no', this.props.navigation.state.params.q_no);
   
    const {responseJson,err} =await requestPostApiMedia(update_tws,formData,'POST')

    if(responseJson.status){
      const {date_tws,
        delivery_tws,
        on_site,
        collection,
        maintenance_tws,
         frequency_tws,
        road1,
        permit1,
        road2,
        permit2,
        authority,
        area,
        srw1,
        time1,
        op_name1,
        timeoff1,
        opname1,
        log1,
        srw2,
        time2,
        op_name2,
        timeoff2,
        opname2,
        log2,
        pname1,
        sign1,
        date1,
        pname2,
        sign2,
        date2} = responseJson.data
        this.setState({loading:false,date_tws,
          delivery_tws,
          on_site,
          collection,
          maintenance_tws,
          frequency_tws,
          road1,
          permit1,
          road2,
          permit2,
          authority,
          area,
          srw1,
          time1,
          op_name1,
          timeoff1,
          opname1,
          log1,
          srw2,
          time2,
          op_name2,
          timeoff2,
          opname2,
          log2,
          pname1,
          sign1,
          date1,
          pname2,
          sign2,
          date2})
    }
    const name = "tmws"+this.props.navigation.state.params.q_no

    KeyStore.getKey(name, (err, value) => {


      if (value) {

       const data = JSON.parse(value)
        
        const {time_defect_1,    details_1, print_1,time_wr_1,defect_1,
          time_defect_2,    details_2, print_2,time_wr_2,defect_2,  
          time_defect_3,    details_3, print_3,time_wr_3,defect_3,  
          time_defect_4,    details_4, print_4,time_wr_4,defect_4,  
          time_defect_5,    details_5, print_5,time_wr_5, defect_5, 
          time_defect_6,    details_6, print_6,time_wr_6,defect_6,  
          time_defect_7,    details_7, print_7,time_wr_7,defect_7,  
          time_defect_8,    details_8, print_8,time_wr_8, defect_8, 
          time_defect_9,    details_9, print_9,time_wr_9, defect_9, 
          time_defect_10,    details_10, print_10,time_wr_10, defect_10, 
          time_defect_11,    details_11, print_11,time_wr_11, defect_11, 
          time_defect_12,    details_12, print_12,time_wr_12,  defect_12,
          time_defect_13,    details_13, print_13,time_wr_13,  defect_13,
          time_defect_14,    details_14, print_14,time_wr_14,  defect_14,
          time_defect_15,    details_15, print_15,time_wr_15,  defect_15,
          time_defect_16,    details_16, print_16,time_wr_16,  defect_16,
          time_defect_17,    details_17, print_17,time_wr_17,  defect_17,
          time_defect_18,    details_18, print_18,time_wr_18,  defect_18,
          time_defect_19,    details_19, print_19,time_wr_19,  defect_19,
          time_defect_20,    details_20, print_20,time_wr_20,  defect_20,
          time_defect_21,    details_21, print_21,time_wr_21, defect_21, 
          time_defect_22,    details_22, print_22,time_wr_22,defect_22,} = data
      
      this.setState({time_defect_1,    details_1, print_1,time_wr_1,defect_1,
        time_defect_2,    details_2, print_2,time_wr_2,defect_2,  
        time_defect_3,    details_3, print_3,time_wr_3,defect_3,  
        time_defect_4,    details_4, print_4,time_wr_4,defect_4,  
        time_defect_5,    details_5, print_5,time_wr_5, defect_5, 
        time_defect_6,    details_6, print_6,time_wr_6,defect_6,  
        time_defect_7,    details_7, print_7,time_wr_7,defect_7,  
        time_defect_8,    details_8, print_8,time_wr_8, defect_8, 
        time_defect_9,    details_9, print_9,time_wr_9, defect_9, 
        time_defect_10,    details_10, print_10,time_wr_10, defect_10, 
        time_defect_11,    details_11, print_11,time_wr_11, defect_11, 
        time_defect_12,    details_12, print_12,time_wr_12,  defect_12,
        time_defect_13,    details_13, print_13,time_wr_13,  defect_13,
        time_defect_14,    details_14, print_14,time_wr_14,  defect_14,
        time_defect_15,    details_15, print_15,time_wr_15,  defect_15,
        time_defect_16,    details_16, print_16,time_wr_16,  defect_16,
        time_defect_17,    details_17, print_17,time_wr_17,  defect_17,
        time_defect_18,    details_18, print_18,time_wr_18,  defect_18,
        time_defect_19,    details_19, print_19,time_wr_19,  defect_19,
        time_defect_20,    details_20, print_20,time_wr_20,  defect_20,
        time_defect_21,    details_21, print_21,time_wr_21, defect_21, 
        time_defect_22,    details_22, print_22,time_wr_22,defect_22})
        
      } 
    });


    Orientation.lockToLandscape();

    Orientation.addOrientationListener(this._orientationDidChange);
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      console.warn('portartaitttttttttttttt');
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  };

  componentWillUnmount() {
    Orientation.lockToPortrait();
    Orientation.getOrientation((err, orientation) => {
      console.warn(`Current Device Orientation: ${orientation}`);
    });

    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
  }


    render() {
      const {date_tws,
        delivery_tws,
        on_site,
        collection,
        maintenance_tws,
        frequency_tws,
        road1,
        permit1,
        road2,
        permit2,
        authority,
        area,
        srw1,
        time1,
        op_name1,
        timeoff1,
        opname1,
        log1,
        srw2,
        time2,
        op_name2,
        timeoff2,
        opname2,
        log2,
        pname1,
        sign1,
        date1,
        pname2,
        sign2,
        date2,
        time_defect_1,    details_1, print_1,time_wr_1,defect_1,
        time_defect_2,    details_2, print_2,time_wr_2,defect_2,  
        time_defect_3,    details_3, print_3,time_wr_3,defect_3,  
        time_defect_4,    details_4, print_4,time_wr_4,defect_4,  
        time_defect_5,    details_5, print_5,time_wr_5, defect_5, 
        time_defect_6,    details_6, print_6,time_wr_6,defect_6,  
        time_defect_7,    details_7, print_7,time_wr_7,defect_7,  
        time_defect_8,    details_8, print_8,time_wr_8, defect_8, 
        time_defect_9,    details_9, print_9,time_wr_9, defect_9, 
        time_defect_10,    details_10, print_10,time_wr_10, defect_10, 
        time_defect_11,    details_11, print_11,time_wr_11, defect_11, 
        time_defect_12,    details_12, print_12,time_wr_12,  defect_12,
        time_defect_13,    details_13, print_13,time_wr_13,  defect_13,
        time_defect_14,    details_14, print_14,time_wr_14,  defect_14,
        time_defect_15,    details_15, print_15,time_wr_15,  defect_15,
        time_defect_16,    details_16, print_16,time_wr_16,  defect_16,
        time_defect_17,    details_17, print_17,time_wr_17,  defect_17,
        time_defect_18,    details_18, print_18,time_wr_18,  defect_18,
        time_defect_19,    details_19, print_19,time_wr_19,  defect_19,
        time_defect_20,    details_20, print_20,time_wr_20,  defect_20,
        time_defect_21,    details_21, print_21,time_wr_21, defect_21, 
        time_defect_22,    details_22, print_22,time_wr_22,defect_22,
      } = this.state
        return (
            <View style={{flex: 1, backgroundColor: '#FBFBFB'}}>
            <HeaderWithBack
              title="TM WORK SHEET"
              goBack={() =>
                Alert.alert('Amber RTM', 'Do you want to Exit?', [
                  {text: 'No', style: 'cancel'},
                  {text: 'Yes', onPress: () => this.props.navigation.goBack()},
                ])
              }
            />

<ScrollView style={{margin:8}}>


{/* First Row */}
<View style={{flexDirection:'row',borderWidth:2}}>
<View style={{flex:5,backgroundColor:'#F9BF8F',justifyContent:'center',borderRightWidth:2,paddingVertical:3,paddingHorizontal:3}}>
<Text style={{fontSize:15,}}>Date:</Text>
</View>
<View style={{flex:3.5,borderRightWidth:2,flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
            <Text style={{fontSize:14,color:'#B3B3B3'}}>{ this.state.confirmDate1 ?this.state.confirmDate1 : date_tws ? date_tws:"MM/DD/YYYY"}</Text>
            <TouchableOpacity  onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 1,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>
</View>
<View style={{flex:5,borderRightWidth:2,backgroundColor:'#F9BF8F',justifyContent:'center',paddingHorizontal:3}}>
<Text style={{fontSize:15,}}>Delivery:</Text>
</View>
<View style={{flex:3.5,justifyContent:'space-around',flexDirection:'row',alignItems:'center'}}>
<View style={{flexDirection:'row',}}>

<TouchableOpacity
             onPress={() => this.setState({delivery_tws: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:delivery_tws ==='1' ?'#FF1493':'#fff', borderWidth:delivery_tws ==='1'?0:1}]}>
                {delivery_tws === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({delivery_tws: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:delivery_tws ==='0' ?'#FF1493':'#fff', borderWidth:delivery_tws ==='0'?0:1}]}>
                {delivery_tws === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
</View>


{/* Second Row */}

<View style={{flexDirection:'row',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:5,backgroundColor:'#F9BF8F',justifyContent:'center',borderRightWidth:2,paddingVertical:3,paddingHorizontal:3}}>
<Text style={{fontSize:15,}}>On Site:</Text>
</View>
<View style={{flex:3.5,borderRightWidth:2,flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
<View style={{flexDirection:'row',}}>

<TouchableOpacity
             onPress={() => this.setState({on_site: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:on_site ==='1' ?'#FF1493':'#fff', borderWidth:on_site ==='1'?0:1}]}>
                {on_site === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({on_site: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:on_site ==='0' ?'#FF1493':'#fff', borderWidth:on_site ==='0'?0:1}]}>
                {on_site === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:5,borderRightWidth:2,backgroundColor:'#F9BF8F',justifyContent:'center',paddingHorizontal:3}}>
<Text style={{fontSize:15,}}>Collection:</Text>
</View>
<View style={{flex:3.5,justifyContent:'space-around',flexDirection:'row',alignItems:'center'}}>
<View style={{flexDirection:'row',}}>

<TouchableOpacity
             onPress={() => this.setState({collection: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:collection ==='1' ?'#FF1493':'#fff', borderWidth:collection ==='1'?0:1}]}>
                {collection === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({collection: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:collection ==='0' ?'#FF1493':'#fff', borderWidth:collection ==='0'?0:1}]}>
                {collection === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
</View>




{/* Third Row */}

<View style={{flexDirection:'row',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:5,backgroundColor:'#F9BF8F',justifyContent:'center',borderRightWidth:2,paddingVertical:5,paddingHorizontal:3}}>
<Text style={{fontSize:15,}}>Maintenance Required:</Text>
</View>
<View style={{flex:3.5,borderRightWidth:2,flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
<View style={{flexDirection:'row',}}>

<TouchableOpacity
             onPress={() => this.setState({maintenance_tws: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:maintenance_tws ==='1' ?'#FF1493':'#fff', borderWidth:maintenance_tws ==='1'?0:1}]}>
                {maintenance_tws === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row'}}>
<TouchableOpacity 
            onPress={() => this.setState({maintenance_tws: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:maintenance_tws ==='0' ?'#FF1493':'#fff', borderWidth:maintenance_tws ==='0'?0:1}]}>
                {maintenance_tws === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:5,borderRightWidth:2,backgroundColor:'#F9BF8F',justifyContent:'center',paddingHorizontal:3}}>
<Text style={{fontSize:15,}}>Maintenance Frequency:</Text>
</View>
<View style={{flex:3.5,justifyContent:"center"}}>
  <TextInput
  value={frequency_tws}
  onChangeText={frequency_tws=>this.setState({frequency_tws})}
  multiline={true}
  style={{padding:0,fontSize:15,fontWeight:'bold',paddingHorizontal:3,paddingVertical:10}}
  />

</View>
</View>



{/* Fourth Row */}

<View style={{flexDirection:"row",marginVertical:8,borderWidth:2,backgroundColor:'#F9BF8F',marginBottom:0}}>
<View style={{justifyContent:'center',alignItems:'center',borderRightWidth:2,paddingVertical:4,paddingHorizontal:3,flex:1}}>
  <Text style={{fontSize:15}}>Streetworks Permits No.</Text>
</View>

<View style={{justifyContent:'center',alignItems:'center',paddingVertical:3,paddingHorizontal:3,flex:1}}>
  <Text style={{fontSize:15}}>Road Name</Text>
</View>
</View>

{/* First Child */}

<View style={{flexDirection:"row",borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{justifyContent:'center',borderRightWidth:2,paddingVertical:0,paddingHorizontal:3,flex:1}}>
 <TextInput
 value={permit1}
 onChangeText={permit1=>this.setState({permit1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>

<View style={{justifyContent:'center',paddingVertical:3,paddingHorizontal:3,flex:1}}>
<TextInput
value={road1}
onChangeText={road1=>this.setState({road1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
</View>

{/* Second Child */}
<View style={{flexDirection:"row",borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{justifyContent:'center',borderRightWidth:2,paddingVertical:0,paddingHorizontal:3,flex:1}}>
 <TextInput
 value={permit2}
 onChangeText={permit2=>this.setState({permit2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>

<View style={{justifyContent:'center',paddingVertical:3,paddingHorizontal:3,flex:1}}>
<TextInput
value={road2}
onChangeText={road2=>this.setState({road2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
</View>

{/* Fifth Row */}
<View style={{flexDirection:'row',marginVertical:8,borderWidth:2}}>
 <View style={{flex:3.5,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',borderRightWidth:2,paddingVertical:3}}>
   <Text style={{fontSize:15}}>Highways Authority</Text>
 </View>

 <View style={{flex:5,backgroundColor:'#FAFAFA',justifyContent:'center',borderRightWidth:2}}>
 <TextInput
 value={authority}
 onChangeText={authority=>this.setState({authority})}
 multiline={true}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
 </View>

 <View style={{flex:3.5,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',borderRightWidth:2}}>
   <Text style={{fontSize:15,textAlign:'center'}}>Highways England Area</Text>
 </View>

 <View style={{flex:5,backgroundColor:'#FAFAFA',justifyContent:'center',}}>
 <TextInput
 value={area}
 onChangeText={area=>this.setState({area})}
 multiline={true}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
 </View>
</View>

{/* Sixth Row */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#F9BF8F'}}>
<View style={{flex:2,justifyContent:'center',alignItems:'center',borderRightWidth:2,paddingVertical:5}}>
  <Text style={{fontSize:15}}>SRW</Text>
</View>
<View style={{flex:3,justifyContent:'center',alignItems:'center',borderRightWidth:2}}>
  <Text style={{fontSize:15}}>NCC</Text>
</View>
<View style={{flex:2,justifyContent:'center',alignItems:'center'}}>
  <Text style={{fontSize:15}}>RCC</Text>
</View>
</View>

{/* first child */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{flex:2,justifyContent:'center',borderRightWidth:2,alignItems:'center',backgroundColor:'#F9BF8F'}}>
<Text style={{fontSize:15}}>SRW No.</Text>
</View>
<View style={{flex:3,justifyContent:'center',borderRightWidth:2}}>
<TextInput
 multiline={true}
 value={srw1}
 onChangeText={srw1=>this.setState({srw1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
<View style={{flex:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={srw2}
 onChangeText={srw2=>this.setState({srw2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:3}}
 />
</View>
</View>



{/* Second child */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{flex:2,justifyContent:'center',borderRightWidth:2,alignItems:'center',backgroundColor:'#F9BF8F'}}>
<Text style={{fontSize:15}}>Time Called On</Text>
</View>
<View style={{flex:3,justifyContent:'center',borderRightWidth:2}}>
<TextInput
 multiline={true}
 value={time1}
 onChangeText={time1=>this.setState({time1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
<View style={{flex:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time2}
 onChangeText={time2=>this.setState({time2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:3}}
 />
</View>
</View>

{/* Third child */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{flex:2,justifyContent:'center',borderRightWidth:2,alignItems:'center',backgroundColor:'#F9BF8F'}}>
<Text style={{fontSize:15}}>Operator Name</Text>
</View>
<View style={{flex:3,justifyContent:'center',borderRightWidth:2}}>
<TextInput
value={op_name1}
onChangeText={op_name1=>this.setState({op_name1})}
 multiline={true}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
<View style={{flex:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={op_name2}
 onChangeText={op_name2=>this.setState({op_name2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:3}}
 />
</View>
</View>

{/* Fourth child */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{flex:2,justifyContent:'center',borderRightWidth:2,alignItems:'center',backgroundColor:'#F9BF8F'}}>
<Text style={{fontSize:15}}>Time Called Off</Text>
</View>
<View style={{flex:3,justifyContent:'center',borderRightWidth:2}}>
<TextInput
value={timeoff1}
onChangeText={timeoff1=>this.setState({timeoff1})}
 multiline={true}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
<View style={{flex:2,justifyContent:'center'}}>
<TextInput
value={timeoff2}
onChangeText={timeoff2=>this.setState({timeoff2})}
 multiline={true}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:3}}
 />
</View>
</View>

{/* Fifth child */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{flex:2,justifyContent:'center',borderRightWidth:2,alignItems:'center',backgroundColor:'#F9BF8F'}}>
<Text style={{fontSize:15}}>Operator Name</Text>
</View>
<View style={{flex:3,justifyContent:'center',borderRightWidth:2}}>
<TextInput
 multiline={true}
 value={opname1}
 onChangeText={opname1=>this.setState({opname1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
<View style={{flex:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={opname2}
 onChangeText={opname2=>this.setState({opname2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:3}}
 />
</View>
</View>

{/* Six child */}

<View style={{flexDirection:'row',borderWidth:2,backgroundColor:'#FAFAFA',borderTopWidth:0}}>
<View style={{flex:2,justifyContent:'center',borderRightWidth:2,alignItems:'center',backgroundColor:'#F9BF8F'}}>
<Text style={{fontSize:15}}>Log Numbers</Text>
</View>
<View style={{flex:3,justifyContent:'center',borderRightWidth:2}}>
<TextInput
 multiline={true}
 value={log1}
 onChangeText={log1=>this.setState({log1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0}}
 />
</View>
<View style={{flex:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={log2}
 onChangeText={log2=>this.setState({log2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:3}}
 />
</View>
</View>

{/* Seventh Row */}

<View style={{marginVertical:8,borderWidth:2,backgroundColor:'#F9BF8F',flexDirection:'row',marginBottom:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'center',paddingVertical:3}}>
  <Text style={{fontSize:15,textAlign:'center'}}>
    Time
  </Text>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'center'}}>
  <Text style={{fontSize:15,textAlign:'center'}}>
    Defects Found?
  </Text>
</View>
<View style={{flex:3,borderRightWidth:2,alignItems:'center',justifyContent:'center'}}>
  <Text style={{fontSize:15,textAlign:'center'}}>
    Time Defect Rectified
  </Text>
</View>
<View style={{flex:5,borderRightWidth:2,alignItems:'center',justifyContent:'center'}}>
  <Text style={{fontSize:15,textAlign:'center'}}>
    Details of Works or Details of Defects/ Maintenance/ Incidents
  </Text>
</View>
<View style={{flex:2.5,borderRightWidth:2,alignItems:'center',justifyContent:'center'}}>
  <Text style={{fontSize:15,textAlign:'center'}}>
    Print Name
  </Text>
</View>
</View>


{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
   { time_defect_1?time_defect_1:" HH:MM"}
  </Text>

  <TouchableOpacity  onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 4,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_1: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_1 ==='1' ?'#FF1493':'#fff', borderWidth:defect_1 ==='1'?0:1}]}>
                {defect_1 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_1: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_1 ==='0' ?'#FF1493':'#fff', borderWidth:defect_1 ==='0'?0:1}]}>
                {defect_1 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_1}
 onChangeText={time_wr_1=>this.setState({time_wr_1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_1}
 onChangeText={details_1=>this.setState({details_1})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
value={print_1}
onChangeText={print_1=>this.setState({print_1})}
 multiline={true}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>


{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_2?time_defect_2 :"HH:MM"}
  </Text>

  <TouchableOpacity  onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 5,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_2: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_2 ==='1' ?'#FF1493':'#fff', borderWidth:defect_2 ==='1'?0:1}]}>
                {defect_2 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_2: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_2 ==='0' ?'#FF1493':'#fff', borderWidth:defect_2 ==='0'?0:1}]}>
                {defect_2 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_2}
 onChangeText={time_wr_2=>this.setState({time_wr_2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_2}
 onChangeText={details_2=>this.setState({details_2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_2}
 onChangeText={print_2=>this.setState({print_2})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>


{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_3?time_defect_3 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 6,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_3: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_3 ==='1' ?'#FF1493':'#fff', borderWidth:defect_3 ==='1'?0:1}]}>
                {defect_3 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_3: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_3 ==='0' ?'#FF1493':'#fff', borderWidth:defect_3 ==='0'?0:1}]}>
                {defect_3 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_3}
 onChangeText={time_wr_3=>this.setState({time_wr_3})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_3}
 onChangeText={details_3=>this.setState({details_3})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_3}
 onChangeText={print_3=>this.setState({print_3})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_4?time_defect_4 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 7,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_4: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_4 ==='1' ?'#FF1493':'#fff', borderWidth:defect_4 ==='1'?0:1}]}>
                {defect_4 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_4: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_4 ==='0' ?'#FF1493':'#fff', borderWidth:defect_4 ==='0'?0:1}]}>
                {defect_4 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_4}
 onChangeText={time_wr_4=>this.setState({time_wr_4})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_4}
 onChangeText={details_4=>this.setState({details_4})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_4}
 onChangeText={print_4=>this.setState({print_4})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_5?time_defect_5 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 8,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_5: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_5 ==='1' ?'#FF1493':'#fff', borderWidth:defect_5 ==='1'?0:1}]}>
                {defect_5 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_5: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_5 ==='0' ?'#FF1493':'#fff', borderWidth:defect_5 ==='0'?0:1}]}>
                {defect_5 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_5}
 onChangeText={time_wr_5=>this.setState({time_wr_5})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_5}
 onChangeText={details_5=>this.setState({details_5})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_5}
 onChangeText={print_5=>this.setState({print_5})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_6?time_defect_6 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 9,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_6: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_6 ==='1' ?'#FF1493':'#fff', borderWidth:defect_6 ==='1'?0:1}]}>
                {defect_6 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_6: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_6 ==='0' ?'#FF1493':'#fff', borderWidth:defect_6 ==='0'?0:1}]}>
                {defect_6 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_6}
 onChangeText={time_wr_6=>this.setState({time_wr_6})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_6}
 onChangeText={details_6=>this.setState({details_6})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_6}
 onChangeText={print_6=>this.setState({print_6})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_7?time_defect_7 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 10,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_7: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_7 ==='1' ?'#FF1493':'#fff', borderWidth:defect_7 ==='1'?0:1}]}>
                {defect_7 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_7: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_7 ==='0' ?'#FF1493':'#fff', borderWidth:defect_7 ==='0'?0:1}]}>
                {defect_7 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_7}
 onChangeText={time_wr_7=>this.setState({time_wr_7})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_7}
 onChangeText={details_7=>this.setState({details_7})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_7}
 onChangeText={print_7=>this.setState({print_7})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_8?time_defect_8 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 11,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_8: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_8 ==='1' ?'#FF1493':'#fff', borderWidth:defect_8 ==='1'?0:1}]}>
                {defect_8 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_8: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_8 ==='0' ?'#FF1493':'#fff', borderWidth:defect_8 ==='0'?0:1}]}>
                {defect_8 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_8}
 onChangeText={time_wr_8=>this.setState({time_wr_8})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_8}
 onChangeText={details_8=>this.setState({details_8})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_8}
 onChangeText={print_8=>this.setState({print_8})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_9?time_defect_9 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 12,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_9: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_9 ==='1' ?'#FF1493':'#fff', borderWidth:defect_9 ==='1'?0:1}]}>
                {defect_9 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_9: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_9 ==='0' ?'#FF1493':'#fff', borderWidth:defect_9 ==='0'?0:1}]}>
                {defect_9 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_9}
 onChangeText={time_wr_9=>this.setState({time_wr_9})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_9}
 onChangeText={details_9=>this.setState({details_9})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_9}
 onChangeText={print_9=>this.setState({print_9})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_10?time_defect_10 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 13,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_10: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_10 ==='1' ?'#FF1493':'#fff', borderWidth:defect_10 ==='1'?0:1}]}>
                {defect_10 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_10: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_10 ==='0' ?'#FF1493':'#fff', borderWidth:defect_10 ==='0'?0:1}]}>
                {defect_10 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_10}
 onChangeText={time_wr_10=>this.setState({time_wr_10})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_10}
 onChangeText={details_10=>this.setState({details_10})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_10}
 onChangeText={print_10=>this.setState({print_10})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_11?time_defect_11 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status:14,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_11: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_11 ==='1' ?'#FF1493':'#fff', borderWidth:defect_11 ==='1'?0:1}]}>
                {defect_11 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_11: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_11 ==='0' ?'#FF1493':'#fff', borderWidth:defect_11 ==='0'?0:1}]}>
                {defect_11 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_11}
 onChangeText={time_wr_11=>this.setState({time_wr_11})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_11}
 onChangeText={details_11=>this.setState({details_11})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_11}
 onChangeText={print_11=>this.setState({print_11})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_12?time_defect_12 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 15,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_12: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_12 ==='1' ?'#FF1493':'#fff', borderWidth:defect_12 ==='1'?0:1}]}>
                {defect_12 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_12: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_12 ==='0' ?'#FF1493':'#fff', borderWidth:defect_12 ==='0'?0:1}]}>
                {defect_12 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_12}
 onChangeText={time_wr_12=>this.setState({time_wr_12})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_12}
 onChangeText={details_12=>this.setState({details_12})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_12}
 onChangeText={print_12=>this.setState({print_12})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_13?time_defect_13 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 16,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_13: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_13 ==='1' ?'#FF1493':'#fff', borderWidth:defect_13 ==='1'?0:1}]}>
                {defect_13 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_13: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_13 ==='0' ?'#FF1493':'#fff', borderWidth:defect_13 ==='0'?0:1}]}>
                {defect_13 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_13}
 onChangeText={time_wr_13=>this.setState({time_wr_13})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_13}
 onChangeText={details_13=>this.setState({details_13})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_13}
 onChangeText={print_13=>this.setState({print_13})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_14?time_defect_14 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 17,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_14: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_14 ==='1' ?'#FF1493':'#fff', borderWidth:defect_14 ==='1'?0:1}]}>
                {defect_14 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_14: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_14 ==='0' ?'#FF1493':'#fff', borderWidth:defect_14 ==='0'?0:1}]}>
                {defect_14 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_14}
 onChangeText={time_wr_14=>this.setState({time_wr_14})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_14}
 onChangeText={details_14=>this.setState({details_14})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_14}
 onChangeText={print_14=>this.setState({print_14})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_15?time_defect_15 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 18,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_15: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_15 ==='1' ?'#FF1493':'#fff', borderWidth:defect_15 ==='1'?0:1}]}>
                {defect_15 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_15: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_15 ==='0' ?'#FF1493':'#fff', borderWidth:defect_15 ==='0'?0:1}]}>
                {defect_15 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_15}
 onChangeText={time_wr_15=>this.setState({time_wr_15})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_15}
 onChangeText={details_15=>this.setState({details_15})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_15}
 onChangeText={print_15=>this.setState({print_15})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_16?time_defect_16 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 19,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_16: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_16 ==='1' ?'#FF1493':'#fff', borderWidth:defect_16 ==='1'?0:1}]}>
                {defect_16 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_16: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_16 ==='0' ?'#FF1493':'#fff', borderWidth:defect_16 ==='0'?0:1}]}>
                {defect_16 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_16}
 onChangeText={time_wr_16=>this.setState({time_wr_16})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_16}
 onChangeText={details_16=>this.setState({details_16})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_16}
 onChangeText={print_16=>this.setState({print_16})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_17?time_defect_17 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 20,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_17: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_17 ==='1' ?'#FF1493':'#fff', borderWidth:defect_17 ==='1'?0:1}]}>
                {defect_17 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_17: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_17 ==='0' ?'#FF1493':'#fff', borderWidth:defect_17 ==='0'?0:1}]}>
                {defect_17 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_17}
 onChangeText={time_wr_17=>this.setState({time_wr_17})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_17}
 onChangeText={details_17=>this.setState({details_17})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_17}
 onChangeText={print_17=>this.setState({print_17})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_18?time_defect_18 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 21,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_18: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_18 ==='1' ?'#FF1493':'#fff', borderWidth:defect_18 ==='1'?0:1}]}>
                {defect_18 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_18: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_18 ==='0' ?'#FF1493':'#fff', borderWidth:defect_18 ==='0'?0:1}]}>
                {defect_18 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_18}
 onChangeText={time_wr_18=>this.setState({time_wr_18})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_18}
 onChangeText={details_18=>this.setState({details_18})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_18}
 onChangeText={print_18=>this.setState({print_18})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_19?time_defect_19 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 22,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_19: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_19 ==='1' ?'#FF1493':'#fff', borderWidth:defect_19 ==='1'?0:1}]}>
                {defect_19 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_19: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_19 ==='0' ?'#FF1493':'#fff', borderWidth:defect_19 ==='0'?0:1}]}>
                {defect_19 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_19}
 onChangeText={time_wr_19=>this.setState({time_wr_19})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_19}
 onChangeText={details_19=>this.setState({details_19})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_19}
 onChangeText={print_19=>this.setState({print_19})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_20?time_defect_20 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 23,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity >
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_20: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_20 ==='1' ?'#FF1493':'#fff', borderWidth:defect_20 ==='1'?0:1}]}>
                {defect_20 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_20: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_20 ==='0' ?'#FF1493':'#fff', borderWidth:defect_20 ==='0'?0:1}]}>
                {defect_20 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_20}
 onChangeText={time_wr_20=>this.setState({time_wr_20})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_20}
 onChangeText={details_20=>this.setState({details_20})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_20}
 onChangeText={print_20=>this.setState({print_20})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
  {time_defect_21?time_defect_21 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 24,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_21: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_21 ==='1' ?'#FF1493':'#fff', borderWidth:defect_21 ==='1'?0:1}]}>
                {defect_21 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_21: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_21 ==='0' ?'#FF1493':'#fff', borderWidth:defect_21 ==='0'?0:1}]}>
                {defect_21 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_21}
 onChangeText={time_wr_21=>this.setState({time_wr_21})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_21}
 onChangeText={details_21=>this.setState({details_21})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_21}
 onChangeText={print_21=>this.setState({print_21})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* First Child */}

<View style={{marginVertical:0,borderWidth:2,backgroundColor:'#FAFAFA',flexDirection:'row',borderTopWidth:0}}>
<View style={{flex:2,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:3,flexDirection:'row'}}>
  <Text style={{fontSize:15,textAlign:'center',color:'#7D7D7D'}}>
    {time_defect_22?time_defect_22 :"HH:MM"}
  </Text>

  <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'time',
                        status: 25,
                      });
                    }}>
  <Ionicons
  name="time-outline"
  size={20}
  />
  </TouchableOpacity>
</View>
<View style={{flex:1.5,borderRightWidth:2,alignItems:'center',justifyContent:'space-around',paddingVertical:10}}>
<View style={{flexDirection:'row',marginBottom:5}}>

<TouchableOpacity
             onPress={() => this.setState({defect_22: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:defect_22 ==='1' ?'#FF1493':'#fff', borderWidth:defect_22 ==='1'?0:1}]}>
                {defect_22 === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>

            <Text >Yes</Text>
</View>
<View style={{flexDirection:'row',marginBottom:3}}>
<TouchableOpacity 
            onPress={() => this.setState({defect_22: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:defect_22 ==='0' ?'#FF1493':'#fff', borderWidth:defect_22 ==='0'?0:1}]}>
                {defect_22 === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text>No</Text>
</View>
</View>
<View style={{flex:3,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={time_wr_22}
 onChangeText={time_wr_22=>this.setState({time_wr_22})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={details_22}
 onChangeText={details_22=>this.setState({details_22})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
<View style={{flex:2.5,borderRightWidth:2,justifyContent:'center'}}>
<TextInput
 multiline={true}
 value={print_22}
 onChangeText={print_22=>this.setState({print_22})}
 style={{fontSize:15,fontWeight:'bold',textAlign:'center',padding:0,paddingVertical:1}}
 />
</View>
</View>

{/* last Row */}

<View style={{marginVertical:8,marginBottom:0,borderWidth:2,justifyContent:'center',paddingVertical:5,backgroundColor:'#F9BF8F',paddingHorizontal:3}}>
<Text style={{fontSize:15}}>TEAM LEADER: the above works / maintenance have been carried out as recorded above</Text>
</View>

{/* First Child */}

<View style={{flexDirection:'row',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:2}}>
  <Text style={{fontSize:15}}>Print Name:</Text>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'center',paddingVertical:3,borderRightWidth:2}}>
  <TextInput
  value={pname1}
  onChangeText={pname1=>this.setState({pname1})}
  style={{fontSize:15,fontWeight:'bold',padding:0,paddingHorizontal:3}}
  />
</View>
<View style={{flex:1,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:2}}>
  <Text style={{fontSize:15}}>Date:</Text>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'space-between',alignItems:'center',paddingVertical:3,flexDirection:'row',paddingHorizontal:3}}>
            <Text style={{color:'#9A9A9A',fontSize:15}}>{this.state.confirmDate2 ?this.state.confirmDate2 :date1 ? date1: "MM/DD/YYYY"}</Text>
            <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 2,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>

</View>
</View>


{/* Second Child */}

<View style={{flexDirection:'row',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:2}}>
  <Text style={{fontSize:15}}>Signature:</Text>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'center',paddingVertical:3,borderRightWidth:0,paddingHorizontal:3}}>
<TouchableOpacity onPress ={()=>this.props.navigation.navigate('Signature', {
                    reload: this.passImage.bind(this)})} style={{backgroundColor:'#D6D8D7',paddingVertical:5}}>
  <Text style={{textAlign:'center',fontSize:17}}>UPLOAD</Text>
</TouchableOpacity>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:0}}>

</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'space-between',alignItems:'center',paddingVertical:3,flexDirection:'row',paddingHorizontal:3}}>
  

</View>
</View>
{/* last Row */}

<View style={{marginVertical:0,marginBottom:0,borderWidth:2,justifyContent:'center',paddingVertical:5,backgroundColor:'#F9BF8F',paddingHorizontal:3,borderTopWidth:0}}>
<Text style={{fontSize:15}}>CUSTOMER: I confirm thatthe above work has been carried out to my satisfection in line with my requirements</Text>
</View>

{/* First Child */}

<View style={{flexDirection:'row',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:2}}>
  <Text style={{fontSize:15}}>Print Name:</Text>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'center',paddingVertical:3,borderRightWidth:2}}>
  <TextInput
  value={pname2}
  onChangeText={pname2=>this.setState({pname2})}
  style={{fontSize:15,fontWeight:'bold',padding:0,paddingHorizontal:3}}
  />
</View>
<View style={{flex:1,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:2}}>
  <Text style={{fontSize:15}}>Date:</Text>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'space-between',alignItems:'center',paddingVertical:3,flexDirection:'row',paddingHorizontal:3}}>
            <Text style={{color:'#9A9A9A',fontSize:15}}>{ this.state.confirmDate3 ?this.state.confirmDate3  : date2 ? date2:"MM/DD/YYYY"}</Text>
            <TouchableOpacity onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'date',
                        status: 3,
                      });
                    }}>
                  <Icon   name="calendar" size={18} />
                  </TouchableOpacity>

</View>
</View>


{/* Second Child */}

<View style={{flexDirection:'row',borderWidth:2,borderTopWidth:0}}>
<View style={{flex:1,backgroundColor:'#F9BF8F',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:2}}>
  <Text style={{fontSize:15}}>Signature:</Text>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'center',paddingVertical:3,borderRightWidth:0,paddingHorizontal:3}}>
<TouchableOpacity onPress ={()=>  this.props.navigation.navigate('Signature', {
                    reload: this.passImage.bind(this)})}
                     style={{backgroundColor:'#D6D8D7',paddingVertical:5,}}>
  <Text style={{textAlign:'center',fontSize:17}}>UPLOAD</Text>
</TouchableOpacity>
</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'center',alignItems:'center',paddingVertical:3,borderRightWidth:0}}>

</View>
<View style={{flex:1,backgroundColor:'#FAFAFA',justifyContent:'space-between',alignItems:'center',paddingVertical:3,flexDirection:'row',paddingHorizontal:3}}>
  

</View>
</View>

<TouchableButton
onPress={()=>this.onSubmit()}
title="SUBMIT"
/>

</ScrollView>

<Loader isLoader={this.state.loading}></Loader>

{this.state.isDateTimePickerVisible ? (
            <DateTimePicker
              mode={this.state.mode}
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.handleDatePicked}
              onCancel={this.hideDateTimePicker}
              is24Hour={false}
             // onChange={this.handleDatePicked}
              isDarkModeEnabled= {false}
              textColor= "#000000	"
              date = {this.state.initialDateTime}

             //  onConfirm={(date)=>this.hideDateTimePicker(date)}
            />
          ) : null}
          </View>
        )
    }

    passImage(data) {
      console.log('testingggggggggggggggggggggggg', data);
      this.setState({imageDatas: data});
    }
    
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {

    backgroundColor: "white",
    borderRadius: 20,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  outerBox: {
    height: 19,
    width: 19,
    borderWidth: 2,
  
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  label: {fontSize: 15},
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 0,
  },
  radioBox: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
})
