import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Alert,
  ScrollView,
  CheckBox,
  TouchableOpacity,
  TextInput,
  TouchableHighlight,
  Modal,
  Linking
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {requestPostApiMedia, updateTpra} from '../../../WebAPI/Service';
import Orientation from 'react-native-orientation';
import HeaderWithBack from '../../../component/HeaderWithBack';
import Icon from '../../../component/Icon';
import TouchableButton from '../../../component/TouchableButton';
import Loader from '../../../WebAPI/Loader';
import moment from 'moment';
import DocumentPicker from 'react-native-document-picker';


import KeyStore from '../../KeyStore/LocalKeyStore'
class OneRowFourCoulmn extends Component {
  render() {
    return (
      <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
            paddingRight: 20,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
            {this.props.firstCoulmn}
          </Text>
        </View>
        <View
          style={{
            flex: 2,
            backgroundColor: '#DBDAD8',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: 'bold'}}>{this.props.secondCoulmn}</Text>
        </View>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
            {this.props.thirdCoulmn}
          </Text>
        </View>
        {this.props.checkBoxEnabled ? (
          <View
            style={{
              flex: 2,
              backgroundColor: '#DBDAD8',
              justifyContent: 'space-around',
              alignItems: 'flex-start',
              padding: 5,
              borderWidth: 2,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.checkValue == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.checkValue == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        ) : (
          <View
            style={{
              flex: 2,
              backgroundColor: '#DBDAD8',
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 5,
              borderWidth: 2,
            }}>
            <Text style={{fontWeight: 'bold'}}>{this.props.fourthCoulmn}</Text>
          </View>
        )}
      </View>
    );
  }
}

class OneRowTwoCoulmn extends Component {
  render() {
    return (
      <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
            {this.props.firstCoulmn}
          </Text>
        </View>

        {this.props.checkBoxEnabled ? (
          <View
            style={{
              flex: 7.2,
              backgroundColor: '#DBDAD8',
              borderWidth: 2,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.checkValue == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Days</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.checkValue == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>Night</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.checkValue == '2' ? true : null}
                style={{height: 20}}
              />
              <Text>Both</Text>
            </View>
          </View>
        ) : (
          <View
            style={{
              flex: 7,
              backgroundColor: '#DBDAD8',
              borderWidth: 2,
              justifyContent: 'center',
              alignItems: 'flex-start',
              padding: 5,
            }}>
            <Text style={{fontWeight: 'bold'}}>{this.props.secondCoulmn}</Text>
          </View>
        )}
      </View>
    );
  }
}

class DownloadUpload extends Component {
  render() {
    return (
      <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
            {this.props.firstCoulmn}
          </Text>
        </View>

        <View
          style={{
            flex: 7,
            backgroundColor: '#FAFAFA',
            borderWidth: 2,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            padding: 8,
          }}>
          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.downloadFile()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
              }}>
              <Text>DOWNLOAD</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: '',
              padding: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'auto'}}>fp1038563393.jpg</Text>
          </View>

          <View style={{flex: 1}}>
            <TouchableOpacity
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>UPLOAD</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

class VehicleReqNumOpt extends Component {
  render() {
    return <View></View>;
  }
}

class ProManDrawMeth extends Component {
  render() {
    return <View></View>;
  }
}

class PreDepartureCheck extends Component {
  render() {
    return (
      <View style={{marginBottom: 10}}>
        <View
          style={{
            marginBottom: 10,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F9BF8F',
            padding: 5,
            borderWidth: 2,
          }}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>
            PRE-DEPARTURE CHECK
          </Text>
        </View>

        <View
          style={{
            backgroundColor: '#F9BF8F',
            borderWidth: 2,
            padding: 10,
            paddingLeft: 2,
          }}>
          <Text style={{fontSize: 14, textAlign: 'justify'}}>
            Person in Charge (TEAM LEADER) to check and confirm before leaving
            depot for installation - do not proceed unless all are ok
          </Text>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Drawing, method statement and risk assesment held?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.firstCheck == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.firstCheck == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Vehicles all present and correct types ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.secondCheck == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.secondCheck == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Equipment all present,suitable and secured?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.thirdCheck == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.thirdCheck == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Vehicle inspection completed and vehicles all ok ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.fourthCheck == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.fourthCheck == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Labour all present and ssuitably trained / qualified ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.fifthCheck == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.fifthCheck == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Suitable and sufficent PPE avilable for working party ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'flex-start',
              }}>
              <CheckBox
                value={this.props.sixCheck == '1' ? true : null}
                style={{height: 20}}
              />
              <Text>Yes</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
              <CheckBox
                value={this.props.sixCheck == '0' ? true : null}
                style={{height: 20}}
              />
              <Text>No</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default class TMPlanAndResAlloc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusOfOrientation: false,
      loading: false,
      dataBeforeSubmit: {},
      ipv_no: '',
      ipv_1: '',
      ipv_2: '',
      ipv_3: '',
      ipv_4: '',
      ipv_5: '',
      vehicle_no: '',
      vehicle_1: '',
      vehicle_2: '',
      vehicle_3: '',
      vehicle_4: '',
      vehicle_5: '',
      flatbed_no: '',
      flatbed_1: '',
      flatbed_2: '',
      flatbed_3: '',
      flatbed_4: '',
      flatbed_5: '',
      p_name:'',
      p_date:'',
      telephone:'',
      operator_id:'',
      q_no:'',
      value: 1,
      drawing:'',
      vehicles_tpra:'',
      equipment:'',
      inspection:'',
      trained:'',
      ppe:'',
     
     
      open: false,
      setDate: new Date(),
      confirmDate:'',
      modalVisible:false,
      isDateTimePickerVisible: false,
      choosenDate: '',
methodStatementRefUpload:{},
riskAssessmentFormUpload:{},
drawaingRefUpload:{},

drawing_ref_name:'',
risk_frequency_name:'',
method_frequency_name:''
    
};
  }

  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (datetime) => {
    this.setState({isDateTimePickerVisible: false});

    this.setState({
      choosenDate: moment(datetime).format('DD-MM-YYYY'),
    });

   
  };

  downloadFile1(){
    console.warn("done")
    Linking.canOpenURL(this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit.method_frequency:null ).then(supported => {
      if (supported) {
      Linking.openURL(this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit.method_frequency:null);
      } else {
      console.log("Don't know how to open URI: " + this.state.dataBeforeSubmit.method_frequency);
      }
      });

  }
  
  downloadFile2(){
    console.warn("done")
    Linking.canOpenURL(this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit.risk_assessment:null ).then(supported => {
      if (supported) {
      Linking.openURL(this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit.risk_assessment:null);
      } else {
      console.log("Don't know how to open URI: " + this.state.dataBeforeSubmit.risk_assessment);
      }
      });

  }
  
  downloadFile13(){
    console.warn("done")
    Linking.canOpenURL(this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit.drawing_ref:null ).then(supported => {
      if (supported) {
      Linking.openURL(this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit.drawing_ref:null);
      } else {
      console.log("Don't know how to open URI: " + this.state.dataBeforeSubmit.drawing_ref);
      }
      });

  }
  





  
  componentWillMount() {
    // The getOrientation method is async. It happens sometimes that
    // you need the orientation at the moment the JS runtime starts running on device.
    // `getInitialOrientation` returns directly because its a constant set at the
    // beginning of the JS runtime.

    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      console.warn('portartait');
      // do something
    } else {
      // do something else
    }
  }


  async pickAttachment1(){
    try {
        const res = await DocumentPicker.pick({
          type: [DocumentPicker.types.allFiles],
        });
this.setState({methodStatementRefUpload:res})
        console.log(
          res
          
        );
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      }
}

async pickAttachment2(){
  try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
this.setState({riskAssessmentFormUpload:res})
      console.log(
        res
        
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
}

async pickAttachment3(){
  try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
this.setState({drawaingRefUpload:res})
      console.log(
        res
        
      );
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
}


  async componentDidMount() {
  console.warn("heyyyyyyyyyyyyyyyyyyy",this.props.navigation.state.params.operator_id)



   this.setState({loading: true});
    const formData = new FormData();
    formData.append('operator_id', this.props.navigation.state.params.operator_id);
    formData.append('q_no', this.props.navigation.state.params.q_no);

    const {responseJson} = await requestPostApiMedia(
      updateTpra,
      formData,
      'POST',
    );
    console.warn( "++++++++++++",responseJson.data);
    if (responseJson) {
      const {drawing,vehicles_tpra,inspection,ppe,trained,equipment} = responseJson.data
      this.setState({loading: false, dataBeforeSubmit: responseJson.data,telephone:responseJson.data.telephone,p_name:responseJson.data.p_name,drawing,vehicles_tpra,inspection,ppe,trained,equipment});
      const {method_frequency,risk_assessment,drawing_ref} = responseJson.data
      let new_method_frequency 
      new_method_frequency=  method_frequency.split("/")
      let new_risk_assessment = risk_assessment.split("/")
      let new_drawing_ref = drawing_ref.split("/")
      let drawing_ref_name = new_drawing_ref[new_drawing_ref.length-1]
      let risk_frequency_name = new_risk_assessment[new_risk_assessment.length-1]
let method_frequency_name=new_method_frequency[new_method_frequency.length-1]
this.setState({drawing_ref_name,risk_frequency_name,method_frequency_name}) 



     
      const name = "tmpara"+this.props.navigation.state.params.q_no
      KeyStore.getKey(name, (err, value) => {


        if (value) {
  
         const data = JSON.parse(value)
         const {ipv_no,vehicle_no,flatbed_no,flatbed_1,flatbed_2,flatbed_3,flatbed_4,flatbed_5,vehicle_1,vehicle_2,vehicle_3,vehicle_4,vehicle_5,ipv_1,ipv_2,ipv_3,ipv_4,ipv_5} = data
         this.setState({ipv_no,vehicle_no,flatbed_no,flatbed_1,flatbed_2,flatbed_3,flatbed_4,flatbed_5,vehicle_1,vehicle_2,vehicle_3,vehicle_4,vehicle_5,ipv_1,ipv_2,ipv_3,ipv_4,ipv_5})
        }})

      


    }
    else{
      console.warn("nooooooooooooooooooooooo")
    }

    // this locks the view to Portrait Mode
    //Orientation.lockToPortrait();

    // this locks the view to Landscape Mode
    Orientation.lockToLandscape();

    // this unlocks any previous locks to all Orientations
    //  Orientation.unlockAllOrientations();

    Orientation.addOrientationListener(this._orientationDidChange);
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      console.warn('portartaitttttttttttttt');
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  };

  componentWillUnmount() {
     Orientation.lockToPortrait();
    Orientation.getOrientation((err, orientation) => {
      console.warn(`Current Device Orientation: ${orientation}`);
    });

    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
  }
  



  async updateTMPRA(){

    const {ipv_no,ipv_1,ipv_2,ipv_3,ipv_4,ipv_5,vehicle_no,vehicle_1,vehicle_2,vehicle_3,vehicle_4,vehicle_5,flatbed_no,flatbed_1,flatbed_2,flatbed_3,flatbed_4,flatbed_5,telephone,p_date,p_name,drawing,inspection,ppe,equipment,trained,vehicles_tpra,confirmDate} = this.state





     this.setState({loading: true});
     const formData = new FormData();
     formData.append('operator_id', this.props.navigation.state.params.operator_id);
     formData.append('q_no', this.props.navigation.state.params.q_no);
     formData.append('flatbed_no', flatbed_no);
     formData.append('ipv_no', ipv_no);
     formData.append('vehicle_no', vehicle_no);
     formData.append('ipv_1', ipv_1);
     formData.append('ipv_2', ipv_2);
     formData.append('ipv_3', ipv_3);
     formData.append('ipv_4', ipv_4);
     formData.append('ipv_5', ipv_5);
     formData.append('flatbed_1', flatbed_1);
     formData.append('flatbed_2', flatbed_2);
     formData.append('flatbed_3', flatbed_3);
     formData.append('flatbed_4', flatbed_4);
     formData.append('flatbed_5', flatbed_5);
     formData.append('vehicle_1', vehicle_1);
     formData.append('vehicle_2', vehicle_2);
     formData.append('vehicle_3', vehicle_3);
     formData.append('vehicle_4', vehicle_4);
     formData.append('vehicle_5', vehicle_5);
     formData.append('p_name', p_name);
     formData.append('p_date', this.state.choosenDate);
     formData.append('telephone', telephone);
    //  formData.append('sign', '80081');
      formData.append('drawing', drawing);
      formData.append('vehicles_tpra', vehicles_tpra);
      formData.append('equipment', equipment);
      formData.append('inspection', inspection);
      formData.append('trained', trained);
      formData.append('ppe', ppe);
  

const veh_required={
  ipv_no,vehicle_no,flatbed_no,ipv_1,ipv_2,ipv_3,ipv_4,ipv_5,vehicle_1,vehicle_2,vehicle_3,vehicle_4,vehicle_5,flatbed_1,flatbed_2,flatbed_3,flatbed_4,flatbed_5
}

// make for local storage 
const name = "tmpara"+this.props.navigation.state.params.q_no


KeyStore.setKey(name, JSON.stringify(veh_required));


     const {responseJson} = await requestPostApiMedia(
      updateTpra,
       formData,
      'POST',
   );
     console.warn("heko",responseJson);
    if (responseJson) {

      this.setState({loading: false, });
      Alert.alert('',responseJson.message,
      [
        
        {
          text: "Ok",
          onPress: () => this.props.navigation.navigate('FormsList')
          
        },
      ])
   }






  }

  render() {





    const {
      id,
      date_tpra,
      finish_date,
      maintenance,
      frequency,
      when_required,
      msref,
      time_on,
      time_off,
      additional,
      p_name,
      p_date,
      labour,
      telephone,
      sign,
      
      method_frequency,
      risk_assessment,
      drawing_ref,
      tpra_update_status,
    } = this.state.dataBeforeSubmit ? this.state.dataBeforeSubmit : null;
const {drawing,vehicles_tpra,inspection,ppe,trained,equipment,drawing_ref_name,risk_frequency_name,method_frequency_name}= this.state







    return (
      <View style={{flex: 1, backgroundColor: '#FBFBFB'}}>
        <HeaderWithBack
          title="TM PLANING AND RESOURCE ALLOCATION"
          goBack={() =>
            Alert.alert('Amber RTM', 'Do you want to Exit?', [
              {text: 'No', style: 'cancel'},
              {text: 'Yes', onPress: () => this.props.navigation.goBack()},
            ])
          }
        />
        <ScrollView style={{margin: 8, backgroundColor: '', flex: 1}}>
          {/* <OneRowFourCoulmn
            firstCoulmn="Start Date :"
            secondCoulmn={date_tpra}
            thirdCoulmn="Managed by AMBER-RTM"
            fourthCoulmn=""
            checkValue={maintenance}
            checkBoxEnabled={true}
          /> */}


<View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
            paddingRight: 20,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
          Start Date :
          </Text>
        </View>
        <View
          style={{
            flex: 2,
            backgroundColor: '#DBDAD8',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: 'bold'}}>{date_tpra}</Text>
        </View>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
          Managed by AMBER-RTM
          </Text>
        </View>
       
          <View
            style={{
              flex: 2,
              backgroundColor: '#DBDAD8',
              justifyContent: 'space-around',
              alignItems: 'flex-start',
              padding: 5,
              borderWidth: 2,
              flexDirection: 'row',
            }}>
            
            <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
            // onPress={() => this.setState({value: 1})}
             >
              <View style={[styles.outerBox,{ backgroundColor:maintenance ==='1' ?'#FF1493':'#DBDAD8', borderWidth:maintenance ==='1'?0:1}]}>
                {maintenance === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
           // onPress={() => this.setState({value: 2})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:maintenance ==='0' ?'#FF1493':'#DBDAD8', borderWidth:maintenance ==='0'?0:1}]}>
                {maintenance === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
       
      </View>

          <OneRowFourCoulmn
            firstCoulmn="Finshe Date :"
            secondCoulmn={finish_date}
            thirdCoulmn="Maintenance Frequency :"
            fourthCoulmn={frequency}
            checkBoxEnabled={false}
          />

          {/* <OneRowTwoCoulmn
            firstCoulmn="When is the TM Required?"
            checkBoxEnabled={true}
            checkValue={when_required}
          /> */}


<View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
          When is the TM Required?
          </Text>
        </View>

        
          <View
            style={{
              flex: 7.2,
              backgroundColor: '#DBDAD8',
              borderWidth: 2,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            
           
            <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
            // onPress={() => this.setState({value: 1})}
             >
              <View style={[styles.outerBox,{ backgroundColor:when_required ==='0' ?'#FF1493':'#DBDAD8', borderWidth:when_required ==='0'?0:1}]}>
                {when_required === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Days</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
           // onPress={() => this.setState({value: 2})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:when_required ==='1' ?'#FF1493':'#DBDAD8', borderWidth:when_required ==='1'?0:1}]}>
                {when_required === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Night</Text>
          </View>


          <View style={styles.radioBox}>
            <TouchableOpacity 
           // onPress={() => this.setState({value: 2})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:when_required ==='2' ?'#FF1493':'#DBDAD8', borderWidth:when_required ==='2'?0:1}]}>
                {when_required === '2' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Both</Text>
          </View>

        </View>

           
          </View>
     
        
      
      </View>

          <OneRowFourCoulmn
            firstCoulmn="Tme On :"
            secondCoulmn={time_on}
            thirdCoulmn="Time Off :"
            fourthCoulmn={time_off}
            checkBoxEnabled={false}
          />

<View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
          Method Statment Ref:
          </Text>
        </View>

        <View
          style={{
            flex: 7,
            backgroundColor: '#FAFAFA',
            borderWidth: 2,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            padding: 8,
          }}>
          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.downloadFile1()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
              }}>
              <Text>DOWNLOAD</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: '',
              padding: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'auto'}}>
             
              {method_frequency_name?method_frequency_name:this.state.methodStatementRefUpload ?this.state.methodStatementRefUpload.name:null  }
              </Text>
          </View>

          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.pickAttachment1()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>UPLOAD</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

    {/* second */}

    <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
          Risk Assessment Ref:
          </Text>
        </View>

        <View
          style={{
            flex: 7,
            backgroundColor: '#FAFAFA',
            borderWidth: 2,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            padding: 8,
          }}>
          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.downloadFile()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
              }}>
              <Text>DOWNLOAD</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: '',
              padding: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'auto'}}>{risk_frequency_name ? risk_frequency_name : this.state.riskAssessmentFormUpload?this.state.riskAssessmentFormUpload.name:null}</Text>
          </View>

          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.pickAttachment2()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>UPLOAD</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      {/* Third */}

      <View style={{flexDirection: 'row', marginBottom: 8}}>
        <View
          style={{
            flex: 3,
            backgroundColor: '#F9BF8F',
            justifyContent: 'center',
            alignItems: 'flex-start',
            padding: 5,
            borderWidth: 2,
            marginRight: 5,
          }}>
          <Text style={{fontWeight: '900', fontSize: 15}}>
          Drawing Ref(s):
          </Text>
        </View>

        <View
          style={{
            flex: 7,
            backgroundColor: '#FAFAFA',
            borderWidth: 2,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            padding: 8,
          }}>
          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.downloadFile1()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
              }}>
              <Text>DOWNLOAD</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flex: 1,
              backgroundColor: '',
              padding: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'auto'}}>{ drawing_ref_name ? drawing_ref_name: this.state.drawaingRefUpload?this.state.drawaingRefUpload.name:null}</Text>
          </View>

          <View style={{flex: 1}}>
            <TouchableOpacity
            onPress={()=>this.pickAttachment3()}
              style={{
                backgroundColor: '#D6D8D7',
                padding: 3,
                paddingLeft: 30,
                paddingRight: 30,
                borderRadius: 3,
                elevation: 3,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text>UPLOAD</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

          {/* <DownloadUpload firstCoulmn="Method Statment Ref:" /> */}
          {/* <DownloadUpload firstCoulmn="Risk Assessment Ref:" />
          <ownloadUpload firstCoulmn="Drawing Ref(s):" /> */}

          <OneRowTwoCoulmn
            firstCoulmn="Additional Instruction/  Contact Specific Requirement"
            secondCoulmn={additional}
            checkBoxEnabled={false}
          />
          <OneRowTwoCoulmn
            firstCoulmn="Labour Quantites Required"
            secondCoulmn={labour}
            checkBoxEnabled={false}
          />

          {/* <VehicleReqNumOpt /> */}

          <View style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#F9BF8F',
                padding: 10,
                borderWidth: 2,
                paddingLeft: 2,
              }}>
              <View
                style={{
                  flex: 2.5,
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                }}>
                <Text style={{fontSize: 14}}>Vehicles Required</Text>
              </View>
              <View style={{flex: 1, backgroundColor: ''}}>
                <Text style={{fontSize: 14}}>Number</Text>
              </View>
              <View style={{flex: 5, backgroundColor: ''}}>
                <Text style={{fontSize: 14, textAlign: 'auto'}}>
                  Operations-Note Registration number of Vehicles Allocated
                </Text>
              </View>
            </View>
            {/* second row */}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                backgroundColor: '#FAFAFA',
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: 2,
                padding: 2,
              }}>
              <Text style={{fontSize: 16}}>IPV :</Text>
            </View>
            {/* third Row */}
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#FAFAFA',
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: 2,
              }}>
              <View style={{flex: 3, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.ipv_no}
                  onChangeText={(ipv_no) => this.setState({ipv_no})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.ipv_1}
                  onChangeText={(ipv_1) => this.setState({ipv_1})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.ipv_2}
                  onChangeText={(ipv_2) => this.setState({ipv_2})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.ipv_3}
                  onChangeText={(ipv_3) => this.setState({ipv_3})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.ipv_4}
                  onChangeText={(ipv_4) => this.setState({ipv_4})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2}}>
                <TextInput
                  value={this.state.ipv_5}
                  onChangeText={(ipv_5) => this.setState({ipv_5})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
            </View>
            {/* second row */}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                backgroundColor: '#FAFAFA',
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: 2,
                padding: 2,
              }}>
              <Text style={{fontSize: 16}}>7.5t Installation Vehicles :</Text>
            </View>
            {/* third Row */}
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#FAFAFA',
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: 2,
              }}>
              <View style={{flex: 3, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.vehicle_no}
                  onChangeText={(vehicle_no) => this.setState({vehicle_no})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.vehicle_1}
                  onChangeText={(vehicle_1) => this.setState({vehicle_1})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.vehicle_2}
                  onChangeText={(vehicle_2) => this.setState({vehicle_2})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.vehicle_3}
                  onChangeText={(vehicle_3) => this.setState({vehicle_3})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.vehicle_4}
                  onChangeText={(vehicle_4) => this.setState({vehicle_4})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2}}>
                <TextInput
                  value={this.state.vehicle_5}
                  onChangeText={(vehicle_5) => this.setState({vehicle_5})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
            </View>

            {/* second row */}
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                backgroundColor: '#FAFAFA',
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: 2,
                padding: 2,
              }}>
              <Text style={{fontSize: 16}}>3.5t Transit Flatbed :</Text>
            </View>
            {/* third Row */}
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#FAFAFA',
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: 2,
              }}>
              <View style={{flex: 3, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.flatbed_no}
                  onChangeText={(flatbed_no) => this.setState({flatbed_no})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.flatbed_1}
                  onChangeText={(flatbed_1) => this.setState({flatbed_1})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.flatbed_2}
                  onChangeText={(flatbed_2) => this.setState({flatbed_2})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.flatbed_3}
                  onChangeText={(flatbed_3) => this.setState({flatbed_3})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2, borderRightWidth: 2}}>
                <TextInput
                  value={this.state.flatbed_4}
                  onChangeText={(flatbed_4) => this.setState({flatbed_4})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
              <View style={{flex: 2}}>
                <TextInput
                  value={this.state.flatbed_5}
                  onChangeText={(flatbed_5) => this.setState({flatbed_5})}
                  style={{
                    padding: 0,
                    fontWeight: 'bold',
                    fontSize: 15,
                    paddingLeft: 2,
                  }}
                />
              </View>
            </View>
          </View>

          {/* <ProManDrawMeth /> */}

          <View style={{marginBottom: 10}}>
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#F9BF8F',
                padding: 5,
                borderWidth: 2,
                paddingLeft: 2,
              }}>
              <View style={{flex: 1}}>
                <Text>
                  Project Manager:drawing, method statement and risk assessment
                  are in place,requirement
                </Text>
              </View>
              <View style={{flex: 1}}>
                <Text>
                  labours/vehicles aare identified are suitable to satisfy
                  contract
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '',
                borderWidth: 2,
                borderTopWidth: 0,
              }}>
              <View
                style={{flexDirection: 'row', flex: 1, borderRightWidth: 2}}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: '#F9BF8F',
                    borderRightWidth: 2,
                  }}>
                  <View style={{flex: 1, borderBottomWidth: 2}}>
                    <Text>Print Name:</Text>
                  </View>
                  <View style={{flex: 1}}>
                    <Text>Telephone Number:</Text>
                  </View>
                </View>
                <View style={{flex: 1}}>
                  <View style={{flex: 1, borderBottomWidth: 2}}>
                    <TextInput
                      value={this.state.p_name}
                      onChangeText={p_name=>this.setState({p_name})}
                      style={{
                        padding: 0,
                        fontWeight: 'bold',
                        fontSize: 15,
                        paddingLeft: 2,
                      }}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <TextInput
                      value={this.state.telephone}
                      onChangeText={telephone=>this.setState({telephone})}
                      style={{
                        padding: 0,
                        fontWeight: 'bold',
                        fontSize: 15,
                        paddingLeft: 2,
                      }}
                    />
                  </View>
                </View>
              </View>
              <View style={{flexDirection: 'row', flex: 1}}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: '#F9BF8F',
                    borderRightWidth: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text>Date :</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text>{this.state.choosenDate
                      ? this.state.choosenDate
                      : p_date}</Text>
                  <TouchableOpacity 
                  onPress={() => {
                    this.setState({
                      isDateTimePickerVisible: true,
                     
                    });
                  }}
        
        >
                  <Icon   name="calendar" size={20} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
          {/* <PreDepartureCheck
            firstCheck={drawing}
            secondCheck={vehicles_tpra}
            thirdCheck={equipment}
            fourthCheck={inspection}
            fifthCheck={trained}
            sixCheck={ppe}
          /> */}

<View style={{marginBottom: 10}}>
        <View
          style={{
            marginBottom: 10,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F9BF8F',
            padding: 5,
            borderWidth: 2,
          }}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>
            PRE-DEPARTURE CHECK
          </Text>
        </View>

        <View
          style={{
            backgroundColor: '#F9BF8F',
            borderWidth: 2,
            padding: 10,
            paddingLeft: 2,
          }}>
          <Text style={{fontSize: 14, textAlign: 'justify'}}>
            Person in Charge (TEAM LEADER) to check and confirm before leaving
            depot for installation - do not proceed unless all are ok
          </Text>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Drawing, method statement and risk assesment held?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
      
      <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
             onPress={() => this.setState({drawing: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:drawing ==='1' ?'#FF1493':'#fff', borderWidth:drawing ==='1'?0:1}]}>
                {drawing === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
            onPress={() => this.setState({drawing: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:drawing ==='0' ?'#FF1493':'#fff', borderWidth:drawing ==='0'?0:1}]}>
                {drawing === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Vehicles all present and correct types ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            
            <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
             onPress={() => this.setState({vehicles_tpra: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:vehicles_tpra ==='1' ?'#FF1493':'#fff', borderWidth:vehicles_tpra ==='1'?0:1}]}>
                {vehicles_tpra === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
            onPress={() => this.setState({vehicles_tpra: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:vehicles_tpra ==='0' ?'#FF1493':'#fff', borderWidth:vehicles_tpra ==='0'?0:1}]}>
                {vehicles_tpra === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Equipment all present,suitable and secured?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
           
           <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
             onPress={() => this.setState({equipment: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:equipment ==='1' ?'#FF1493':'#fff', borderWidth:equipment ==='1'?0:1}]}>
                {equipment === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
            onPress={() => this.setState({equipment: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:equipment ==='0' ?'#FF1493':'#fff', borderWidth:equipment ==='0'?0:1}]}>
                {equipment === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Vehicle inspection completed and vehicles all ok ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
           
           <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
             onPress={() => this.setState({inspection: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:inspection ==='1' ?'#FF1493':'#fff', borderWidth:inspection ==='1'?0:1}]}>
                {inspection === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
            onPress={() => this.setState({inspection: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:inspection ==='0' ?'#FF1493':'#fff', borderWidth:inspection ==='0'?0:1}]}>
                {inspection === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Labour all present and ssuitably trained / qualified ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
           
           <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
             onPress={() => this.setState({trained: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:trained ==='1' ?'#FF1493':'#fff', borderWidth:trained ==='1'?0:1}]}>
                {trained === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
            onPress={() => this.setState({trained: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:trained ==='0' ?'#FF1493':'#fff', borderWidth:trained ==='0'?0:1}]}>
                {trained === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
          <View
            style={{
              flex: 2,
              backgroundColor: '#F9BF8F',
              padding: 5,
              borderRightWidth: 2,
              paddingLeft: 2,
            }}>
            <Text>Suitable and sufficent PPE avilable for working party ?</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
         
         <View style={styles.container}>
          <View style={styles.radioBox}>
            <TouchableOpacity
             onPress={() => this.setState({ppe: '1'})}
             >
              <View style={[styles.outerBox,{ backgroundColor:ppe ==='1' ?'#FF1493':'#fff', borderWidth:ppe ==='1'?0:1}]}>
                {ppe === '1' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity 
            onPress={() => this.setState({ppe: '0'})}
           
           >
            
              <View style={[styles.outerBox,{ backgroundColor:ppe ==='0' ?'#FF1493':'#fff', borderWidth:ppe ==='0'?0:1}]}>
                {ppe === '0' && <Icon name="check" color="white" size={15} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
          </View>
        </View>
      </View>

          <TouchableButton 
          onPress={()=>this.updateTMPRA()}
          title="SUBMIT" />
        </ScrollView>
        <Loader isLoader={this.state.loading}></Loader>
        {/* <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          <View>
              <View style={{backgroundColor: '', marginBottom: 100}}>
                <DatePicker
                mode="date"
                  date={this.state.setDate}
                  onDateChange={(setDatess) => this.setState({setDate:moment(setDatess).format('MM-DD- YYYY')})}
                  androidVariant="nativeAndroid"
                />
              </View>
              <View style={{flexDirection: 'row',borderTopWidth:1}}>
              <TouchableOpacity
              onPress={() => this.setState({open:false,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({open:false,confirmDate:this.state.setDate,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>Ok</Text>
                </TouchableOpacity>
                
              </View>
            </View>

          </View>
        </View>
      </Modal> */}


{
  this.state.isDateTimePickerVisible ?
  <DateTimePicker
  mode="date"
  isVisible={this.state.isDateTimePickerVisible}
  onConfirm={this.handleDatePicked}
  onCancel={this.hideDateTimePicker}
  is24Hour={false}
  onChange={this.handleDatePicked}
  // onConfirm={(date)=>this._handleDatePicked(date)}
/> :null
}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {

    backgroundColor: "white",
    borderRadius: 20,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },

   outerBox: {
     height: 19,
     width: 19,
     borderWidth: 2,
   
     justifyContent: 'center',
     alignItems: 'center',
     marginRight: 10,
   },
   label: {fontSize: 15},
   container: {
     flexDirection: 'row',
     justifyContent: 'space-around',
     marginTop: 0,
   },
   radioBox: {
     flexDirection: 'row',
     alignItems: 'center',
     flex: 1,
     justifyContent: 'center',
   },
 });
 