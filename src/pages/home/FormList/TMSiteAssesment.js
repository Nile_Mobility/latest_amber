import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Alert,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Modal,
} from 'react-native';
import HeaderWithBack from '../../../component/HeaderWithBack';
//import DatePicker from 'react-native-date-picker';
import Orientation from 'react-native-orientation';
import Icon from '../../../component/Icon';
import TouchableButton from '../../../component/TouchableButton';
import Loader from '../../../WebAPI/Loader';
import {update_Tsaf, requestPostApiMedia} from '../../../WebAPI/Service';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

export default class TMSiteAssesment extends Component {
  constructor(props) {
    super();
    this.state = {
      loading: false,
      date_tsaf: '',
      location: '',

      works_order: '',
      time_tsaf: '',
      road_no: '',
      name_tsaf: '',
      speed: '',
      width: '',
      traffic: '',
      f_width: '',
      f_width2: '',
      verge: '',
      cycleway: '',
      ped_count: '',
      description: '',
      carriageway: '',
      footway: '',
      duration2: '',
      road1_tsaf: '',
      ttro: '',
      duration1: '',
      road2_tsaf: '',
      temp_ob: '',
      mobileworks: '',
      duration_over1: '',
      give_take: '',
      duration_over2: '',
      priority: '',
      duration_over3: '',
      stop: '',
      duration_over4: '',
      traffic_signal: '',
      duration_5: '',
      road_wide: '',
      temp_clos: '',
      temp_duration: '',
      walk_carriage: '',
      walk_duration: '',
      divert: '',
      divert_duration: '',
      temp_signals: '',
      tempsignal_duration: '',
      more_than_3: '',
      add_info: '',
      isDateTimePickerVisible: false,
      choosenDateTime: '',
      mode: '',
      value: '',

      open: false,
      setDate: new Date(),
      confirmDate: '',
      modalVisible: false,
      isDateTimePickerVisible: false,
      choosenDate: '',
    };
  }

  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (datetime) => {
    this.setState({isDateTimePickerVisible: false});

    this.setState({
      choosenDate: moment(datetime).format('MM/DD/YYYY'),
    });
  };

  // showDateTimePicker = () => {
  //   this.setState({isDateTimePickerVisible: true});
  // };

  // hideDateTimePicker = () => {
  //   this.setState({isDateTimePickerVisible: false});
  // };

  // handleDatePicked = (datetime) => {
  //   this.setState({isDateTimePickerVisible: false,choosenDateTime: moment(datetime).format('MM/D/YYYY')});

  // };

  async componentDidMount() {
    this.setState({loading: true});
    console.warn('data', this.props.navigation.state.params.q_no);

    const formData = new FormData();
    formData.append('q_no', this.props.navigation.state.params.q_no);

    const {responseJson, err} = await requestPostApiMedia(
      update_Tsaf,
      formData,
      'POST',
    );

    if (responseJson.status) {
      console.warn(responseJson.data);
      const {
        date_tsaf,
        location,
        works_order,
        time_tsaf,
        road_no,
        name_tsaf,
        speed,
        width,
        traffic,
        f_width,
        f_width2,
        verge,
        cycleway,
        ped_count,
        description,
        carriageway,
        footway,
        duration2,
        road1_tsaf,
        ttro,
        duration1,
        road2_tsaf,
        temp_ob,
        mobileworks,
        duration_over1,
        give_take,
        duration_over2,
        priority,
        duration_over3,
        stop,
        duration_over4,
        traffic_signal,
        duration_5,
        road_wide,
        temp_clos,
        temp_duration,
        walk_carriage,
        walk_duration,
        divert,
        divert_duration,
        temp_signals,
        tempsignal_duration,
        more_than_3,
        add_info,
      } = responseJson.data;
      this.setState({
        loading: false,
        choosenDate: date_tsaf,
        location,
        works_order,
        time_tsaf,
        road_no,
        name_tsaf,
        speed,
        width,
        traffic,
        f_width,
        f_width2,
        verge,
        cycleway,
        ped_count,
        description,
        carriageway,
        footway,
        duration2,
        road1_tsaf,
        ttro,
        duration1,
        road2_tsaf,
        temp_ob,
        mobileworks,
        duration_over1,
        give_take,
        duration_over2,
        priority,
        duration_over3,
        stop,
        duration_over4,
        traffic_signal,
        duration_5,
        road_wide,
        temp_clos,
        temp_duration,
        walk_carriage,
        walk_duration,
        divert,
        divert_duration,
        temp_signals,
        tempsignal_duration,
        more_than_3,
        add_info,
      });
    }

    Orientation.lockToLandscape();

    Orientation.addOrientationListener(this._orientationDidChange);
  }

  async onSubmit() {
    const {
      date_tsaf,
      location,
      works_order,
      time_tsaf,
      road_no,
      name_tsaf,
      speed,
      width,
      traffic,
      f_width,
      f_width2,
      verge,
      cycleway,
      ped_count,
      description,
      carriageway,
      footway,
      duration2,
      road1_tsaf,
      ttro,
      duration1,
      road2_tsaf,
      temp_ob,
      mobileworks,
      duration_over1,
      give_take,
      duration_over2,
      priority,
      duration_over3,
      stop,
      duration_over4,
      traffic_signal,
      duration_5,
      road_wide,
      temp_clos,
      temp_duration,
      walk_carriage,
      walk_duration,
      divert,
      divert_duration,
      temp_signals,
      tempsignal_duration,
      more_than_3,
      add_info,
      choosenDateTime,
      confirmDate,
    } = this.state;
    this.setState({loading: true});

    const formData = new FormData();
    formData.append('q_no', this.props.navigation.state.params.q_no);
    formData.append('date_tsaf', this.state.choosenDate);
    formData.append('location', location);
    formData.append('works_order', works_order);
    formData.append('time_tsaf', time_tsaf);
    formData.append('road_no', road_no);
    formData.append('name_tsaf', name_tsaf);
    formData.append('speed', speed);
    formData.append('width', width);
    formData.append('traffic', traffic);
    formData.append('f_width', f_width);
    formData.append('f_width2', f_width2);
    formData.append('verge', verge);
    formData.append('cycleway', cycleway);
    formData.append('ped_count', ped_count);
    formData.append('description', description);
    formData.append('carriageway', carriageway);
    formData.append('footway', footway);
    formData.append('duration2', duration2);
    formData.append('road1_tsaf', road1_tsaf);
    formData.append('ttro', ttro);
    formData.append('duration1', duration1);
    formData.append('road2_tsaf', road2_tsaf);
    formData.append('temp_ob', temp_ob);
    formData.append('mobileworks', mobileworks);
    formData.append('duration_over1', duration_over1);
    formData.append('give_take', give_take);
    formData.append('duration_over2', duration_over2);
    formData.append('priority', priority);
    formData.append('duration_over3', duration_over3);
    formData.append('stop', stop);
    formData.append('duration_over4', duration_over4);
    formData.append('traffic_signal', traffic_signal);
    formData.append('duration_5', duration_5);
    formData.append('road_wide', road_wide);
    formData.append('temp_clos', temp_clos);
    formData.append('temp_duration', temp_duration);
    formData.append('walk_carriage', walk_carriage);
    formData.append('walk_duration', walk_duration);
    formData.append('divert', divert);
    formData.append('divert_duration', divert_duration);
    formData.append('temp_signals', temp_signals);
    formData.append('tempsignal_duration', tempsignal_duration);
    formData.append('more_than_3', more_than_3);
    formData.append('add_info', add_info);

    const {responseJson, err} = await requestPostApiMedia(
      update_Tsaf,
      formData,
      'POST',
    );

    if (responseJson.status) {
      console.warn(responseJson.data);

      this.setState({
        loading: false,
      });
      Alert.alert('', responseJson.message, [
        {
          text: 'Ok',
          onPress: () => this.props.navigation.navigate('FormsList'),
        },
      ]);
    }
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      console.warn('portartaitttttttttttttt');
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  };

  componentWillMount() {
    // The getOrientation method is async. It happens sometimes that
    // you need the orientation at the moment the JS runtime starts running on device.
    // `getInitialOrientation` returns directly because its a constant set at the
    // beginning of the JS runtime.

    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      console.warn('portartait');
      // do something
    } else {
      // do something else
    }
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    Orientation.getOrientation((err, orientation) => {
      console.warn(`Current Device Orientation: ${orientation}`);
    });

    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

  render() {
    const {
      date_tsaf,
      location,
      works_order,
      time_tsaf,
      road_no,
      name_tsaf,
      speed,
      width,
      traffic,
      f_width,
      f_width2,
      verge,
      cycleway,
      ped_count,
      description,
      carriageway,
      footway,
      duration2,
      road1_tsaf,
      ttro,
      duration1,
      road2_tsaf,
      temp_ob,
      mobileworks,
      duration_over1,
      give_take,
      duration_over2,
      priority,
      duration_over3,
      stop,
      duration_over4,
      traffic_signal,
      duration_5,
      road_wide,
      temp_clos,
      temp_duration,
      walk_carriage,
      walk_duration,
      divert,
      divert_duration,
      temp_signals,
      tempsignal_duration,
      more_than_3,
      add_info,
      value,
    } = this.state;
console.warn(verge,cycleway)
    return (
      <View style={{flex: 1, backgroundColor: '#FBFBFB'}}>
        <HeaderWithBack
          title="TM SITE ASSESSMENT FORM"
          goBack={() =>
            Alert.alert('Amber RTM', 'Do you want to Exit?', [
              {text: 'No', style: 'cancel'},
              {text: 'Yes', onPress: () => this.props.navigation.goBack()},
            ])
          }
        />

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{margin: 8, backgroundColor: '', flex: 1}}>
          {/* Zero Row */}

          <View style={{flexDirection: 'row', borderWidth: 2}}>
            {/* zero first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Date:</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  flexDirection: 'row',
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <Text style={{fontSize: 13, fontWeight: 'bold'}}>
                 
                  {this.state.choosenDate
                    ? this.state.choosenDate
                    : date_tsaf
                    ? date_tsaf
                    : 'MM/DD/YYYY'}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      isDateTimePickerVisible: true,
                    });
                  }}>
                  <Icon name="calendar" size={18} />
                </TouchableOpacity>
              </View>
            </View>

            {/* zero second */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Location:</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(location) => this.setState({location})}
                  value={location}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>

            {/* zero third */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Works Order:</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 0,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(works_order) => this.setState({works_order})}
                  value={works_order}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          </View>

          {/* First Row */}

          <View
            style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
            {/* First first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Time of Traffic Count:
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(time_tsaf) => this.setState({time_tsaf})}
                  value={time_tsaf}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>

            {/* First second */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Road No.:</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(road_no) => this.setState({road_no})}
                  value={road_no}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>

            {/* First third */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Name:</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 0,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(name_tsaf) => this.setState({name_tsaf})}
                  value={name_tsaf}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          </View>

          {/* Existing Carriage Way Condition Row */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              borderTopWidth: 0,
              backgroundColor: '#F9BF8F',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  paddingHorizontal: 3,
                }}>
                1.1
              </Text>
            </View>

            <View
              style={{
                flex: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Existing Carriageway Conditions
              </Text>
            </View>
          </View>

          <View
            style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
            {/* first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Road Speed:
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(speed) => this.setState({speed})}
                  value={speed}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
                <Text style={{textAlign: 'right'}}>mph</Text>
              </View>
            </View>

            {/*  second */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Carriageway Width:
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <View style={{flex: 4}}>
                  <TextInput
                    onChangeText={(width) => this.setState({width})}
                    value={width}
                    style={{
                      padding: 0,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}
                  />
                </View>
                <View style={{flex: 1}}>
                  <Text style={{textAlign: 'center'}}>M</Text>
                </View>
              </View>
            </View>

            {/*  third */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Traffic Count{' '}
                </Text>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  No. of Vehicles in 3 minute:
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 0,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}>
                <TextInput
                  onChangeText={(traffic) => this.setState({traffic})}
                  value={traffic}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          </View>

          {/*Existing Conditions for Pedestrain/ Cyclists */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              borderTopWidth: 0,
              backgroundColor: '#F9BF8F',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  paddingHorizontal: 3,
                }}>
                1.2
              </Text>
            </View>

            <View
              style={{
                flex: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Existing Conditions for Pedestrain/ Cyclists
              </Text>
            </View>
          </View>

          {/* 1.2 Rows */}

          <View
            style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
            {/* first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Category
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Footway Width
                </Text>
              </View>
            </View>

            {/* first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Category
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Footway Width
                </Text>
              </View>
            </View>
            {/* first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>Verge</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Cycle Way
                </Text>
              </View>
            </View>
            {/* first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Pedistrain/ Cycle Count
                </Text>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  No. in 3 minutes
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 0,
                  paddingVertical: 3,
                }}>
                <TextInput
                  onChangeText={(ped_count) => this.setState({ped_count})}
                  value={ped_count}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          </View>

          {/* 1.2 Second Row */}
          <View
            style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
            {/* first */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>
                  Urban/ Estate
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                  flexDirection: 'row',
                }}>
                <View style={{flex: 4}}>
                  <TextInput
                    onChangeText={(f_width) => this.setState({f_width})}
                    value={f_width}
                    style={{
                      padding: 0,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{textAlign: 'center'}}>m</Text>
                </View>
              </View>
            </View>

            {/* second */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14, textAlign: 'center'}}>Rural</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                  flexDirection: 'row',
                }}>
                <View style={{flex: 4}}>
                  <TextInput
                    onChangeText={(f_width2) => this.setState({f_width2})}
                    value={f_width2}
                    style={{
                      padding: 0,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{textAlign: 'center'}}>m</Text>
                </View>
              </View>
            </View>
            {/* third */}
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({verge: this.state.verge =="true" ? "false":"true"})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: verge=="true" ? '#FF1493' : '#fff',
                        borderWidth: verge=="true" ? 0 : 1,
                      },
                    ]}>
                    {verge=="true" && <Icon name="check" color="white" size={15} /> }
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({cycleway: this.state.cycleway =="true" ? "false":"true"})
                  }>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: cycleway =="true" ? '#FF1493' : '#fff',
                        borderWidth: cycleway =="true" ? 0 : 1,
                      },
                    ]}>
                    {cycleway =="true"&&
                      <Icon name="check" color="white" size={15} />
                    }
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            {/* Fourth */}
            <View
              style={{
                flex: 1,
                marginVertical: 3,
                justifyContent: 'space-around',
              }}>
              <Text
                style={{fontSize: 14, textAlign: 'center', marginVertical: 3}}>
                Description of users
              </Text>
              <TextInput
                onChangeText={(description) => this.setState({description})}
                value={description}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  marginVertical: 3,
                }}
              />
            </View>
          </View>

          {/* Remaining Width Avilable */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              borderTopWidth: 0,
              backgroundColor: '#F9BF8F',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  paddingHorizontal: 3,
                }}>
                2.1
              </Text>
            </View>

            <View
              style={{
                flex: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Remaining Width avilable to Traffic, Pedstrains During Works
              </Text>
            </View>
          </View>

          <View
            style={{flexDirection: 'row', borderWidth: 2, borderTopWidth: 0}}>
            {/* zero first */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Carriageway:(A)</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 2,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                  flexDirection: 'row',
                }}>
                <View style={{flex: 4}}>
                  <TextInput
                    onChangeText={(carriageway) => this.setState({carriageway})}
                    value={carriageway}
                    style={{
                      padding: 0,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{textAlign: 'center'}}>m</Text>
                </View>
              </View>
            </View>

            {/* zero third */}

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#F9BF8F',
                  borderRightWidth: 2,
                  paddingVertical: 3,
                }}>
                <Text style={{fontSize: 14}}>Footway/ Cycleway:(B)</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'space-between',
                  backgroundColor: '#FAFAFA',
                  borderRightWidth: 0,
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                  flexDirection: 'row',
                }}>
                <View style={{flex: 4}}>
                  <TextInput
                    onChangeText={(footway) => this.setState({footway})}
                    value={footway}
                    style={{
                      padding: 0,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{textAlign: 'center'}}>m</Text>
                </View>
              </View>
            </View>
          </View>

          {/*Include Maerial */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              borderTopWidth: 0,
              backgroundColor: '#FAFAFA',
            }}>
            <View
              style={{
                flex: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Include Materials, Site Machinary, Vehicles in Works and
                Appropriate Safety Zone
              </Text>
            </View>
          </View>

          {/* Carriageway for Standard */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              borderTopWidth: 0,
              backgroundColor: '#F9BF8F',
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  paddingHorizontal: 3,
                }}>
                3.1
              </Text>
            </View>

            <View
              style={{
                flex: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Carriageway for Standard Traffic Management ( from section 2.1,
                A)
              </Text>
            </View>
          </View>

          {/* Internal Content */}

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 8,
            }}>
            <View
              style={{
                borderWidth: 2,
                flexDirection: 'row',
                paddingVertical: 10,
                paddingHorizontal: 7,
              }}>
              <Text>A: Under 3.00m:</Text>
              <TouchableOpacity onPress={() => this.setState({more_than_3: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: more_than_3 === '0' ? '#FF1493' : '#fff',
                      borderWidth: more_than_3 === '0' ? 0 : 1,
                    },
                  ]}>
                  {more_than_3 === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}></View>
            <View
              style={{
                flex: 10,
                flexDirection: 'row',
                borderWidth: 2,
                backgroundColor: '#F9BF8F',
              }}>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>NO</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>YES</Text>
              </View>
              <View
                style={{
                  flex: 2,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}></View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>YES</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>NO</Text>
              </View>
              <View
                style={{
                  flex: 3,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>Comment</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 0,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>Duration</Text>
              </View>
            </View>
          </View>

          {/* 3.1 second  */}

          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}>
              <View>
                <Text
                  style={{
                    borderWidth: 2,
                    textAlign: 'center',
                    marginHorizontal: 2,
                  }}>
                  Road Closer
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 10,
                flexDirection: 'row',
                borderWidth: 2,
                backgroundColor: '#FAFAFA',
                borderTopWidth: 0,
              }}>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({road1_tsaf: '0'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor:
                          road1_tsaf === '0' ? '#FF1493' : '#fff',
                        borderWidth: road1_tsaf === '0' ? 0 : 1,
                      },
                    ]}>
                    {road1_tsaf === '0' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({road1_tsaf: '1'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor:
                          road1_tsaf === '1' ? '#FF1493' : '#fff',
                        borderWidth: road1_tsaf === '1' ? 0 : 1,
                      },
                    ]}>
                    {road1_tsaf === '1' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 2,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text>TTRO</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => this.setState({ttro: '1'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: ttro === '1' ? '#FF1493' : '#fff',
                        borderWidth: ttro === '1' ? 0 : 1,
                      },
                    ]}>
                    {ttro === '1' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => this.setState({ttro: '0'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: ttro === '0' ? '#FF1493' : '#fff',
                        borderWidth: ttro === '0' ? 0 : 1,
                      },
                    ]}>
                    {ttro === '0' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 3,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3}}>Comment</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 0,
                  justifyContent: 'center',
                }}>
                <TextInput
                  onChangeText={(duration1) => this.setState({duration1})}
                  value={duration1}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          </View>

          {/* 3.1 Third  */}

          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 1}}></View>
            <View
              style={{
                flex: 10,
                flexDirection: 'row',
                borderWidth: 2,
                backgroundColor: '#FAFAFA',
                borderTopWidth: 0,
              }}>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({road2_tsaf: '0'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor:
                          road2_tsaf === '0' ? '#FF1493' : '#fff',
                        borderWidth: road2_tsaf === '0' ? 0 : 1,
                      },
                    ]}>
                    {road2_tsaf === '0' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.setState({road2_tsaf: '1'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor:
                          road2_tsaf === '1' ? '#FF1493' : '#fff',
                        borderWidth: road2_tsaf === '1' ? 0 : 1,
                      },
                    ]}>
                    {road2_tsaf === '1' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 2,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3, textAlign: 'center'}}>
                  Temporary Obstruction
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => this.setState({temp_ob: '1'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: temp_ob === '1' ? '#FF1493' : '#fff',
                        borderWidth: temp_ob === '1' ? 0 : 1,
                      },
                    ]}>
                    {temp_ob === '1' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => this.setState({temp_ob: '0'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: temp_ob === '0' ? '#FF1493' : '#fff',
                        borderWidth: temp_ob === '0' ? 0 : 1,
                      },
                    ]}>
                    {temp_ob === '0' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flex: 3,
                  borderRightWidth: 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{marginVertical: 3, textAlign: 'center'}}>
                  Limited to 15 minutes in Any hour
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  borderRightWidth: 0,
                  justifyContent: 'center',
                }}>
                <TextInput
                  onChangeText={(duration2) => this.setState({duration2})}
                  value={duration2}
                  style={{
                    padding: 0,
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          </View>

          {/* Internal Content Option Second */}

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 8,
            }}>
            <View
              style={{
                borderWidth: 2,
                flexDirection: 'row',
                paddingVertical: 10,
                paddingHorizontal: 7,
              }}>
              <Text>A: Under 3.00m to 6.75m:</Text>
              <TouchableOpacity onPress={() => this.setState({more_than_3: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: more_than_3 === '1' ? '#FF1493' : '#fff',
                      borderWidth: more_than_3 === '1' ? 0 : 1,
                    },
                  ]}>
                  {more_than_3 === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
          </View>

          {/*  Zero Row */}

          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 2,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                borderColor: '#FAFAFA',
                borderRightWidth: 0,
              }}></View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>NO</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>YES</Text>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>Comment</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{marginVertical: 3}}>Duration</Text>
            </View>
          </View>

          {/* First  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 2,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                borderRightWidth: 0,
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginHorizontal: 1,
                }}>
                Mobile Works/ Short Duration
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({mobileworks: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: mobileworks === '0' ? '#FF1493' : '#fff',
                      borderWidth: mobileworks === '0' ? 0 : 1,
                    },
                  ]}>
                  {mobileworks === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({mobileworks: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: mobileworks === '1' ? '#FF1493' : '#fff',
                      borderWidth: mobileworks === '1' ? 0 : 1,
                    },
                  ]}>
                  {mobileworks === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>Moving Works</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(duration_over1) =>
                  this.setState({duration_over1})
                }
                value={duration_over1}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Second  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 2,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                borderRightWidth: 0,
                borderTopWidth: 0,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginHorizontal: 1,
                }}>
                Give & Take
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({give_take: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: give_take === '0' ? '#FF1493' : '#fff',
                      borderWidth: give_take === '0' ? 0 : 1,
                    },
                  ]}>
                  {give_take === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({give_take: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: give_take === '1' ? '#FF1493' : '#fff',
                      borderWidth: give_take === '1' ? 0 : 1,
                    },
                  ]}>
                  {give_take === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>
                Stationary up to 15 minutes Works
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(duration_over2) =>
                  this.setState({duration_over2})
                }
                value={duration_over2}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Third  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 2,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                borderRightWidth: 0,
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 0,
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginHorizontal: 1,
                }}>
                Priority
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({priority: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: priority === '0' ? '#FF1493' : '#fff',
                      borderWidth: priority === '0' ? 0 : 1,
                    },
                  ]}>
                  {priority === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({priority: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: priority === '1' ? '#FF1493' : '#fff',
                      borderWidth: priority === '1' ? 0 : 1,
                    },
                  ]}>
                  {priority === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginBottom: 0,
                }}>
                Stationary up to 15-60 minutes Minimum longways Clearance
                applies (maximum 50mph)
              </Text>
              <Text style={{textAlign: 'center', marginBottom: 3}}>
                Limited Speed 30mph Max
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(duration_over3) =>
                  this.setState({duration_over3})
                }
                value={duration_over3}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Fourth  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 2,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginHorizontal: 1,
                }}>
                Stop & Go
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({stop: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: stop === '0' ? '#FF1493' : '#fff',
                      borderWidth: stop === '0' ? 0 : 1,
                    },
                  ]}>
                  {stop === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({stop: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: stop === '1' ? '#FF1493' : '#fff',
                      borderWidth: stop === '1' ? 0 : 1,
                    },
                  ]}>
                  {stop === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>
                Stationary up to 15 minutes
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(duration_over4) =>
                  this.setState({duration_over4})
                }
                value={duration_over4}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Five  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 2,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginHorizontal: 1,
                }}>
                Traffic Signal
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({traffic_signal: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor:
                        traffic_signal === '0' ? '#FF1493' : '#fff',
                      borderWidth: traffic_signal === '0' ? 0 : 1,
                    },
                  ]}>
                  {traffic_signal === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({traffic_signal: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor:
                        traffic_signal === '1' ? '#FF1493' : '#fff',
                      borderWidth: traffic_signal === '1' ? 0 : 1,
                    },
                  ]}>
                  {traffic_signal === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>
                Stationary up to 15 minutes
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(duration_5) => this.setState({duration_5})}
                value={duration_5}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/*Pedestrain Management */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              backgroundColor: '#F9BF8F',
              marginTop: 8,
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  paddingHorizontal: 3,
                }}>
                4.1
              </Text>
            </View>

            <View
              style={{
                flex: 10,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Padestrain Management ( from section 2.1, B)
              </Text>
            </View>
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 8,
              backgroundColor: '',
              flexDirection: 'row',
              marginBottom: 8,
            }}>
            <View
              style={{
                borderWidth: 2,
                flexDirection: 'row',
                paddingVertical: 10,
                paddingHorizontal: 7,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View>
                <Text>B: Footway less than </Text>
                <Text style={{textAlign: 'center'}}>1.0m wide:</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({road_wide: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: road_wide === '0' ? '#FF1493' : '#fff',
                      borderWidth: road_wide === '0' ? 0 : 1,
                    },
                  ]}>
                  {road_wide === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                borderWidth: 2,
                flexDirection: 'row',
                paddingVertical: 10,
                paddingHorizontal: 7,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 20,
              }}>
              <View>
                <Text>B: Footway more than </Text>
                <Text style={{textAlign: 'center'}}>1.0m wide:</Text>
              </View>
              <TouchableOpacity onPress={() => this.setState({road_wide: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: road_wide === '1' ? '#FF1493' : '#fff',
                      borderWidth: road_wide === '1' ? 0 : 1,
                    },
                  ]}>
                  {road_wide === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
          </View>

          {/*  Zero Row */}

          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>NO</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>YES</Text>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>Comment</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#F9BF8F',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{marginVertical: 3}}>Duration</Text>
            </View>
          </View>

          {/* First  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({temp_clos: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: temp_clos === '0' ? '#FF1493' : '#fff',
                      borderWidth: temp_clos === '0' ? 0 : 1,
                    },
                  ]}>
                  {temp_clos === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({temp_clos: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: temp_clos === '1' ? '#FF1493' : '#fff',
                      borderWidth: temp_clos === '1' ? 0 : 1,
                    },
                  ]}>
                  {temp_clos === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>
                Temporary Closure Sign Limited to 15 minutes in any hour
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(temp_duration) => this.setState({temp_duration})}
                value={temp_duration}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Second  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({walk_carriage: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor:
                        walk_carriage === '0' ? '#FF1493' : '#fff',
                      borderWidth: walk_carriage === '0' ? 0 : 1,
                    },
                  ]}>
                  {walk_carriage === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({walk_carriage: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor:
                        walk_carriage === '1' ? '#FF1493' : '#fff',
                      borderWidth: walk_carriage === '1' ? 0 : 1,
                    },
                  ]}>
                  {walk_carriage === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>Walkway in Carriageway</Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(walk_duration) => this.setState({walk_duration})}
                value={walk_duration}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Third  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({divert: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: divert === '0' ? '#FF1493' : '#fff',
                      borderWidth: divert === '0' ? 0 : 1,
                    },
                  ]}>
                  {divert === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity onPress={() => this.setState({divert: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor: divert === '1' ? '#FF1493' : '#fff',
                      borderWidth: divert === '1' ? 0 : 1,
                    },
                  ]}>
                  {divert === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text
                style={{
                  marginVertical: 3,
                  textAlign: 'center',
                  marginBottom: 0,
                }}>
                Divert to other footway
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(divert_duration) =>
                  this.setState({divert_duration})
                }
                value={divert_duration}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Fourth  */}
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({temp_signals: '0'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor:
                        temp_signals === '0' ? '#FF1493' : '#fff',
                      borderWidth: temp_signals === '0' ? 0 : 1,
                    },
                  ]}>
                  {temp_signals === '0' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <TouchableOpacity
                onPress={() => this.setState({temp_signals: '1'})}>
                <View
                  style={[
                    styles.outerBox,
                    {
                      backgroundColor:
                        temp_signals === '1' ? '#FF1493' : '#fff',
                      borderWidth: temp_signals === '1' ? 0 : 1,
                    },
                  ]}>
                  {temp_signals === '1' && (
                    <Icon name="check" color="white" size={15} />
                  )}
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 5,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                alignItems: 'center',
                borderRightWidth: 0,
                borderTopWidth: 0,
              }}>
              <Text style={{marginVertical: 3}}>
                Temporary Signals, 'Traffic Signals' above
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                backgroundColor: '#FAFAFA',
                borderWidth: 2,
                justifyContent: 'center',
                borderTopWidth: 0,
              }}>
              <TextInput
                onChangeText={(tempsignal_duration) =>
                  this.setState({tempsignal_duration})
                }
                value={tempsignal_duration}
                style={{
                  padding: 0,
                  fontSize: 15,
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              />
            </View>
          </View>

          {/* Additional Info */}

          <View
            style={{
              flexDirection: 'row',
              borderWidth: 2,
              backgroundColor: '#F9BF8F',
              marginTop: 8,
            }}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  paddingHorizontal: 3,
                }}>
                5.1
              </Text>
            </View>

            <View
              style={{
                flex: 12,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 3,
              }}>
              <Text
                style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
                Additional information relating to this site is included
                overleaf, e.g. Streat lighting, traffic signs, proximity of
                junctions, sight lines problems, parking restriction
              </Text>
            </View>
          </View>

          <View
            style={{
              borderWidth: 2,
              borderTopWidth: 0,
              backgroundColor: '#FAFAFA',
              paddingVertical: 5,
            }}>
            <View
              style={[
                styles.container,
                {justifyContent: 'space-between', marginVertical: 0},
              ]}>
              <View
                style={[
                  styles.radioBox,
                  {justifyContent: 'flex-start', paddingLeft: 10},
                ]}>
                <TouchableOpacity
                  onPress={() => this.setState({add_info: '1'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: add_info === '1' ? '#FF1493' : '#fff',
                        borderWidth: add_info === '1' ? 0 : 1,
                      },
                    ]}>
                    {add_info === '1' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
                <Text style={styles.label}>Yes</Text>
              </View>

              <View
                style={[
                  styles.radioBox,
                  {justifyContent: 'flex-start', paddingLeft: 10},
                ]}>
                <TouchableOpacity
                  onPress={() => this.setState({add_info: '0'})}>
                  <View
                    style={[
                      styles.outerBox,
                      {
                        backgroundColor: add_info === '0' ? '#FF1493' : '#fff',
                        borderWidth: add_info === '0' ? 0 : 1,
                      },
                    ]}>
                    {add_info === '0' && (
                      <Icon name="check" color="white" size={15} />
                    )}
                  </View>
                </TouchableOpacity>
                <Text style={styles.label}>No</Text>
              </View>
            </View>
          </View>

          <View style={{marginVertical: 5}}>
            <Text>
              NOTE: The information contained herein is minimum Guidance on the
              level of Temporary Traffic Management required.
            </Text>
          </View>

          <TouchableButton title="SUBMIT" onPress={() => this.onSubmit()} />
        </ScrollView>
        {this.state.isDateTimePickerVisible ? (
          <DateTimePicker
            mode="date"
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            is24Hour={false}
            onChange={this.handleDatePicked}
            // onConfirm={(date)=>this._handleDatePicked(date)}
          />
        ) : null}
        <Loader isLoader={this.state.loading}></Loader>
        {/* <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
          <View>
              <View style={{backgroundColor: '', marginBottom: 100}}>
                <DatePicker
                mode="date"
                  date={this.state.setDate}
                  onDateChange={(setDates) => this.setState({setDate:moment(setDates).format('MM-DD- YYYY')})}
                  androidVariant="nativeAndroid"
                />
              </View>
              <View style={{flexDirection: 'row',borderTopWidth:1}}>
              <TouchableOpacity
              onPress={() => this.setState({open:false,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.setState({open:false,confirmDate:this.state.setDate,modalVisible:!this.state.modalVisible})}
                  style={{backgroundColor: '', flex: 1, padding: 20,justifyContent:'center',alignItems:'center'}}>
                  <Text>Ok</Text>
                </TouchableOpacity>
                
              </View>
            </View>

          </View>
        </View>
      </Modal> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  outerBox: {
    height: 19,
    width: 19,
    borderWidth: 2,

    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  label: {fontSize: 15},
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 0,
  },
  radioBox: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
});
