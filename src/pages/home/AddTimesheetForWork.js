import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  ScrollView,
  Image,Alert
} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import Icon from '../../component/Icon';
import {TouchableOpacity} from 'react-native-gesture-handler';
import TouchableButton from '../../component/TouchableButton';
import RadioButton from '../../component/RadioButton';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import Loader from '../../WebAPI/Loader'
import {save_user_job,requestPostApiMedia,get_jobs_dataById,requestGetApi} from '../../WebAPI/Service'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

import KeyStore from '../KeyStore/LocalKeyStore'
import ImagePicker from 'react-native-image-crop-picker';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
export default class AddTimesheetForWork extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      imageDatas: '',

      status: '',
      isDateTimePickerVisible: false,
      mode: '',
      value:1,
      q_number:'',
      employee_name:'',
      vehicle_reg:'',
      customer_name:'',
      site_address:'',
      date_and_time:'',
      site_check:0,
      desc_of_site:'',
      job_complete:0, 
      job_complete_date:'',
      desc_of_tm:'' ,
      user_id:'',
      loading:false,
      image_one:[],
    image_second:[],
    image_third:[],
    image_fourth:[],
    image_fifth:[],
    imageContainer:[],
    id:'',
    errorMessage:''
    };
  }


  async firstImagePicker(){
    await ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64:true,
      compressImageQuality:.5,
      compressImageMaxWidth:300,
      compressImageMaxHeight:400
    }).then(async(image) => {
let arr = this.state.imageContainer
arr.push(image)
     await this.setState({imageContainer:arr})
      console.warn(image.path);
    });
  
    
  }
async componentDidMount() {

  KeyStore.getKey('data', (err, value) => {
    if (value) {
      const {user_id} = JSON.parse(value);
      this.setState({user_id})
    } else {
    }

  });
  

const id = this.props.navigation.state.params ? this.props.navigation.state.params.id :null
const user_id=this.props.navigation.state.params ? this.props.navigation.state.params.user_id :null
if(id){
this.setState({loading:true})  

const body ={
user_id,
  id
}
console.warn(user_id)
const {responseJson,err} = await requestGetApi(get_jobs_dataById,body,'GET')
if(responseJson.status){

  const {employee_name,vehicle_reg,customer_name,q_number,site_address,date_and_time,site_check,desc_of_tm,desc_of_site,job_complete,job_complete_date,signature,image1,image2,image3,image4,image5} = responseJson.jobs_data
  this.setState({loading:false,employee_name,vehicle_reg,customer_name,q_number,site_address,date_and_time,site_check,desc_of_tm,desc_of_site,job_complete,job_complete_date,})





}


}else{
  console.warn("noo")
}



}

async onSubmit(){
const {user_id,employee_name,vehicle_reg,customer_name,q_number,site_address,date_and_time,site_check,desc_of_tm,desc_of_site,job_complete,job_complete_date,} = this.state
this.setState({loading:true})
const fileName = Math.floor(Math.random() * 100) + 1 ;

const id = this.props.navigation.state.params ? this.props.navigation.state.params.id :null
// let img_name= this.state.imageDatas.?  this.state.imageDatas.split('/storage/emulated/0/Pictures/'):null
//     img_name = img_name ? img_name[1]:null
if(id){
  
  const formData = new FormData()
  formData.append('id',id)
  formData.append('user_id',user_id)
  formData.append('employee_name',employee_name)
  formData.append('vehicle_reg',vehicle_reg)
  formData.append('customer_name',customer_name)
  formData.append('q_number',q_number)
  formData.append('site_address',site_address)
  formData.append('date_and_time',date_and_time)
  formData.append('site_check',site_check)
  formData.append('desc_of_tm',desc_of_tm)
  formData.append('desc_of_site',desc_of_site)
  formData.append('job_complete',job_complete)
  formData.append('job_complete_date',job_complete_date)

  if(this.state.imageDatas != ''){
    formData.append('signature',{
      uri: Platform.OS == "android" ?  this.state.imageDatas :  this.state.imageDatas.replace("file://", ""),
      type: 'image/jpg',
      name: fileName+'.png',
    })
  
  }
  console.warn("form",formData)
  const {responseJson,err} = await requestPostApiMedia(save_user_job,formData,'POST')
  console.warn(err)
  if(responseJson.status){
    this.setState({loading:false})
    Alert.alert(
      '',
      responseJson.message,
      [
       
        {text: 'Ok', onPress: async()=>{
          this.props.navigation.replace('MobileWorkRecordSheet')
  
        } },
      ]
      );
    
  }else{
    Alert.alert(err)
    this.setState({loading:false})
  }


}else{

if(this.userInputFormValidation()){
  
  const formData = new FormData()
  formData.append('user_id',user_id)
  formData.append('employee_name',employee_name)
  formData.append('vehicle_reg',vehicle_reg)
  formData.append('customer_name',customer_name)
  formData.append('q_number',q_number)
  formData.append('site_address',site_address)
  formData.append('date_and_time',date_and_time)
  formData.append('site_check',site_check)
  formData.append('desc_of_tm',desc_of_tm)
  formData.append('desc_of_site',desc_of_site)
  formData.append('job_complete',job_complete)
  formData.append('job_complete_date',job_complete_date)
  formData.append('job_complete_date',job_complete_date)
//   formData.append('signature', {
//     uri: Platform.OS === "android" ?  this.state.imageDatas :  this.state.imageDatas.replace("file://", ""),
//     type: 'image/jpg',
//     name: fileName+'.png',
//   }
// )
  
 

  const {responseJson,err} = await requestPostApiMedia(save_user_job,formData,'POST')
  console.warn(err)
  if(responseJson.status){
    this.setState({loading:false})
    Alert.alert(
      '',
      responseJson.message,
      [
       
        {text: 'Ok', onPress: async()=>{
          this.props.navigation.replace('MobileWorkRecordSheet')
  
        } },
      ]
      );
    
  }else{
    this.setState({loading:false})
    Alert.alert('',responseJson.message)
    
  }
}else{
 
  this.setState({loading:false})
}




  


}


}
//Validation

userInputFormValidation(){
  const {user_id,employee_name,vehicle_reg,customer_name,q_number,site_address,date_and_time,site_check,desc_of_tm,desc_of_site,job_complete,job_complete_date,imageDatas,imageContainer} = this.state
  if(employee_name==''|| vehicle_reg==''||customer_name==''||q_number==''||imageDatas==''||imageContainer==''){
if(employee_name==''){
  Alert.alert('','Please fill Emplyoee name')

}else if(vehicle_reg==''){
  Alert.alert('','Please fill Vehicle reg')
 
}else if(q_number==''){
  Alert.alert('','Please fill q number')

}else if(customer_name==''){
  Alert.alert('','Please fill customer name')
 
}else if(date_and_time==''){
  Alert.alert('','Please fill date_and_time')
  
}else if(imageDatas==''){
  Alert.alert('','Please Upload TMO Signature')
  
}else if(imageContainer.length==0){
  Alert.alert('','Please upload at least one Image')

}
return false
}else{
  return true
}

}





passImage(data) {
  console.log('testingggggggggggggggggggggggg', data);
  this.setState({imageDatas: data});
}

  openTimesheet = () => {
    this.setState({open: true});
  };
  closeTimesheet = () => {
    this.setState({open: false});
  };

  openTimesheet = () => {
    this.setState({open: true});
  };
  closeTimesheet = () => {
    this.setState({open: false});
  };
  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (datetime) => {
    this.setState({isDateTimePickerVisible: false});

    if (this.state.status == 1) {
      this.setState({
        date_and_time: moment(datetime).format(
          'MM/DD/YYYY HH:mm',
        ),
      });
    } else {
      this.setState({
        job_complete_date: moment(datetime).format('MM/DD/YYYY HH:mm '),
      });
    }
  };

  render() {
    const {user_id,employee_name,vehicle_reg,customer_name,q_number,site_address,date_and_time,site_check,desc_of_tm,desc_of_site,job_complete,job_complete_date,} = this.state

   
    console.warn("Image Array",this.state.imageContainer.length);
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="Work Sheet Record"
          goBack={() => this.props.navigation.goBack()}
        />
  
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          <View style={{margin: 10}}>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
              Employee Name
              </Text>
              <TextInput
              autoCorrect={false}
                placeholder="Enter Employee Name"
                value={employee_name}
                onChangeText={employee_name=>this.setState({employee_name})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Vehicle Reg
              </Text>
              <TextInput
                placeholder="Enter Vehicle Reg"
                value={vehicle_reg}
                onChangeText={vehicle_reg=>this.setState({vehicle_reg})}
                autoCorrect={false}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                  
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Customer Name
              </Text>
              <TextInput
                placeholder="Enter Customer Name"
                autoCorrect={false}
                value={customer_name}
                onChangeText={customer_name=>this.setState({customer_name})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Q Number</Text>
              <TextInput
                placeholder="Enter Q Number"
                autoCorrect={false}
                value={q_number}
                onChangeText={q_number=>this.setState({q_number})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Site Address
              </Text>
              <TextInput
                placeholder="Enter Site Address"
                value={site_address}
                autoCorrect={false}
                onChangeText={site_address=>this.setState({site_address})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: '#757575',
                }}
              />
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Install Date & Time
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16, color: '#757575'}}>
                    {date_and_time
                      ? date_and_time
                      : 'Select Date & Time'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'datetime',
                        status: 1,
                      });
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Site Check</Text>
             
              <View style={[styles.container,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,}]}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({site_check: 1})}>
              <View style={styles.outerCircle}>
                {site_check == 1 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({site_check: 2})}>
              <View style={styles.outerCircle}>
                {site_check == 2 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>
            <View
              style={{borderWidth: 0.6, borderColor: '#F8981D', padding: 5}}>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Description Of TM
              </Text>
              <TextInput
               
                autoCorrect={false}
            //  multiline={true}
                value={desc_of_tm}
                onChangeText={desc_of_tm=>this.setState({desc_of_tm})}
                placeholder="Enter Description Of TM"
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: 'blue',
                  justifyContent: 'flex-start',
                  paddingTop: 0,
                  textAlign: 'justify',
                }}
              />
            </View>

            <View
              style={{borderWidth: 0.6, borderColor: '#F8981D', padding: 5}}>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Description Of Site Check
              </Text>
              <TextInput
                numberOfLines={5}
                placeholder="Enter Description Of Site Check"
                value={desc_of_site}
                onChangeText={desc_of_site=>this.setState({desc_of_site})}
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 5,
                  marginTop: 5,
                  marginBottom: 5,
                  fontSize: 16,
                  color: 'blue',
                  justifyContent: 'flex-start',
                  paddingTop: 0,
                }}
              />
            </View>
            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>Job Complete</Text>
             
              <View style={[styles.container,{ borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,}]}>
          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({job_complete: 1})}>
              <View style={styles.outerCircle}>
                {job_complete == 1 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>Yes</Text>
          </View>

          <View style={styles.radioBox}>
            <TouchableOpacity onPress={() => this.setState({job_complete: 2})}>
              <View style={styles.outerCircle}>
                {job_complete == 2 && <View style={styles.innerCircle} />}
              </View>
            </TouchableOpacity>
            <Text style={styles.label}>No</Text>
          </View>
        </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Job Complete Date & Time
              </Text>
              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 9,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  backgroundColor: '',
                }}>
                <View style={{flex: 5, justifyContent: 'center'}}>
                  <Text style={{fontSize: 16, color: '#757575'}}>
                    {job_complete_date
                      ? job_complete_date
                      : 'Select Date & Time'}
                  </Text>
                </View>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        isDateTimePickerVisible: true,
                        mode: 'datetime',
                        status: 2,
                      });
                    }}>
                    <Icon name="calendar" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                TMO Signature
              </Text>
              <Text style={{fontSize: 13, fontWeight: '600'}}>
                Click plus icon to add signature
              </Text>

              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('Signature', {
                      reload: this.passImage.bind(this),
                    })
                  }>
                  <View
                    style={{
                      padding: 10,
                      backgroundColor: '#403EB6',
                      borderRadius: 50,
                      height: 40,
                      width: 40,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon size={25} name="plus" color="#fff" />
                  </View>
                </TouchableOpacity>

                <View
                  style={{
                    padding: 10,
                    height: 60,
                    width: 60,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                 {this.state.imageDatas != '' ? (
                    <Image
                      source={{
                        uri: this.state.imageDatas,
                      }}
                      style={{height: 60, width: 40}}
                    />
                  ) : (
                    <Icon size={40} name="pencil" color="black" />
                  )}
                </View>
              </View>
            </View>

            <View>
              <Text style={{fontSize: 17, fontWeight: 'bold'}}>
                Image of Site and Equipment Only
              </Text>
              <Text style={{fontSize: 13, fontWeight: '600'}}>
                Click plus icon to add Image. You can upload maximum 5 images
              </Text>

              <View
                style={{
                  borderWidth: 0.6,
                  borderColor: '#F8981D',
                  padding: 10,
                  marginTop: 5,
                  marginBottom: 5,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
{/* {
  this.state.image_one.data ?
  <Image
            
            source={{uri: `data:${this.state.image_one.mime};base64,${this.state.image_one.data}`}}
             style={{
               height: wp('15%'),
               width: wp('30%'),
               resizeMode: 'contain',
               
             }}
            />:(<Image
            source={require('../../images/images_iconn.png')}
            style={{
              height: wp('15%'),
              width: wp('30%'),
              resizeMode: 'stretch',
            }}
          />)
} */}
 
<View style={{flex:10,justifyContent:"space-around",flexDirection:'row'}}>
<View style={{flex:1}}>
{
this.state.imageContainer.length >0 ?    this.state.imageContainer.map((item,index)=>{

if(index%2==0){
return (
<Image
source={{uri:item.path}}
style={{
  height: wp('15%'),
  width: wp('30%'),
  resizeMode: 'contain',
  marginBottom:wp("5%")
}}
/>

)
}
})
: <Image
source={require('../../images/images_iconn.png')}
style={{
  height: wp('15%'),
  width: wp('30%'),
  resizeMode: 'stretch',
}}
/>
}
</View>
  
<View style={{flex:1}}>
{
this.state.imageContainer.length >0 ?    this.state.imageContainer.map((item,index)=>{

if(index%2!=0){
return (
<Image
source={{uri:item.path}}
style={{
  height: wp('15%'),
  width: wp('30%'),
  resizeMode: 'contain',
  marginBottom:wp("5%")
}}
/>

)
}
})
: null
}
</View>
</View>

<View style={{flex:2,justifyContent:"center",alignItems:'center'}}>
<TouchableOpacity onPress={() => this.firstImagePicker()}>
                  <View
                    style={{
                      padding: 10,
                      backgroundColor: '#403EB6',
                      borderRadius: 50,
                      height: 40,
                      width: 40,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon size={25} name="plus" color="#fff" />
                  </View>
                </TouchableOpacity>
</View>
                
              </View>
              
            </View>

            <TouchableButton
              title="SUBMIT"
              onPress={() => this.onSubmit()}
            />
          </View>
        </KeyboardAwareScrollView>
        <Loader isLoader={this.state.loading}></Loader>
        {this.state.isDateTimePickerVisible ? (
          <DateTimePicker
            mode={this.state.mode}
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            is24Hour={false}
           // onChange={this.handleDatePicked}
            // onConfirm={(date)=>this._handleDatePicked(date)}
          />
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({

  innerCircle:{height:13,width:13,backgroundColor:'#00aaff',borderRadius:100,borderWidth:1,borderColor:'#00aaff'},
  outerCircle:{height:20,width:20,borderWidth:1,borderRadius:100,justifyContent:'center',alignItems:'center',marginRight:10,marginLeft:5},
  label:{fontSize:15},
  container:{flexDirection:'row',justifyContent:'space-around',marginLeft:0,marginVertical:10},
  radioBox:{flexDirection:'row',alignItems:'center',flex:1},
});
