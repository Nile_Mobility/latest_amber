import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Alert,
  AsyncStorage,
  BackHandler,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import LoginTextInput from '../../component/LoginTextInput';
import KeyStore from '../KeyStore/LocalKeyStore';

import {
  requestGetApi,
  loginApi,
  requestPostApi,
  requestPostApiMedia,
} from '../../WebAPI/Service';
import Loader from '../../WebAPI/Loader';

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      emailError: '',
      password: '',
      passwordError: '',
      loading: false,
    };
  }

  async componentDidMount() {
    await AsyncStorage.setItem('isLoading', 'false');
  }

  validateEmail() {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.state.email).toLowerCase());
  }

  fullValidation() {
    const {email, password} = this.state;
    if (email === '' && password === '') {
      this.setState({
        emailError: 'Email can not be empty',
        passwordError: 'Password can not be empty',
      });
    } else if (!this.validateEmail()) {
      this.setState({emailError: 'Please enter right  Format of email'});
    } else if (password.length <= 5) {
      this.setState({passwordError: ' password must be grater than 6'});
    } else {
      return true;
    }
  }

  async loginApi() {
    if (this.fullValidation()) {
      const formData = new FormData();
      formData.append('email', this.state.email);
      formData.append('password', this.state.password);
      formData.append('device_type_id', '6');
      formData.append('device_id', '1234444444');
      formData.append('fcm_id', '236465461');

      this.setState({loading: true});
      const {responseJson, err} = await requestPostApiMedia(
        loginApi,
        formData,
        'POST',
      );

      console.warn('heyy', responseJson);

      if (responseJson.status) {
        this.setState({loading: false});

        KeyStore.setKey('data', JSON.stringify(responseJson.data));

        //await AsyncStorage.setItem('data',JSON.stringify(responseJson.data))
        this.props.navigation.navigate('Tab');
      } else {
        console.log('checking', responseJson.data);
        this.setState({loading: false});
        Alert.alert('', responseJson.message);
      }
    }
  }

  openToast = () => {
    // ToastAndroid.showWithGravity('Show Test Toast',ToastAndroid.SHORT,ToastAndroid.BOTTOM)
  };

  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick = () => {
    //this.props.navigation.isFocused() helps to exit the app in one screen not in the whole
    if (this.props.navigation.isFocused()) {
      Alert.alert(
        '',
        'Do you want to close app?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => BackHandler.exitApp(),
          },
        ],
        {cancelable: false},
      );
      return true;
    }
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: ''}}>
        <View
          style={{justifyContent: 'center', alignItems: 'center', padding: 60}}>
          <Image
            source={require('./../../images/app_icon_circle.png')}
            style={{height: wp('30%'), width: wp('30%')}}
          />
        </View>

        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{color: '#7C7C7C', fontSize: 20}}>
            Welcome to Amber-RTM
          </Text>
        </View>

        <View style={{margin: wp('5%')}}>
          <LoginTextInput
            label="Email"
            value={this.state.email}
            onChangeText={(newText) =>
              this.setState({email: newText, emailError: ''})
            }
          />
          {<Text style={{color: 'red'}}>{this.state.emailError}</Text>}

          <LoginTextInput
            label="Password"
            value={this.state.password}
            onChangeText={(newText) =>
              this.setState({password: newText, passwordError: ''})
            }
            secureTextEntry={true}
          />
          {<Text style={{color: 'red'}}>{this.state.passwordError}</Text>}
          <View style={{alignItems: 'flex-end'}}>
          <Text
               onPress={() => this.props.navigation.navigate('ForgotPassword')}
                style={{
                  color: '#B89268',
                  fontWeight: 'bold',
                  marginBottom: 10,
                  textAlign:'right'
                }}>
                Forgot Password?
              </Text>
          </View>
          <TouchableOpacity
            onPress={() => this.loginApi()}
            style={{
              backgroundColor: '#FCA73A',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: '#fff', fontSize: 18}}>SIGN IN</Text>
          </TouchableOpacity>
        </View>

        <View>
          <Image
            source={require('../../images/login_bg.png')}
            style={{height: wp('40%'), width: wp('100%')}}
            resizeMode={'stretch'}
          />
        </View>
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
