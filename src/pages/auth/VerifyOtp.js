import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity, Alert} from 'react-native';
import HeaderWithBack from '../../component/HeaderWithBack';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import LoginTextInput from '../../component/LoginTextInput';
import {requestPostApiMedia, verify_email} from '../../WebAPI/Service';
import Loader from '../../WebAPI/Loader';
export default class VerifyOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      loading: false,
      email: '',
    };
  }

  async componentDidMount() {
    this.setState({email: this.props.navigation.state.params.data.email});
    console.warn('datatata::', this.props.navigation.state.params.data.otp);
  }

  async onSubmit() {
    this.setState({loading: true});
    const formData = new FormData();
    formData.append('email', this.state.email);
    formData.append('otp', this.state.otp);

    const {responseJson, err} = await requestPostApiMedia(
      verify_email,
      formData,
      'POST',
    );

    if (responseJson.status) {
      this.setState({loading: false});

      Alert.alert('', responseJson.message, [
        {
          text: 'Ok',
          onPress: () =>
            this.props.navigation.navigate('ResetPassword', {
              data: responseJson.data,
            }),
        },
      ]);
    } else {
      this.setState({loading: false});
      Alert.alert('', responseJson.message);
    }
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#FAFAFA'}}>
        <HeaderWithBack
          title="VERIFY OTP"
          goBack={() => this.props.navigation.goBack()}
        />
        <View style={{margin: wp('5%')}}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 16,
              marginLeft: 0,
              marginRight: 0,
            }}>
            We have sent you the One Time Password(OTP) to your registered Email
            id
          </Text>
          <Text style={{textAlign: 'center', fontSize: 15.5}}>
            Please enter the same here to change the password
          </Text>
          <LoginTextInput
            label="OTP Code"
            value={this.state.otp}
            onChangeText={(newText) => this.setState({otp: newText})}
          />

          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={{
              backgroundColor: '#FCA73A',
              padding: 15,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 5,
            }}>
            <Text style={{color: '#fff', fontSize: 18}}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
