import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import LoginTextInput from '../../component/LoginTextInput';
import HeaderWithBack from '../../component/HeaderWithBack';
import {requestPostApiMedia, resend_otp} from '../../WebAPI/Service';
import Loader from '../../WebAPI/Loader';
export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      loading: false,
    };
  }

  async onSubmit() {

    this.setState({loading: true});
    
    const formData = new FormData();
    formData.append('email', this.state.email);

    const {responseJson, err} = await requestPostApiMedia(
      resend_otp,
      formData,
      'POST',
    );

    if (responseJson.status) {
      this.setState({loading: false});

      Alert.alert('', responseJson.message, [
        {
          text: 'Ok',
          onPress: () =>
            this.props.navigation.navigate('VerifyOtp', {
              data: responseJson.data,
            }),
        },
      ]);
    } else {
      this.setState({loading: false});
      Alert.alert('', responseJson.message);
    }
  } 
  render() {
    return (
      <View style={{flex: 1}}>
        <HeaderWithBack
          title="FORGOT PASSWORD"
          goBack={() => this.props.navigation.goBack()}
        />

        <View style={{margin: wp('5%')}}>
          <Text style={{margin: 0, textAlign: 'center', fontSize: 14}}>
            Enter your email below, and we'll send you One Time
          </Text>

          <Text style={{marginLeft: 30, fontSize: 14}}>
            Password (OTP) to your registered Email Id.
          </Text>
        </View>

        <View style={{margin: wp('5%')}}>
          <LoginTextInput
            label="Email"
            value={this.state.email}
            onChangeText={(newText) => this.setState({email: newText})}
            secureTextEntry={false}
          />

          <TouchableOpacity
            onPress={() => this.onSubmit()}
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: '70%',
              marginTop: 30, backgroundColor: '#FCA73A',alignSelf:'center'
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
               
                padding: 15,
                textAlign:"center"
              }}>
              SUBMIT
            </Text>
          </TouchableOpacity>
        </View>

        <Loader isLoader={this.state.loading}></Loader>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
