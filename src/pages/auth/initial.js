import React, {Component} from 'react';
import {StyleSheet, Image, ImageBackground, BackHandler} from 'react-native';

import KeyStore from '../KeyStore/LocalKeyStore';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    resizeMode: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class Initial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  async componentDidMount() {
    KeyStore.getKey('data', (err, value) => {


      if (value) {

        setTimeout(() => {
          this.props.navigation.navigate('Tab');
        }, 3000);
        
      } else {

        setTimeout(() => {
          this.props.navigation.navigate('Login');
        }, 3000);

       
      }
    });
  }

  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick = () => {
    if (this.props.navigation.isFocused()) {
      return true;
    }
  };

  render() {
    return (
      <ImageBackground
        source={require('../../images/splash.jpeg')}
        style={[styles.container]}></ImageBackground>
    );
  }
}
