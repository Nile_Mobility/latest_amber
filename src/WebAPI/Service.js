const baseUrl = 'https://securespace.eu/amber-rtm/api/'
// const baseUrl = 'https://niletechinnovations.com/projects/amber-rtm-mobile/api/'
// const baseUrl = 'http://securespace.info/amber-rtm/api/'



//API END POINT LISTS


export const resend_otp='resend_otp'
export const verify_email='verify_email'
export const update_password='update_password'
export const loginApi = 'login_user';
export const logOutApi = 'user_logout';
export const totalNoOfJob = 'get_userhistory';
export const jobslist = 'operator_qform';
export const updateTpra = 'update_tpra';
export const update_loading1 = 'update_loading';
export const update_loading2 = 'update_loading2';
export const update_Tsaf = 'update_tsaf';
export const update_briefing = 'update_briefing';
export const update_tws = 'update_tws';
export const update_image2 = 'update_image2';
export const vehicle_inspection = 'save_driver_vehicle_inspection';
export const update_status='update_status'
//Timesheet Api
export const get_total_timesheet_data = 'get_total_timesheet_data';
export const save_user_timesheet = 'save_user_timesheet';
export const delete_timesheetById = 'delete_timesheetById';
export const timesheet_final_submit = 'timesheet_final_submit';
export const get_timesheetById='get_timesheetById'
//Mobile Work Sheet

export const get_holding_jobs_data = 'get_holding_jobs_data';
export const delete_user_jobs = 'delete_user_jobs';
export const get_jobs_dataById='get_jobs_dataById'
export const save_user_job = 'save_user_job';
export const job_final_submit = 'job_final_submit';
//history

export const get_userhistory = 'get_userhistory';
export const completed_jobs='completed_jobs'
export const get_vehicle_history='get_vehicle_history'
export const get_vehicle_inspectionById='get_vehicle_inspectionById'
export const save_driver_vehicle_inspection='save_driver_vehicle_inspection'
export const get_all_final_submitted_timesheet='get_all_final_submitted_timesheet'

export const get_final_submitted_timesheet_data_ById='get_final_submitted_timesheet_data_ById'

export const all_completed_job='all_completed_job'
export const job_data_ById='job_data_ById'
// profile
export const operator_details='operator_details'
export const update_details='update_details'

//loading list

export const get_all_inventory_items='get_all_inventory_items'
export const get_loading_list_item='get_loading_list_item'
export const update_loading_list_data='update_loading_list_data'


export const requestGetApi = async (endPoint, body, method) => {
  var header = {};
  var url = baseUrl + endPoint;

  header = {'Content-type': 'application/json', 'Cache-Control': 'no-cache'};

  url = url + objToQueryString(body);
  console.log('Request Url:-' + url + '\n');

  try {
    let response = await fetch(url, {
      method: method,
      headers: header,
    });

    let code = await response.status;
    console.log(code);

    if (code == 200) {
      let responseJson = await response.json();
      console.log(responseJson);
      return {responseJson: responseJson, err: null, code: code};
    } else if (code == 400) {
      let responseJson = await response.json();
      return {responseJson: null, err: responseJson.message, code: code};
    } else {
      return {responseJson: null, err: 'Something went wrong!', code: code};
    }
  } catch (error) {
    return {
      responseJson: null,
      err: 'Something Went Wrong! Please check your internet connection.',
      code: 500,
    };
    console.error(error);
  }
};

export const requestPostApiMedia = async (endPoint, formData, method) => {
  var header = {
   // 'Content-type': 'multipart/form-data'
    
  };

  var url = baseUrl + endPoint;
  console.log('Request Url:-' + url + '\n');
  console.warn(formData + '\n');

  try {
    let response = await fetch(url, {
      method: method,
      body: formData,
    //  headers: header,
    });

    let code = await response.status;
      console.log(code )

    if (code == 200) {
      let responseJson = await response.json();
      console.log('data from backend', responseJson);
      return {responseJson: responseJson, err: null};
    } else if (code == 400) {
      let responseJson = await response.json();
      return {responseJson: null, err: responseJson.message};
    } else {
      return {responseJson: null, err: 'Something went wrong!'};
    }
  } catch (error) {
    console.log(error);
    return {
      responseJson: null,
      err: 'Something Went Wrong! Please check your internet connection.',
    };
    
  }
};

const objToQueryString = (obj) => {
  const keyValuePairs = [];
  for (const key in obj) {
    keyValuePairs.push(
      encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]),
    );
  }
  return keyValuePairs.length == 0 ? '' : '?' + keyValuePairs.join('&');
};
