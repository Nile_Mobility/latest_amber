import FontAwesome from 'react-native-vector-icons/FontAwesome'
import React  from 'react'
Icon = (props)=>{

    return (
        <FontAwesome
        name={props.name}
        color={props.color}
        size={props.size}
        />
    )
}

export default Icon






