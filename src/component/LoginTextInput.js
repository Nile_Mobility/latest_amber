import React, { Component } from 'react';
import {
  View,

  TextInput,
  Animated,
} from 'react-native';

export default class LoginTextInput extends Component {
  state = {
    isFocused: false,
  };

  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);
  }

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
      duration: 200,
    }).start();
  }

  render() {
    const { label, ...props } = this.props;
    const labelStyle = {
        
      position: 'absolute',
      left: 0,
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [30, 5],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 16],
      }),
      color: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: ['#000', '#D65786'],
        
      }),
      // fontWeight: this._animatedIsFocused.interpolate({
      //   inputRange: [0, 1],
      //   outputRange: ['600', '900'],
        
      // }),

      
      
    };

    

    return (
      <View style={{ paddingTop: 18 ,backgroundColor:"",marginBottom:10}}>
        <Animated.Text style={{ ...labelStyle}}>
          {label}
        </Animated.Text>
        <TextInput
          {...props}
          autoCapitalize = 'none'
          underlineColorAndroid="transparent"
          style={{ height: 45, fontSize: 20, color: '#000', borderBottomWidth: 1, borderBottomColor: '#555', }}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
         
          blurOnSubmit
        />
      </View>
    );
  }
}

