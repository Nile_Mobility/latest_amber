import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  
} from 'react-native';

const styles = StyleSheet.create({
  selectedBtn: {
    flex: 1,
    backgroundColor: 'rgba(241,253,254,1.0)',
    borderTopWidth: 1,
    borderColor: '#F8981D',
    alignItems: 'center',
    justifyContent:"center",
    tintColor:'#F8981D',
    color:"red"
  },
  unSelectbtn: {flex: 1, backgroundColor: 'white', alignItems: 'center',justifyContent:"center"},
});

export default class TabBottom extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {tabArr, action} = this.props;
    return (
      <View style={{height: 75, flexDirection: 'row'}}>
        {tabArr.map((item, index) => (
          <TouchableOpacity
            style={[
              item.isSelect ? styles.selectedBtn : styles.unSelectbtn,
              {alignItems: 'center'},
            ]}
            key={index}
            onPress={() => action(index)}>
            <Icon name={item.tabImg} size={25} color= {item.isSelect ? "#F8981D" : "#000"} />
            <Text style={{fontSize: 15, color: item.isSelect ? "#F8981D" : "#000", marginTop: 8}}>
              {item.tabName}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}
