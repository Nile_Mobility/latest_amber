import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';

export default class JobHistoryContentBox extends Component {
  render() {
    return (
      <View
        style={{
          margin: 20,
          borderWidth: 2,
          borderColor: '#F8981D',
          padding: 5,
          borderRadius: 5,
          marginBottom: 0,
        }}>
        <View style={{flexDirection: 'row', padding: 5, marginLeft: 10}}>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Job Id:</Text>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Text style={{color: '#545454', fontSize: 16}}>{this.props.jobId}</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', padding: 5, marginLeft: 10}}>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>Start Date:</Text>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Text style={{color: '#545454', fontSize: 16}}>{this.props.startDate}</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', padding: 5, marginLeft: 10}}>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
              justifyContent: 'center',
            }}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>End Date:</Text>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Text style={{color: '#545454', fontSize: 16}}>{this.props.endDate}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
