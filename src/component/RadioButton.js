import React, {Component} from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';

const PROP = [
  {
    key: 'yes',
    text: 'yes',
  },
  {
    key: 'No',
    text: 'No',
  },
];

export default class RadioButton extends Component {
  state = {
    value: null,
  };

  render() {
    const {value} = this.state;

    return (
      <View>
        {PROP.map((res) => {
          return (
            <View key={res.key} style={styles.container}>
              <View style={{alignItems: 'center', flexDirection: 'row'}}>
                <View
                  style={{
                    height: 25,
                    width: 25,
                    backgroundColor: 'gray',
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      height: 20,
                      width: 20,
                      backgroundColor: '#fff',
                      borderRadius: 100,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <TouchableOpacity onPress={() => this.setState({value:res.key})}>
                    {value === res.key && <View style={styles.selectedRb} />}
                    </TouchableOpacity>
                  </View>
                </View>
                    <Text style={{fontSize: 16, marginLeft: 10}}>{res.text}</Text>
              </View>
            </View>
          );
        })}
        <Text> Selected: {this.state.value} </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 35,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  radioText: {
    marginRight: 35,
    fontSize: 20,
    color: '#000',
    fontWeight: '700',
  },
  radioCircle: {
    height: 30,
    width: 30,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#3740ff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedRb: {
    width: 15,
    height: 15,
    borderRadius: 50,
    backgroundColor: '#3740ff',
  },
  result: {
    marginTop: 20,
    color: 'white',
    fontWeight: '600',
    backgroundColor: '#F3FBFE',
  },
});

//   <View style={{alignItems:"center",flexDirection:"row"}}>
//   <View style={{height:25,width:25,backgroundColor:"gray",borderRadius:100,justifyContent:"center",alignItems:"center"}}>
// <View style={{height:20,width:20,backgroundColor:"#fff",borderRadius:100,justifyContent:"center",alignItems:"center"}}>
// <TouchableOpacity onPress={()=>this.radio(false)}>
// <View style={{height:15,width:15,backgroundColor:this.state.isSelected ? "#fff": "#00aaff",borderRadius:100}}></View>
// </TouchableOpacity>

// </View>

//   </View>
//   <Text style={{fontSize:16,marginLeft:10}}>No</Text>
//   </View>
