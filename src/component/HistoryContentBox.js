import React, { Component } from 'react'
import { Text, StyleSheet, View,TouchableOpacity } from 'react-native'
import {heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen'
import Icon from '../component/Icon'
export default class HistoryContentBox extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={{margin:10,borderWidth:2,borderColor:'#7B7978',flexDirection:"row",alignItems:"center",backgroundColor:"#fff",marginBottom:0,padding:10,borderRadius:5}}>
                <View style={{justifyContent:"center",alignItems:"center",padding:10,flex:1,paddingRight:0}}>
                    <Icon
                    name={this.props.iconName}
                    size={45}
                    
                    />
                </View>
                <View style={{flex:5}}>
        <Text numberOfLines={2} style={{fontWeight:"bold",fontSize:19,padding:10}}>{this.props.title}</Text>
                </View>
               <View style={{flex:1}}>
        <View style={{borderRadius:200,height:wp("10%"),width:wp("10%"),backgroundColor:"#FCA73A",justifyContent:"center",alignItems:"center"}}><Text style={{color:"#fff",fontSize:20}}>{this.props.notification}</Text></View>
               </View>
            </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({})
