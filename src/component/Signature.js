import React, {Component} from 'react';

import {StyleSheet, Text, View, TouchableHighlight} from 'react-native';
import Orientation from 'react-native-orientation';
import SignatureCapture from 'react-native-signature-capture';
// import RNFS from 'react-native-fs';
// import fetch_blob from 'react-native-fetch-blob';
import ViewShot from "react-native-view-shot";

export default class Signature extends Component {
  constructor(props) {
    super();
    this.state = {
      path: '',
      base64: '',
      data:''
    };
  } 
  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      console.warn('portartaitttttttttttttt');
      // do something with landscape layout
    } else {
      // do something with portrait layout
    }
  };

componentWillUnmount() {
  this.props.navigation.state.params.reload(this.state.path)
  Orientation.lockToPortrait();
    Orientation.getOrientation((err, orientation) => {
      console.warn(`Current Device Orientation: ${orientation}`);
    });

    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationDidChange);
}

  saveSign() {
    this.refs.viewShot.capture().then(uri => {
      console.log("do something with ", uri);
      this.setState({path:uri})
      this.props.navigation.goBack()
  
    });

   // this.refs['sign'].saveImage();
  }

  resetSign() {
    this.refs['sign'].resetImage();
  }

  _onSaveEvent = (result) => {
  

//     var fileName = Math.floor(Math.random() * 100) + 1 ;
//     const fs = fetch_blob.fs;
//     const dirs = fetch_blob.fs.dirs;
//     const file_path = dirs.PictureDir + "/"+fileName +'.jpg';
   

//     var image_data = result.encoded

//     RNFS.writeFile(file_path, image_data, 'base64')
//       .then(() =>
//         console.warn('Image converted to jpg and saved at ' + file_path),
//       )
//       .catch((error) => {
//         console.warn("***********************************",JSON.stringify(error));
//       });

//     console.warn(
//       '-------------------------------------------------',
//       file_path,
//     );

//  const data={
//     path:file_path,
//     base64:result.encoded
//   }
    
//   this.setState({ data});

//     this.props.navigation.goBack()
  };

  _onDragEvent() {
    // This callback will be called when the user enters signature
    console.log('dragged');
  }

  onCapture = uri => {

  }

  render() {
console.warn("ffggfgsdgsdf",this.state.data)
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>

<ViewShot style={{flex:1}} ref="viewShot" options={{ format: "jpg", quality: 0.9 }}>
        <SignatureCapture
          style={styles.signature}
          ref="sign"
          onSaveEvent={this._onSaveEvent}
          onDragEvent={this._onDragEvent}
            saveImageFileInExtStorage={false}
          showNativeButtons={false}
          showTitleLabel={true}
          viewMode={'landscape'}
          maxSize={200}
        >

          </SignatureCapture>
          </ViewShot>
        <View style={{height:100,width:'100%', flexDirection: 'row', borderTopWidth: 1}}>
          <TouchableHighlight
            style={styles.buttonStyle}
            onPress={() => {
              this.saveSign();
            }}>
            <Text>Save</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.buttonStyle}
            onPress={() => {
              this.resetSign();
            }}>
            <Text>Reset</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  signature: {
    flex: 7,
    borderColor: '#000033',
    borderBottomWidth: 18,
  },
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#999999',
    margin: 10,
  },
});
