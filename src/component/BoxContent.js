import React, { Component } from 'react'
import { Text, StyleSheet, View ,TouchableOpacity} from 'react-native'
import {heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen'
import Icon from '../component/Icon'
export default class BoxContent extends Component {
    render() {
        return (
           <TouchableOpacity onPress={this.props.openBox}>
                <View style={{borderWidth:2,paddingTop:15,borderColor:"#F8981D",flexDirection:"row",backgroundColor:"#fff",paddingBottom:15,marginBottom:10,elevation:0,borderRadius:8}}>
    <View style={{flex:1,backgroundColor:"",marginLeft:10,justifyContent:"center",alignItems:"center"}}>
    <Icon name={this.props.iconName} size={40}  />
    </View>
    <View style={{flex:5,justifyContent:"space-between",flexDirection:"row"}}>
        <View style={{flex:4}}>
        <Text style={{fontSize:20,fontWeight:'bold'}}>{this.props.title}</Text>
        </View>
        {this.props.notification >0 ? (<View  style={{flex:1,justifyContent:"center",alignItems:"center"}}>
<View style={{height:40,width:40,backgroundColor:"orange",justifyContent:"center",alignItems:"center",borderRadius:100}}>
        <Text style={{color:'#fff',fontSize:19,fontWeight:"bold"}}>{this.props.notification}</Text>
</View>
</View>): null}

    </View>
    

</View>
           </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({})
