import React, { Component } from 'react'
import { Text, StyleSheet, View,TextInput } from 'react-native'
import Icon from './Icon'


export default class ProfileContentBox extends Component {
    render(props) {
        return (
            <View style={{borderBottomWidth:.5,flexDirection:"row",backgroundColor:"",paddingTop:5,paddingBottom:5}}>
               <View style={{flex:1,backgroundColor:"",alignItems:"center",padding:5,paddingBottom:10}}>
                   <Icon
                   name={this.props.iconName}
                   color={this.props.color}
                   size={30}
                   />
               </View>
               <View style={{flex:4}}>
        <Text>{this.props.title}</Text>
                   <TextInput
                   style={{backgroundColor:"",fontSize:17,padding:0,fontWeight:"bold",color:"#757575"}}
                   value={this.props.value}
editable={this.props.editable}



                   />
               </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({})
