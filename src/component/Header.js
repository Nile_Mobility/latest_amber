import React, {Component} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity,Alert} from 'react-native';
import Icon from '../component/Icon';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {logOutApi, requestPostApiMedia} from '../WebAPI/Service';
import KeyStore from '../pages/KeyStore/LocalKeyStore'
export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      deviceId: '',
   
    };
  }


  
  async logOut() {


    




    const formData = new FormData();
    formData.append('user_id', '51');

    formData.append('device_id', '1234444444');

   
    const {responseJson, err} = await requestPostApiMedia(
      logOutApi,
      formData,
      'POST',
    );
    console.log('responsssssse', responseJson);
    console.log('Responseeeee error', err);

   

    if (err == null) {
        Alert.alert(
            "Amber RTM",
            "Do you wish to Sign Out",
            [
              {
                text: "No",
                onPress: () => console.warn("Cancel Pressed"),
                style: "cancel"
              },
              { text: "Yes", onPress: this.props.onPress }
            ],
            { cancelable: false }
          );
     // console.warn(responseJson);
    } else {
      console.log(err);
    }
  }

  render() {
    return (
      <View
        style={{
       
          backgroundColor: '#F8981D',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
         
          height:84
        }}>
        <Image
          source={require('../images/app_icon_circle.png')}
          style={{height: wp('12%'), width: wp('12%'),marginTop:16,marginLeft:8}}
        />
        <Text
          style={{
            color: '#fff',
            fontSize: 20,
            marginRight: 200,
            fontWeight: 'bold',paddingTop:16
          }}>
          {this.props.title}
        </Text>

        <TouchableOpacity style={{marginTop:16,alignItems:'center',justifyContent:'center',height: 50, width: 50,marginTop:16,marginRight:8}}
          onPress={() => this.logOut()}
         >
          <Icon name="power-off" color="#fff" size={30} />
        </TouchableOpacity>

       
      </View>
    );
  }
}

const styles = StyleSheet.create({});
