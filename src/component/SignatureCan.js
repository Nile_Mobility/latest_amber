import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import Signature from 'react-native-signature-canvas';
// import RNFS from 'react-native-fs';
// import fetch_blob from 'react-native-fetch-blob';
import ViewShot from "react-native-view-shot";

export default class SignatureCan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {signature: ''};
  }

  handleSignature = (signature) => {
    // console.warn(signature)

    // this.setState({signature});
    // //const imageData = signature.replace('data:image/png;base64,', '');
    // // const imagePath = `${RNFS.TemporaryDirectoryPath}image.jpg`;

    // // RNFS.writeFile(imagePath, imageData, 'base64')
    // //     .then(() => console.warn('Image converted to jpg and saved at ' + imagePath));

    // const fs = fetch_blob.fs;
    // const dirs = fetch_blob.fs.dirs;
    // const file_path = dirs.DCIMDir + '/haha.png';
    // // Test under 'Samsung Galaxy Note 5'
    // // console.log(dirs.DocumentDir) // /data/user/0/com.bigjpg/files
    // // console.log(dirs.CacheDir)    // /data/user/0/com.bigjpg/cache
    // // console.log(dirs.DCIMDir)     // /storage/emulated/0/DCIM
    // // console.log(dirs.DownloadDir) // /storage/emulated/0/Download
    // // console.log(dirs.PictureDir)  // /storage/emulated/0/Pictures

    // var image_data = signature.split('data:image/png;base64,');
    // image_data = image_data[1];

    // RNFS.writeFile(file_path, image_data, 'base64')
    //   .then(() =>
    //     console.warn('Image converted to jpg and saved at ' + file_path),
    //   )
    //   .catch((error) => {
    //     console.warn(JSON.stringify(error));
    //   });

    // console.warn(
    //   '-------------------------------------------------',
    //   file_path,
    // );
  };

  saveSign() {
    this.refs.viewShot.capture().then(uri => {
      console.log("do something with ", uri);
      this.setState({path:uri})
      this.props.navigation.goBack()
  
    });

   // this.refs['sign'].saveImage();
  }


  render() {
    const style = `.m-signature-pad--footer
    .button {
      background-color: red;
      color: #FFF;
    }`;
    return (
      <View style={{flex: 1}}>
        <View style={styles.preview}>
          {this.state.signature ? (
            <Image
              resizeMode={'contain'}
              style={{width: 335, height: 114}}
              source={{uri: this.state.signature}}
            />
          ) : null}
        </View>
        <Signature
          onOK={this.handleSignature}
          descriptionText="Sign"
          clearText="Clear"
          confirmText="Save"
          webStyle={style}
          dataURL="Base64"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  preview: {
    width: 335,
    height: 114,
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  previewText: {
    color: '#FFF',
    fontSize: 14,
    height: 40,
    lineHeight: 40,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#69B2FF',
    width: 120,
    textAlign: 'center',
    marginTop: 10,
  },
});
