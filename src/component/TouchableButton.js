import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';

export default class TouchableButton extends Component {
  render() {
    return (
      <TouchableOpacity
      onPress={this.props.onPress}
        style={{
          backgroundColor: '#F8981D',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10,
          marginLeft: '30%',
          marginRight: '30%',
          borderRadius: 8,
          marginTop:10
        }}>
        <Text style={{color: '#fff', fontSize: 16}}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({});
