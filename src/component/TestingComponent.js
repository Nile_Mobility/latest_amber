import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

const Testing = (props) => {
  return (
    <View style={{backgroundColor: props.color, padding: props.size}}>
      <Text>{props.name}</Text>
    </View>
  );
};

const CustomizedButton = (props) => {
  return props.status === 0 ? (
    <TouchableOpacity
      onPress={() => props.callAlert()}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        padding: props.padding,
        margin: props.margin,
        backgroundColor: props.color,
        borderRadius:props.radius
      }}>
      <Text>{props.title}</Text>
    </TouchableOpacity>
  ) : null;
};

export default CustomizedButton;
