import React, { Component } from 'react'
import { Text, StyleSheet, View,Image,TouchableOpacity } from 'react-native'
import Icon from '../component/Icon'
import {heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen'

export default class HeaderWithBack extends Component {
    render(props) {
        return (
            <View style={{felx:1,backgroundColor:"#F8981D",flexDirection:"row",justifyContent:"space-between",alignItems:"center",padding:20}}>
                
                <View style={{flex:1}}>
                <TouchableOpacity style={{marginTop:16}} onPress={this.props.goBack}>
                <Icon
                name="chevron-left"
                size={25}
                color="#fff"
                
                />
                </TouchableOpacity>
                </View>
<View style={{justifyContent:"center",alignItems:"center"}}>
<Text style={{color:"#fff",fontSize:20,fontWeight:"bold",paddingTop:16,textAlign:'center'}}>{this.props.title}</Text>
</View>
               
<View style={{flex:1}}>
</View>

            </View>
        )
    }
}

const styles = StyleSheet.create({})
