import React, {Component} from 'react';
import {Button, View, Text} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';


export default class DateTimePickers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false,
      choosenDate: '',
     
     
    };
  }



  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = (datetime) => {
    this.setState({isDateTimePickerVisible: false});

    this.setState({
      choosenDate: moment(datetime).format('MMMM, Do YYYY HH:mm a'),
    });

   
  };

  render() {
    return (
      <View>
        <Button title="Show DatePicker" onPress={this.showDateTimePicker} />
        <Text>{this.state.choosenDate}</Text>

        <DateTimePicker
          mode="datetime"
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
          is24Hour={false}
          onChange={this.handleDatePicked}
          // onConfirm={(date)=>this._handleDatePicked(date)}
        />
      </View>
    );
  }
}
